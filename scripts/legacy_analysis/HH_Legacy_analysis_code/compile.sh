#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m' # No Color

if [[ ! -v $DIR ]]; then
    printf "${GREEN}\n
==================================
| Sourcing   source setup.sh  ...
==================================${NC}\n"
    source setup.sh
fi


printf "${GREEN}\n
==============================================
| Compiling  submodules/quickstats ...
==============================================${NC}\n"
export PATH=${DIR}/submodules/quickstats/bin:$PATH
export PYTHONPATH=${DIR}/submodules/quickstats:$PYTHONPATH
quickstats compile


cd ${DIR}
