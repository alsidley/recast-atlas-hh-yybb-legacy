TREENAME CollectionTree

DEFINE weight_lumi=${weight_lumi}
DEFINE weight_sum=${weight_sum}

ALIAS eventNumber = EventInfoAuxDyn.eventNumber
ALIAS runNumber = EventInfoAuxDyn.runNumber

SAFEALIAS m_hh = HGamTruthEventInfoAuxDyn.m_hh
SAFEDEFINE m_hh = 0.

SAVE_FRAME BaseFrame

########################################
###           Down Variation         ###
########################################

DEFINE weight=weight_lumi*HGamEventInfoAuxDyn.crossSectionBRfilterEff*HGamEventInfo_${syst_name}__1downAuxDyn.weight*HGamEventInfo_${syst_name}__1downAuxDyn.yybb_weight*HGamEventInfo_${syst_name}__1downAuxDyn.weightFJvt/weight_sum

FILTER @{All event} 1
FILTER @{isPassed} HGamEventInfo_${syst_name}__1downAuxDyn.isPassed
FILTER @{passPresel} HGamEventInfo_${syst_name}__1downAuxDyn.yybb_btag77_cutFlow == 7

# crack veto cleaning for mc16a samples
FILTER @{Pass crack veto cleaning} !((strcmp("${mc_campaign}","mc16a")==0) && (!HGamEventInfo_${syst_name}__1downAuxDyn.passCrackVetoCleaning))
GETSUM @{Pass crack veto cleaning} weight

DEFINE yybb_nonRes_XGBoost_BCal_Cat = HGamEventInfo_${syst_name}__1downAuxDyn.yybb_nonRes_XGBoost_BCal_Cat
DEFINE mass_yy = HGamEventInfo_${syst_name}__1downAuxDyn.m_yy

SAVE_FRAME DownVariation

########################################
###            Up Variation          ###
########################################

LOAD_FRAME BaseFrame

DEFINE weight=weight_lumi*HGamEventInfoAuxDyn.crossSectionBRfilterEff*HGamEventInfo_${syst_name}__1upAuxDyn.weight*HGamEventInfo_${syst_name}__1upAuxDyn.yybb_weight*HGamEventInfo_${syst_name}__1upAuxDyn.weightFJvt/weight_sum

FILTER @{All event} 1
FILTER @{isPassed} HGamEventInfo_${syst_name}__1upAuxDyn.isPassed
FILTER @{passPresel} HGamEventInfo_${syst_name}__1upAuxDyn.yybb_btag77_cutFlow == 7

# crack veto cleaning for mc16a samples
FILTER @{Pass crack veto cleaning} !((strcmp("${mc_campaign}","mc16a")==0) && (!HGamEventInfo_${syst_name}__1upAuxDyn.passCrackVetoCleaning))
GETSUM @{Pass crack veto cleaning} weight

DEFINE yybb_nonRes_XGBoost_BCal_Cat = HGamEventInfo_${syst_name}__1upAuxDyn.yybb_nonRes_XGBoost_BCal_Cat
DEFINE mass_yy = HGamEventInfo_${syst_name}__1upAuxDyn.m_yy

SAVE_FRAME UpVariation

########################################
###            Save Output           ###
########################################

GLOBAL columns=[eventNumber, runNumber, weight, m_hh, mass_yy, yybb_nonRes_XGBoost_BCal_Cat]

LOAD_FRAME DownVariation

REPORT display=1
SAVE treename=output,filename=${outdir}/ntuples/${syst_theme}/${sample}_${syst_name}_1down_${mc_campaign}.root,columns=${columns}

LOAD_FRAME UpVariation

REPORT display=1
SAVE treename=output,filename=${outdir}/ntuples/${syst_theme}/${sample}_${syst_name}_1up_${mc_campaign}.root,columns=${columns}