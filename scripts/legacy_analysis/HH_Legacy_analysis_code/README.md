# HH_Legacy_analysis_code

Analysis framework for ATLAS HH -> bbyy analysis

## Table of Contents

<!-- TOC -->

- [Dependencies](#dependencies)
- [Environment setup](#environment-setup)
    - [Option 1: LCG Environment (Easiest)](#option-1-lcg-environment-easiest)
        - [Setup with PanDA support](#setup-with-panda-support)
    - [Option 2: Conda Environment (Recommended but more involved)](#option-2-conda-environment-recommended)
    - [Option 3: SWAN](#option-3-swan)
    - [Option 4: Docker/Singularity Containers](#option-4-dockersingularity-containers)
- [Examples and Tutorials](#examples-and-tutorials)
    - [Preproduction](#preproduction)
    - [Postproduction](#postproduction)
- [Command Line Interface](#command-line-interface)
- [Epic Command Lines for Reproducing Legacy Analysis Results](#epic-command-lines-for-reproducing-legacy-analysis-results)
- [How to Run Jupyter Notebook Server from Remote Machine](#how-to-run-jupyter-notebook-server-from-remote-machine)
    - [TLDR](#tldr)
    - [Advanced Usage: Multi-port Forwarding with Layers of Remote Servers](#advanced-usage-multi-port-forwarding-with-layers-of-remote-servers)
- [Useful Links](#useful-links)
    - [Twiki Page](#twiki-page)
<!-- /TOC -->

## Dependencies

The bbyy_analysis_framework requires the following python modules:

Python (>= 3.7)
ROOT (>= 6.26)
matplotlib (>= 3.6.0)
pandas (>= 1.4.0)
xgboost (== 1.5.1)
quickstats (latest)
sympy (>= 1.8)

Optional dependencies:

uproot (latest)


## Environment setup

### Option 1: LCG Environment (Easiest)

Perhaps the easiest way to setup the software environment for the framework will be to directly use the LCG environment available in cvmfs. This will allow you to use the latest stable version of ROOT and does not require additional installations. However, it lacks the flexibility of version control of the various software packages provided by the LCG environment.

To setup **for the first time**
```
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/atlas-physics/HDBS/DiHiggs/yybb/hh_legacy_analysis/HH_Legacy_analysis_code.git
cd HH_Legacy_analysis_code
source compile.sh
```

For **future times**
```
source setup.sh
```

If you find out your code is not running properly, try to update the framework repository by doing
```
git pull origin master
git submodules update --recursive
```

#### Setup with PanDA support

To setup with PanDA support (for grid job submission and monitoring)
```
source setup.sh panda
```

### Option 2: Conda Environment (Recommended)



### Option 3: SWAN
If you want to use this framework on SWAN navigate to [https://swan.cern.ch](https://swan.cern.ch)

In your configuration setup you will want to select 'bleeding edge'. Link the path to your bbyy_analysis_framework/setup.sh script in the environment script box.

You may need to upgrade pandas to and numexpr. To do this, you can run the following cells within a jupyter notebook, or directly from the terminal. Replace home-j and jpearkes with your eos initial and username. 
```
pip install --upgrade --user pandas
pip install --upgrade --user numexpr 
export PYTHONPATH=/eos/home-j/jpearkes/.local/lib/python3.9/site-packages:$PYTHONPATH
```

### Option 4: Docker/Singularity Containers

Under construction...

## Analysis Configuration Files

A configuration file is used to pilot various stages of the analysis framework. There are a few templates available in the repository for reproduction / demonstration purposes. They can be found in the subfolder `configs`.

1. `analysis_config_baseline_h027_mH_125p09_GeV.json`: To reproduce results from the ggF HH->bbyy analysis. A Higgs mass assumption of $mH = 125.09$ GeV is used.

2. `analysis_config_h027_3_class_BDT_mH_125p09_GeV.json`: To demonstrate the use of multi-class BDT training and event categorization. A Higgs mass assumption of $mH = 125.09$ GeV is used.

3. `analysis_config_h027_legacy_preproduction_mH_125p09_GeV.json`: To reproduce results from the HH->bbyy legacy analysis before the legacy MxAOD production. A Higgs mass assumption of $mH = 125.09$ GeV is used.

4. `analysis_config_h027_legacy_preproduction_hpo_mH_125p09_GeV.json`: To perform hyperparameter optimization studies. A Higgs mass assumption of $mH = 125.09$ GeV is used.

5. `analysis_config_h027_legacy_mH_125p0_GeV.json`: To reproduce results from the HH->bbyy legacy analysis after the legacy MxAOD production. A Higgs mass assumption of $mH = 125.0$ GeV is used. **Default config for the legacy analysis*.

6. `analysis_config_h027_legacy_mH_125p0_GeV.json`: To reproduce results from the HH->bbyy legacy analysis after the legacy MxAOD production. A Higgs mass assumption of $mH = 125.09$ GeV is used.


## Examples and Tutorials

Example codes and tutorials are available in the form of jupyter notebooks located in `<root_directory>/notebooks`.

For tips on using jupyter notebooks from remote server (e.g. lxplus), please go to this [section](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework#how-to-run-jupyter-notebook-server-from-remote-machine).

### Preproduction

The preproduction notebooks are used for R&D studies before the legacy MxAOD production. They are used to optimize the ML models used in the event selection and categorization of the legacy analysis, as well as to evaluate their performance in terms of some standard figures of merits (e.g. signal strength / XS upper limits and constraints on coupling parameters). The analysis inputs are the h027 MxAODs used in the ggF analysis (2021).

Some of the main tutorials are listed below.

#### Tutorial-01 Creation of skimmed NTuples from MxAODs
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T01-MxAOD_to_NTuples.ipynb)

#### Tutorial-02 Conversion of NTuples to Array Data (in csv format)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T02-NTuples_to_Csv.ipynb)

#### Tutorial-03 BDT Model Training
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T03-ModelTraining.ipynb)

#### Tutorial-06 Hyperparameter Optimization of BDT Models
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T06-HyperparameterOptimization.ipynb)

#### Tutorial-05 (Reproducing) Statistical Evaluation of h027 ggF analysis (assumes mH = 125.09 GeV)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T05-Reproduce_Baseline_h026.ipynb)

#### Tutorial-12 Statistical Evaluation of h027 legacy analysis  (assumes mH = 125.09 GeV)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T12-LegacyAnalysis.ipynb)

#### Tutorial-15 Statistical Evaluation starting from ROOT worksapces  (assumes mH = 125.09 GeV)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/preproduction/T15-StatEvaluationFromWorkspace.ipynb)

### Postproduction

The postproduction notebooks aimed at producing official results for the legacy analysis with finalized definition of event categories. The inputs used are from [h027-yybb-fix MxAODs]() with event categories defined by the BDT models obtained from the preproduction stage.

Please feel free to participate in the [Legacy HH -> bbyy cutflow challenge](https://docs.google.com/spreadsheets/d/1w2pj0VELYHmaQzetU9GnJfzOKKPG_nntDPLpSB39BOg)!

Status of h027-yybb-fix MxAOD production can be found in [this spreadsheet](https://docs.google.com/spreadsheets/d/1-p7sOvRJz8CdERNA_d0aSm2MnDXtN1wjfQNvca3R6IM).

The postproduction notebooks follow a similar workflow as the preproduction notebooks with slight modifications to account for the use of the legacy h027-yybb MxAODs. Below are the notebooks that should guide you to deriving the final statistical results for the legacy analysis.

#### Tutorial-01 Creation of skimmed NTuples from MxAODs
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/T01-MxAOD_to_NTuples.ipynb)

#### Tutorial-02 Conversion of NTuples to Array Data (in csv format)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/T02-NTuples_to_Csv.ipynb)

#### Tutorial-03 Statistical Evaluation of h027 legacy analysis  (assumes mH = 125.09 GeV)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/T03-StatisticalEvaluation.ipynb)

#### Tutorial-04 Statistical Evaluation of h027 legacy analysis with alternative mH assumption (assumes mH = 125 GeV)
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/T04-AlternativeHiggsMassAssumption.ipynb)

#### Auxiliary Tutorials - The Cutflow Challenge
* [![Open In Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/yybb/bbyy_analysis_framework/-/blob/master/notebooks/Aux-CutflowChallenge.ipynb)

## Command Line Interface

The bbyy analysis framework offers a command line interface in the form of an executable binary called the `bbyyTool` which allows execution of framework pipelines in a remote shell.

To use the interface, one should first source the `setup.sh` script to setup the necessary environment variables.

To display a help message of the tool:
```
bbyyTool --help
```

There are multiple subcommands from the tool offering functionalities at different stages of the analysis pipeline. A detailed description of each subcommand is shown below.

### Creation of Skimmed NTuples from MxAODs

```
bbyyTool create_ntuples --sample_config <path-to-sample-config-file> --processor_config <path-to-processor-config-file> ...
```

Required Options

| **Option** | **Description** | **Examples** |
| ---------- | ---------- | ----------- | 
| `--sample_config` | Path to the sample configuration file. | - |
| `--processor_config` | Path to the processor configuration file. | - |

Additional Options

| **Option** | **Description** | **Default** | **Examples** |
| ---------- | ---------- | ----------- | ----------- | 
| `--samples` | MxAOD samples to process (separated by commas). All samples defined in the sample config will be processed by default. | None | "ggH,VBF,ttH" |
| `--sample_types` | Types of MxAOD samples to process (separated by commas). All sample types defined in the sample config will be processed by default. | None | "mc16a,mc16d" |
| `--merge` | Ntuple (and cutflow) samples to merge (separated by commas). All merged samples defined in the sample config will be processed by default. | None | "ggH,ZH,ttyy" |
| `--do-process/--skip-process` | Perform the MxAOD processing step. | True | - |
| `--do-merge/--skip-merge` | Perform the ntuple merging step. | True | - |
| `--do-cutflow/--skip-cutflow` | Perform the cutflow merging step. | True | - |
| `--multithread/--no-multithread` | se multithreading. | True | - |
| `--processor_flags` | Processor flags to set (separated by commas). | None | - |
| `--outdir` | Output directory. | "outputs" | - |
| `--verbosity` | Verbosity level. Choose from "DEBUG", "INFO", "WARNING" or "ERROR". | "INFO" | - |

### Create ggF signal Ntuples with extended klambda Weight Branches

```
bbyyTool create_reweight_samples --reweight_config <path-to-reweight-config-file> --base_sample <name-of-sample-used-as-basis> --reference_sample <name-of-output-sample-with-extended-weight-branches> ...
```

Required Options

| **Option** | **Description** | **Examples** |
| ---------- | ---------- | ----------- | 
| `--reweight_config` | Path to the sample configuration file. | - |
| `--base_sample` | Path to the processor configuration file. | "HHbbyy_cHHH01d0" |
| `--reference_sample` | Path to the processor configuration file. | "HHbbyy_ggF_reweighting" |

Additional Options

| **Option** | **Description** | **Default** | **Examples** |
| ---------- | ---------- | ----------- | ----------- | 
| `--treename` | Tree name of ntuples. | "output" | - |
| `--ntuple_dir` | Path the the output (ntuple) directory. | "outputs/ntuples" | - |
| `--verbosity` | Verbosity level. Choose from "DEBUG", "INFO", "WARNING" or "ERROR". | "INFO" | - |


### Conversion of NTuples to Array Data (CSV Files)

```
bbyyTool convert_ntuples --config <path-to-analysis-config-file> --processor_config <path-to-processor-config-file> ...
```

Required Options

| **Option** | **Description** | **Examples** |
| ---------- | ---------- | ----------- | 
| `--config` | Path to the analysis configuration file. | - |

Additional Options

| **Option** | **Description** | **Default** | **Examples** |
| ---------- | ---------- | ----------- | ----------- | 
| `--columns` | Set of columns to save. Choose between "h027" and "h027-legacy". | "h027-legacy" | - |
| `--samples` | Ntuple samples to convert (separated by commas). All samples defined in the analysis config will be converted by default. | "outputs/ntuples" | None |
| `--do-conversion/--skip-conversion` | Perform the ntuple conversion. | True | - |
| `--do-reweight/--skip-reweight` | Create reweighted sample in the converted file format.| True | - |
| `--reweight_values` | Values of klambda for which the reweighted samples should be produced (separated by commas). The main base sample defined in the config will be used. | None | "0,5.6" |
| `--alt_reweight_values` | Alaternative values of klambda for which the reweighted samples should be produced (separated by commas). The alternative base sample defined in the config will be used. | None | "20" |
| `--ntuple_dir` | Path the the output (ntuple) directory. Use path from config by default. | None | - |
| `--array_dir` | Path the the output (array) directory. Use path from config by default. | None | - |
| `--verbosity` | Verbosity level. Choose from "DEBUG", "INFO", "WARNING" or "ERROR". | "INFO" | - |


### Statistical Evaluation

```
bbyyTool stat_eval -i <path-to-analysis-config>
```

Required Options

| **Option** | **Description** | **Examples** |
| ---------- | ---------- | ----------- | 
| `--config` | Path to the analysis configuration file. | - |

Additional Options

| **Option** | **Description** | **Default** | **Examples** |
| ---------- | ---------- | ----------- | ----------- | 
| `--study_name` | Name of the study. | "h027_legacy" | - |
| `--channels` | Analysis channels to include (separated by commas). | "SM,BSM" | - |
| `--output_mode` | Categorization output mode. Choose from "minimal", "minitree", "array", "standard", "full". | "standard" | - |
| `--do-categorization/--skip-categorization` | Whether to perform the categorization step. This includes evaluation of event scores, score boundary scans, application of event categories and creation of categorized outputs (yields table, minitrees and csv files). | True | - |
| `--do-signal-modelling/--skip-signal-modelling` | Whether to perform the singal modelling step. | True | - |
| `--do-signal-param/--skip-signal-param` | Whether to perform the ggF and VBF signal parameterization step. | True | - |
| `--do-workspace-creation/--skip-workspace-creation` | Whether to perform the workspace (and xml) creation step. | True | - |
| `--do-limit-setting/--skip-limit-setting` | Whether to evaluate limits. | True | - |
| `--do-likelihood-scan/--skip-likelihood-scan` | Whether to run likelihood scans. | True | - |
| `--plot-limit-scan/--skip-plot-limit-scan` | Whether to produce plots for limit scans. | True | - |
| `--plot-likelihood-scan/--skip-plot-likelihood-scan` | Whether to produce plots for likelihood scans. | True | - |
| `--plot-discriminant-fit/--skip-plot-discriminant-fit` | Whether to produce plots for fits on the discriminant variable. | True | - |
| `--plot-score-distribution/--skip-plot-score-distribution` | Whether to produce plots on the score distribution in each analysis channel. | True | - |
| `--limit_tasks` | Limit setting tasks to run (separated by commas). Allowed tasks: sm_limit, sm_vbf_limit, kl_scan, k2v_scan, k2v_vbf_scan | "sm_limit, sm_vbf_limit, kl_scan, k2v_scan, k2v_vbf_scan" | - |
| `--likelihood_tasks` | Likelihood scan tasks to run (separated by commas). Allowed tasks: kl_scan, k2v_scan, kl_k2v_scan | "kl_scan, k2v_scan" | - |
| `--higgs_mass` | Assumptions of Higgs mass value to use (separated by commas) in limit setting/likelihood scans. If more than one is given, they will be evaluated separately. | "125,125.09" | - |
| `--boundary_dir` | Path to the directory containing the boundary information. To be used together with cache-boundary. If not given, the path will be inferred from the analysis config. | None | - |
| `--cache/--no-cache` | Whether to cache limit and likelihood outputs. | False | - |
| `--cache-score/--no-cache-score` | Whether to cache limit and likelihood outputs. | False | - |
| `--cache-boundary/--no-cache-boundary` | Whether to skip evaluation of event scores (use if event scores are already available in the input). | False | - |
| `--cache-category/--no-cache-category` | Whether to skip evaluation of event category based on existing event scores (use if event category is already available in the input) | False | - |
| `--extra_variables` | Additional variables (separated by commas) to be saved in the categorized minitrees. | None | - |


### Systematics Evaluation

Under construction...

## Epic Command Lines for Reproducing Legacy Analysis Results

```
# change your output directory here
BBYY_OUTDIR=${PWD}

bbyyTool create_ntuples --sample_config ${BBYYFRAMEWORKDIR}/configs/MxAOD/sample_config/MxAOD_sample_config_h027_legacy.json --processor_config ${BBYYFRAMEWORKDIR}/configs/MxAOD/process_config/MxAOD_process_config_h027_legacy.txt -o ${BBYY_OUTDIR}

bbyyTool create_reweight_samples -i ${BBYYFRAMEWORKDIR}/configs/parameter_reweighting/klambda_reweight_config_basis_kl1p0.json -b HHbbyy_cHHH01d0 -r HHbbyy_ggF_reweighting -o ${BBYY_OUTDIR}/ntuples

bbyyTool create_reweight_samples -i ${BBYYFRAMEWORKDIR}/configs/parameter_reweighting/klambda_reweight_config_basis_kl10p0.json -b HHbbyy_cHHH10d0 -r HHbbyy_ggF_reweighting_alt -o ${BBYY_OUTDIR}/ntuples

bbyyTool convert_ntuples -i ${BBYYFRAMEWORKDIR}/configs/analysis_config_h027_legacy_mH_125p0_GeV.json -c h027-legacy --reweight_values 0,5.6 --alt_reweight_values=20 --ntuple_dir ${BBYY_OUTDIR}/ntuples --array_dir  ${BBYY_OUTDIR}/arrays

bbyyTool stat_eval -i ${BBYYFRAMEWORKDIR}/configs/analysis_config_h027_legacy_mH_125p0_GeV.json --study_name h027_legacy --boundary_dir ${BBYYFRAMEWORKDIR}/configs/score_boundaries --cache-score --cache-category --cache-boundary --extra_variables "jet1_truth_id, jet2_truth_id, run_number, mc_channel_number, HTXS_Stage1_2_Category_pTjet30, HTXS_Stage1_2_Fine_Category_pTjet30" --array_dir  ${BBYY_OUTDIR}/arrays --outdir ${BBYY_OUTDIR}/outputs
```


## How to Run Jupyter Notebook Server from Remote Machine

If you want to run jupyter notebooks in a remote server (e.g. lxplus), you may use port-forwarding to connect localhost (your local machine) to the jupyter server.

Port-forwarding is done when you log into your remote server via ssh. The general syntax is:
```
ssh -L <port>:<host>:<hostport> <username>@<remote-server>
```

Here `<port>` is the TCP port on the local host `<host>` which is to be forwarded to the port `<hostport>` on the remote host . A typical setup will be
```
ssh -L 9005:localohst:9005 <username>@<remote-server>
```

This means you forward the port `9005` from your local host (your local machine) to the port `9005` on the remove host (the remove server).

After port-forwarding, you may setup the software environment with Jupyter notebook installed. Then launch a jupyter server using
```
jupyter notebook --no-browser --port <port>
```

In order for the jupyter notebook to be launchable from your local machine, you must have `<port> = <hostport>`.

Notice that some port values are reserved for other purposes. To check the list of ports in-use, try running
```
ss -tulw
```

Normally, port values in the range 9000 to 9999 are safe to use.

**Note**: If you find out the port launched by the jupyter server is different from what you specified, e.g. you typed 9005 but 9006 was shown in the jupyter server, it means the port you typed are already used by other users or yourself (when you forget to shutdown the previous notebook server). In that case, please try a different port.

To shut down a jupyter server, press "CTRL+C" (Windows keyboard) or "COMMAND+C" (Mac) in the remote server then type "y" and enter. Remember to shutdown all opened notebook kernels before you shut down the server.

### TLDR

1. SSH into lxplus with port forwarding, e.g.
```
ssh -L 9005:localhost:9005 <username>@lxplus.cern.ch
```

2. Setup software environment where jupyter is installed.

3. Launch jupyter notebook server with
```
jupyter notebook --no-browser --port 9005
```

4. Once finished, terminate the server by pressing "CTRL+C" (Windows keyboard) or "COMMAND+C" (Mac) in the remote server then type "y" and enter. Remember to shutdown all opened notebook kernels before you shut down the server.

### Advanced Usage: Multi-port Forwarding with Layers of Remote Servers

In some cases, one may want to have multiple layers of ssh sessions. For example, one can first log-in to lxplus, then to computing clusters from your institute that is hosted at CERN. In that case, you will need multi-port forwarding to launch jupyer server.

For example:

1. Log into your first remote server by forwarding a source port from your local host to an intermediate port from the first remote server
```
ssh -L 9005:localhost:5005 <username>@lxplus.cern.ch
```

2. Log into the second remote server from the first remote server, while forwarding the intermediate port on the first remote server to a destination port on the second remote server
```
ssh -L 5005:localhost:9005 <username>@<computing_cluster_server>
```

**Note**: The source port must be same as destination port for jupyter server to launch properly. That is you want the local host to be able to communicate with the remote host (from second server) through a sequence of linked ports.

3. Setup software environment where jupyter is installed.

4. Launch jupyter notebook server with
```
jupyter notebook --no-browser --port 9005
```


## Useful Links

### Twiki Page

- HH->bbyy full Run 2 ggF analysis: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DiHiggsTobbyyRun2ggF
- Legacy HH->bbyy full Run 2 analysis: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DiHiggsTobbyyVBF
- HGam MxAODs: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/H027LegacyProduction 
