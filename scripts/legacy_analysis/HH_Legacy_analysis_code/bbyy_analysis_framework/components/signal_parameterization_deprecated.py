from typing import Dict, List, Optional, Union
import os
import copy
import json

import numpy as np
import pandas as pd

from quickstats.utils.common_utils import combine_dict
from quickstats.maths.numerics import str_decode_value
from bbyy_analysis_framework.components import AnalysisBase

class SignalParameterization(AnalysisBase):
    
    TASKS = ("ggF", "VBF")
    
    def __init__(self, analysis_config_path:str,
                 study_name:Optional[str]=None,
                 outdir:Optional[str]=None,
                 tasks:Optional[List[str]]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super().__init__(analysis_config=analysis_config_path,
                         outdir=outdir,
                         verbosity=verbosity,
                         **kwargs)
        if study_name is not None:
            self.set_study_name(study_name)
        
        self.setup_tasks(tasks)
    
    def setup_tasks(self, tasks:Optional[List[str]]=None):
        
        self.param_configs = {}
        self.param_data = {}
        self.param_data_err = {}
        self.validation_data = {}
        
        if tasks is None:
            tasks = self.TASKS
            
        for task in tasks:
            if f"{task}_param" not in self.config:
                raise RuntimeError(f'missing config for {task} parameterisation')
            self.param_configs[task] = combine_dict(self.config[f"{task}_param"])
            self.param_data[task] = None
            self.validation_data[task] = None
            
        self.tasks = tasks
 
    @staticmethod
    def run_polynomial_fit(degree:int,
                           reference_points:Dict[str, str],
                           sample_yields:Dict,
                           param_yields:Dict,
                           coeff_expr:Dict):
        coefficients = {}
        categories = list(sample_yields)
        for category in categories:
            coefficients[category] = {}
            data = {'parameter': [], 'yield':[]}
            for param_str, param_yield in param_yields[category].items():
                data['parameter'].append(str_decode_value(param_str))
                data['yield'].append(param_yield)
            for param_str, sample in reference_points.items():
                data['parameter'].append(str_decode_value(param_str))
                data['yield'].append(sample_yields[category][sample])                
            df = pd.DataFrame(data).sort_values(by=['parameter'])
            x_arr = df['parameter'].values
            y_arr = df['yield'].values
            if len(x_arr) < 3:
                raise RuntimeError("Not enough points for polynomial fit parameterisation")
            fit_result = np.polyfit(x_arr, y_arr, degree)[::-1]
            for i, coeff_name in enumerate(coeff_expr):
                coefficients[category][coeff_name] = fit_result[i]
        return coefficients
    
    @staticmethod
    def run_linear_combination(reference_points:Dict[str, str],
                               sample_yields:Dict,
                               coeff_expr:List[str]):
        categories = list(sample_yields)
        data = {"category": []}
        for point in reference_points:
            data[f"yield_{point}"] = []
        for category in categories:
            data["category"].append(category)
            for point, sample in reference_points.items():
                data[f"yield_{point}"].append(sample_yields[category][sample])
        df = pd.DataFrame(data)
        for key in coeff_expr:
            expr = coeff_expr[key].replace("{", "yield_").replace("}", "")
            df[key] = df.eval(expr)
        coefficients = df.set_index(["category"])[list(coeff_expr)].to_dict("index")
        return coefficients
    
    @staticmethod
    def eval_linear_combination_err(reference_points:Dict[str, str],
                                    sample_yields_err:Dict,
                                    coeff_expr:List[str]):
        import sympy as sp
        from sympy.parsing.sympy_parser import parse_expr
        from quickstats.maths.symbolics import get_coeff_dict
        categories = list(sample_yields_err)
        reference_symbols = {}
        for point, sample in reference_points.items():
            symbol = sp.symbols(f"x_{point}")
            reference_symbols[symbol] = sample
        sympy_expr = {}
        for coeff, expr in coeff_expr.items():
            sympy_expr[coeff] = parse_expr(expr.replace("{", "x_").replace("}", ""))
        coeff_err = {}
        for coeff, expr in sympy_expr.items():
            const_terms = get_coeff_dict(expr, *reference_symbols.keys(), strict=False, linear=True)
            for category in categories:
                if category not in coeff_err:
                    coeff_err[category] = {}
                errlo = 0.
                errhi = 0.
                for symbol in const_terms:
                    sample = reference_symbols[symbol]
                    errlo += (sample_yields_err[category][sample]["errlo"] * float(const_terms[symbol])) ** 2
                    errhi += (sample_yields_err[category][sample]["errhi"] * float(const_terms[symbol])) ** 2
                errlo = np.sqrt(errlo)
                errhi = np.sqrt(errhi)
                coeff_err[category][coeff] = {"errlo": errlo, "errhi": errhi}
        return coeff_err                                    
    
    @staticmethod
    def get_validation_data(param_data:Dict,
                            validation_points:Dict,
                            yield_data:Dict,
                            yield_expr:str,
                            yield_err_data:Optional[Dict]=None,
                            coeff_err:Optional[Dict]=None):
        validation_data = {}
        for category in param_data:
            if category not in yield_data:
                raise RuntimeError(f"missing yield information for the category {category}")
            category_param_data = param_data[category]
            resolved_yield_expr = yield_expr
            # resolve parameter values in the yield formula
            for param, value in category_param_data.items():
                resolved_yield_expr = resolved_yield_expr.replace(param, f"{value}")
            category_validation_data = combine_dict(validation_points)
            for sample in category_validation_data:
                if coeff_err is not None:
                    if category not in coeff_err:
                        raise RuntimeError(f"missing coefficient error information for the category {category}")
                    import sympy as sp
                    from sympy.parsing.sympy_parser import parse_expr
                    from quickstats.maths.symbolics import get_coeff_dict
                    symbol_map = {}
                    param_resolved_yield_expr = yield_expr
                    for i, param_symbol in enumerate(category_param_data.keys()):
                        symbol_map[param_symbol] = sp.symbols(f"x_{i}")
                        param_resolved_yield_expr = param_resolved_yield_expr.replace(param_symbol, f"x_{i}")
                    for param, value in category_validation_data[sample].items():
                        param_resolved_yield_expr = param_resolved_yield_expr.replace(param, str(value))
                    sympy_expr = parse_expr(param_resolved_yield_expr)
                    const_terms = get_coeff_dict(sympy_expr, *symbol_map.values(), strict=False, linear=True)
                    errlo = 0.
                    errhi = 0.
                    for param_symbol in category_param_data.keys():
                        errlo += (coeff_err[category][param_symbol]["errlo"] * \
                                 float(const_terms.get(symbol_map[param_symbol], 0))) ** 2
                        errhi += (coeff_err[category][param_symbol]["errhi"] * \
                                 float(const_terms.get(symbol_map[param_symbol], 0))) ** 2
                    category_validation_data[sample]["param_yield_errlo"] = np.sqrt(errlo)
                    category_validation_data[sample]["param_yield_errhi"] = np.sqrt(errhi)
                if sample not in yield_data[category]:
                    raise RuntimeError(f"missing yield information for the sample {sample} "
                                       f"in the category {category}")
                category_validation_data[sample]["true_yield"] = yield_data[category][sample]
                if yield_err_data is not None:
                    category_validation_data[sample]["true_yield_errlo"] = yield_err_data[category][sample]["errlo"]
                    category_validation_data[sample]["true_yield_errhi"] = yield_err_data[category][sample]["errhi"]
            df = pd.DataFrame(category_validation_data).transpose()
            df["param_yield"] = df.eval(resolved_yield_expr)
            validation_data[category] = df.to_dict()
        return validation_data
    
    def run_single_task(self, task:str, eval_err:bool=True):
        param_config = self.param_configs[task]
        param_method = param_config['method']
        
        args = {
            "reference_points" : param_config['reference_points'],
            "coeff_expr"       : param_config['coefficients']
        }
        
        # load yield data
        yield_path = self.get_file("merged_yield_data")
        with open(yield_path, "r") as yield_file:  
            sample_yields = json.load(yield_file)
        args["sample_yields"] = sample_yields
        
        self.stdout.info(f"INFO: Running {task} parameterisation using the {param_method} method")
        if param_method in ["parabolic_fit", "quadratic_fit", "reweight_fit"]:
            param_yield_path = self.get_file(f"merged_yield_data_{task}_param")
            with open(param_yield_path, "r") as yield_file:
                param_yields = json.load(yield_file)
            args["param_yields"] = param_yields
            coefficients = self.run_polynomial_fit(degree=2, **args)
            coefficient_err = None
        elif param_method == "linear_combination":
            coefficients = self.run_linear_combination(**args)
            if eval_err:
                yield_err_path = self.get_file("merged_yield_err_data")
                with open(yield_err_path, "r") as yield_err_file:
                    sample_yields_err = json.load(yield_err_file)             
                coefficient_err = self.eval_linear_combination_err(param_config['reference_points'],
                                                                   sample_yields_err,
                                                                   param_config['coefficients'])
            else:
                coefficient_err = None
        else:
            raise RuntimeError(f'unsupported parameterisation method "{param_method}"')
    
        self.param_data[task] = coefficients
        self.param_data_err[task] = coefficient_err
        
        validation_points = param_config.get("validation", None)
        if validation_points is not None:
            yield_expr = param_config['expression']
            # load yield error data
            yield_err_path = self.get_file("merged_yield_err_data")
            with open(yield_err_path, "r") as yield_err_file:
                sample_yields_err = json.load(yield_err_file)
            validation_data = self.get_validation_data(coefficients, validation_points,
                                                       sample_yields, yield_expr,
                                                       sample_yields_err,
                                                       coefficient_err)
            self.validation_data[task] = validation_data
        
    def run_param(self, yield_dir:Optional[str]=None, eval_err:bool=True):
        temp_dir = self.path_manager.directories["yield"]
        if yield_dir is not None:
            self.path_manager.set_directory("yield", yield_dir, absolute=True)
        
        for task in self.tasks:
            self.run_single_task(task, eval_err=eval_err)
            
        # restore yield directory in case a custom yield_directory is set
        self.path_manager.set_directory("yield", temp_dir)
        
    def save_param(self, save_dir:Optional[str]=None):
        temp_dir = self.path_manager.directories["signal_param"]
        if save_dir is not None:
            self.path_manager.set_directory("signal_param", save_dir, absolute=True)

        for task in self.param_data:
            if self.param_data[task] is None:
                raise RuntimeError(f"{task} parameterisation not done")
            # create save directory if not already exists
            self.path_manager.makedir_for_files([f"{task}_param"])
        
            param_savepath = self.get_file(f"{task}_param")
            with open(param_savepath, "w") as outfile:
                json.dump(self.param_data[task], outfile, indent=2)
            self.stdout.info(f'INFO: Saved {task} parameterization data to "{param_savepath}"')
            
            param_err_savepath = self.get_file(f"{task}_param_err")
            if self.param_data_err[task] is not None:
                with open(param_err_savepath, "w") as outfile:
                    json.dump(self.param_data_err[task], outfile, indent=2)
                self.stdout.info(f'INFO: Saved {task} parameterization error data to "{param_err_savepath}"')            
        
            # save VBF validation data
            if self.validation_data[task] is not None:
                validation_savepath = self.get_file(f"{task}_validation_data")
                with open(validation_savepath, "w") as outfile:
                    json.dump(self.validation_data[task], outfile, indent=2)
                self.stdout.info(f"INFO: Saved {task} validation data to `{validation_savepath}`")
            
        # restore yield directory in case a custom yield_directory is set
        self.path_manager.set_directory("signal_param", temp_dir)