from typing import Dict, List, Optional, Union, Tuple
import os

from quickstats.analysis import AnalysisBase as QSAnalysisBase

from .analysis_path_manager import AnalysisPathManager
from .bbyy_config_format import BBYY_ANALYSIS_CONFIG_FORMAT

class AnalysisBase(QSAnalysisBase):
    
    CONFIG_FORMAT = BBYY_ANALYSIS_CONFIG_FORMAT
    
    def __init__(self, **kwargs):
        if "path_manager" not in kwargs:
            kwargs["path_manager"] = AnalysisPathManager()
        super().__init__(**kwargs)
        
    def load_analysis_config(self, config_source:Optional[Union[Dict, str]]=None):
        super().load_analysis_config(config_source)
        try:
            self.extra_samples = list(self.config['samples']['extra'])
        except:
            self.extra_samples = []
        
    def load_plot_config(self, config_source:Optional[Union[Dict, str]]=None):
        # use the default plot config from the framework
        if config_source is None:
            import bbyy_analysis_framework as fw
            config_source = os.path.join(fw.resource_path, "default_plot_config.json")
        super().load_plot_config(config_source)
        
    def get_blind_sample_name(self, sample:str):
        return f"{sample}_sideband"
    
    def is_data_sample(self, sample:str):
        """Check whether a given sample is the observed data.
        """
        return "data" in sample    