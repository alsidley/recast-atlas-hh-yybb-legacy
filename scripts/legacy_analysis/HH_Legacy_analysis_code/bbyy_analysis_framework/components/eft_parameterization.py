from typing import Dict, List, Optional, Union
import os
import copy
import json

import numpy as np
import pandas as pd

from bbyy_analysis_framework.components import AnalysisBase
from bbyy_analysis_framework.components import SMEFTReweightingTool, HEFTReweightingTool

class EFTParameterization(AnalysisBase):
    
    def __init__(self, analysis_config:Union[Dict, str],
                 outdir:Optional[str]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super().__init__(analysis_config=analysis_config,
                         outdir=outdir,
                         verbosity=verbosity,
                         **kwargs)
        
    def run_SMEFT_param(self, param_config:Union[Dict, str]):
        pass
    
    def run_HEFT_param(self, param_config:Union[Dict, str]):
        pass
    