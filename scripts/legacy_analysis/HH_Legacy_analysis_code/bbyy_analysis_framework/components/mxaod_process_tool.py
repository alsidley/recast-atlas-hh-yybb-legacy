from typing import Optional, List, Dict, Union
import os
import glob

from quickstats.analysis import NTupleProcessTool as QSNTupleProcessTool
from .bbyy_config_format import BBYY_SAMPLE_CONFIG_FORMAT

class MxAODProcessTool(QSNTupleProcessTool):
    """ Tool for processing MxAODs for the HH -> bbyy analysis
    """
    
    CONFIG_FORMAT = BBYY_SAMPLE_CONFIG_FORMAT
    
    REQUIRED_CONFIG_COMPONENTS = ["sample_dir", "sample_subdir", "samples", "merge_samples",
                                  "hist_name", "lumi", "cat_variable"]
    
    def __init__(self, sample_config:str, outdir:str='output',
                 processor_config:Optional[str]=None,
                 processor_flags:Optional[List[str]]=None,
                 use_template:bool=False,
                 multithread:bool=True,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        
        super().__init__(sample_config=sample_config,
                         outdir=outdir,
                         processor_config=processor_config,
                         processor_flags=processor_flags,
                         use_template=use_template,
                         multithread=multithread,
                         verbosity=verbosity,
                         **kwargs)     
    
    def prerun_process(self, sample_config:Dict):
        import ROOT
        sample_name = sample_config["name"]
        sample_paths = sample_config["path"]
        sample_type = sample_config["type"]       
        processor_flags = list(self.processor_flags)
        if sample_name == "data":
            processor_flags.append("isData")
            sample_type_key = 'data_year'
        else:
            processor_flags.append("isMC")
            sample_type_key = 'mc_campaign'
        self.processor.set_flags(processor_flags)
        all_files = self.get_expanded_paths(sample_paths)
        if sample_name != "data":    
            hist_name = self.sample_config['hist_name'][sample_name]
            lumi      = self.sample_config['lumi'][sample_type]
            sum1, sum2, sum3 = 0, 0, 0
            for fname in all_files:
                f = ROOT.TFile(fname)
                hist = f.Get(hist_name)
                sum1 += hist.GetBinContent(1)
                sum2 += hist.GetBinContent(2)
                sum3 += hist.GetBinContent(3)
            sum_weight = (sum1 / sum2) * sum3
            self.processor.global_variables['weight_lumi'] = lumi
            self.processor.global_variables['weight_sum']  = sum_weight
        self.processor.global_variables[sample_type_key] = sample_type
        
    def get_sample_cutflow(self, sample:str, hide_nan:bool=True, format_float:bool=True):
        if sample not in self.cutflow_report:
            raise RuntimeError(f'no cutflow report found for the sample "{sample}"')
        df = self.cutflow_report[sample]
        df = df[['name', 'yield', 'pass', 'yield_efficiency', 'yield_cumulative_efficiency']]
        df = df.rename(columns={"name": "Cutflow", "yield":"Yield", "pass":"Entries", "yield_efficiency": "Efficiency", 
                                "yield_cumulative_efficiency": "Cumul. Efficiency"})
        if format_float:
            import pandas as pd
            pd.set_option('display.float_format', lambda x: '%.5f' % x)
        if hide_nan:
            df = df.fillna("")
        return df.fillna("")