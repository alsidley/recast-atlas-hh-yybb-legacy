from typing import Dict, Optional, List, Union
import os
import json

import numpy as np
import pandas as pd

from quickstats import AbstractObject
from quickstats.maths.numerics import str_decode_value, str_encode_value
from quickstats.utils.common_utils import combine_dict

import bbyy_analysis_framework

class ParameterReweightingTool(AbstractObject):
    
    CONFIG = {
        "reweight_filename"      : "dummy", # file containing the weight information used for reweighting
        "branch_name_observable" : "dummy", # branch name of the observable used in reweighting
        "extra_branches"         : "all",   # extra branches to save in the output, use "all" to save all branches
        "observable_scale"       : 1        # scale factor applied to the observable values
    }
    
    def __init__(self, config:Optional[Union[Dict, str]]=None,
                 verbosity:Optional[Union[int, str]]="INFO"):
        
        super().__init__(verbosity=verbosity)
        
        if isinstance(config, str):
            config = json.load(open(config, 'r'))
        
        self.config = combine_dict(self.CONFIG, config)
        
        self.initialize()
        
    def initialize(self):
        raise NotImplementedError
        
    def get_branch_data(self, filename:str, treename:str="CollectionTree",
                        extra_branches:Optional[List[str]]=None):
        """
        Extract branch data from a root file in the form of numpy arrays
        
        Parameters:
            filename: str
                Name of input root file.
            treename: str
                Name of tree in the root file containing the branch data.
            extra_branches: (optional) list of str
                Names of branches beside the reweighting variable to extract from the root file.
        
        """
        from quickstats.utils.data_conversion import root2numpy
        columns = [self.config["branch_name_observable"]]
        if extra_branches is not None:
            if isinstance(extra_branches, str) and extra_branches.lower() == "all":
                columns = None
            else:
                columns.extend(extra_branches)
        branch_data = root2numpy(filename, treename, columns=columns, mode=2, multithread=False)
        return branch_data
    
    def get_reweight_branch_data(self, observable_values:np.ndarray, **kwargs):
        raise NotImplementedError
    
    def run(self, infile:str, outfile:str,
            treename:str="CollectionTree", **kwargs):
        extra_branches = self.config["extra_branches"]
        self.stdout.info(f'INFO: Loading branch data from "{infile}"')
        branch_data = self.get_branch_data(infile, treename,
                                           extra_branches=extra_branches)
        observable_name   = self.config["branch_name_observable"]
        observable_scale  = self.config.get("observable_scale", 1)
        observable_values = branch_data[observable_name] * observable_scale
        self.stdout.info(f'INFO: Filling reweight information to output branches')
        reweight_branch_data = self.get_reweight_branch_data(observable_values, **kwargs)
        output_branch_data = {}
        if extra_branches is not None:
            if isinstance(extra_branches, str) and extra_branches.lower() == "all":
                output_branch_data.update(branch_data)
            else:
                for extra_branch in extra_branches:
                    output_branch_data[extra_branch] = branch_data[extra_branch]
        output_branch_data.update(reweight_branch_data)
        from quickstats.utils.data_conversion import array2root
        self.stdout.info(f'INFO: Saving output branches to "{outfile}"')
        array2root(output_branch_data, outfile, treename, multithread=False)    