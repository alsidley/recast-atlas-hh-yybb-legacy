import math
from math import remainder, tau

import numpy as np

def zero_pad(array, size):
    new_array = np.zeros(size)
    new_array[:array.shape[0]] = array[:size]
    return new_array

def getTopness(x):
    import ROOT
    jets = []
    for j in range(x['jet_pt'].size):
        jets.append(ROOT.TLorentzVector())
        jets[j].SetPtEtaPhiE(x['jet_pt'][j]/100000, x['jet_eta'][j], x['jet_phi'][j], x['jet_E'][j]/100000)
    if 3 - x['jet_pt'].size > 0:
        for j in range(3 - x['jet_pt'].size): 
            jets.append(ROOT.TLorentzVector(0, 0, 0, 0))

    min_singletopness = -1

    for i in range(len(jets)): #bjet index
        for j in range(len(jets)): #ljet1 index
            if (i == j): 
                continue
            for k in range(len(jets)): #ljet2 index
                if (i == k or j >= k): 
                    continue
                singletopness = ((jets[j] + jets[k]).M()/0.80 - 1)**2 + ((jets[i] + jets[j] + jets[k]).M()/1.73 - 1)**2

                if (min_singletopness == -1 or singletopness < min_singletopness): 
                    min_singletopness = singletopness

    return math.sqrt(min_singletopness)

def getZihangVBFVariables(x, xgb_model):
    import ROOT
    import xgboost as xgb
    import pandas as pd
    jets = []
    jet_indices = []
    for j in range(x['jet_pt'].size):
        if (j == x['jet_index'][0]) or (j == x['jet_index'][1]):
            continue
        jet4vec = ROOT.TLorentzVector()
        jet4vec.SetPtEtaPhiE(x['jet_pt'][j], x['jet_eta'][j], x['jet_phi'][j], x['jet_E'][j])
        jets.append(jet4vec)
        jet_indices.append(j)
    max_jjscore = 0
    vbf_jj_deta = -999
    vbf_jj_m    = -999
    vbf_j1_index = -1
    vbf_j2_index = -1
    n_jet = len(jets)
    if n_jet >= 2 :
        HT = x["HT_uncorrected"]
        yybb = ROOT.TLorentzVector()
        yybb.SetPtEtaPhiM(x['yybb_pt'], x['yybb_eta'], x['yybb_phi'], x['yybb_M'])
        vbf_jj_deta_ij = []
        vbf_jj_m_ij  = []
        vbf_j1_idx_ij = []
        vbf_j2_idx_ij = []
        xgb_inputs = []
        for i in range(n_jet): #VBF jet 1 index
            for j in range(i + 1, n_jet): #VBF jet 2 index
                if (jets[i].Pt() >= jets[j].Pt()):
                    jet1_index = i
                    jet2_index = j
                    vbf_jet1 = jets[i]
                    vbf_jet2 = jets[j]
                else:
                    jet1_index = j
                    jet2_index = i                    
                    vbf_jet1 = jets[j]
                    vbf_jet2 = jets[i]
                vbf_jets = vbf_jet1 + vbf_jet2
                vbf_mjj  = vbf_jets.M()
                vbf_deta = abs(vbf_jet1.Eta() - vbf_jet2.Eta())
                dR_yybb_vbfj1 = vbf_jet1.DeltaR(yybb)
                dR_yybb_vbfj2 = vbf_jet2.DeltaR(yybb)
                deta_yybb_vbfj1 = abs(vbf_jet1.Eta() - yybb.Eta())
                deta_yybb_vbfj2 = abs(vbf_jet2.Eta() - yybb.Eta())
                dR_yybb_jj = vbf_jets.DeltaR(yybb)
                deta_yybb_jj = abs(vbf_jets.Eta() - yybb.Eta())
                yybbjj = yybb + vbf_jets
                pT_yybbjj = yybbjj.Pt()
                eta_yybbjj = yybbjj.Eta()
                phi_yybbjj = yybbjj.Phi()
                m_yybbjj = yybbjj.M()
                xgb_input = [HT, dR_yybb_vbfj1, dR_yybb_vbfj2, deta_yybb_vbfj1, deta_yybb_vbfj2,
                             dR_yybb_jj, deta_yybb_jj, pT_yybbjj, eta_yybbjj, m_yybbjj,
                             vbf_jet1.Pt(), vbf_jet2.Pt(), vbf_jet1.Eta(), vbf_jet2.Eta(), vbf_mjj, vbf_deta]
                xgb_inputs.append(xgb_input)
                vbf_j1_idx_ij.append(jet1_index)
                vbf_j2_idx_ij.append(jet2_index)
                vbf_jj_m_ij.append(vbf_mjj)
                vbf_jj_deta_ij.append(vbf_deta)
        dmatrix  = xgb.DMatrix(xgb_inputs)
        jjscores = xgb_model.predict(dmatrix)
        idx = np.argmax(jjscores)
        max_jjscore = jjscores[idx]
        vbf_jj_deta = vbf_jj_deta_ij[idx]
        vbf_jj_m    = vbf_jj_m_ij[idx]
        vbf_j1_index = jet_indices[vbf_j1_idx_ij[idx]]
        vbf_j2_index = jet_indices[vbf_j2_idx_ij[idx]]
    return pd.Series([vbf_jj_deta, vbf_jj_m, max_jjscore, vbf_j1_index, vbf_j2_index])