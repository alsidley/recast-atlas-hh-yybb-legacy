from typing import Optional, Union, Dict, List, Tuple
import os

from quickstats.analysis import AnalysisPathManager as QSAnalysisPathManager

class AnalysisPathManager(QSAnalysisPathManager):
    
    DEFAULT_DIRECTORIES = {
        "categorized_array"     : "categorized_arrays",
        "categorized_minitree"  : "categorized_minitrees",
        "categorized_histogram" : "categorized_histograms",
        "yield"                 : "yields",
        "summary"               : "summary",
        "plot"                  : "plots",
        "xml"                   : "xmls",
        "data"                  : "data",
        "workspace"             : "xmls/workspace",
        "xml_config"            : "xmls/config",
        "xml_data"              : "xmls/config/data",
        "xml_model"             : "xmls/config/models",
        "xml_category"          : "xmls/config/categories",
        "xml_systematics"       : "xmls/config/systematics",
        "limit"                 : "limits",
        "likelihood"            : "likelihood",
        "signal_model"          : "signal_modelling",
        "signal_param"          : "summary"
    }

    DEFAULT_FILES = {
        # analysis samples
        "ntuple_sample"                            : ("ntuple", "{sample}.root"),
        "train_sample"                             : ("array", "{sample}.{fmt}"),
        "array_sample"                             : ("array", "{sample}.{fmt}"),
        # categorized analysis samples
        "categorized_array_sample"                 : ("categorized_array", "{sample}_{category}.{fmt}"),
        "categorized_minitree_sample"              : ("categorized_minitree", "{sample}_{category}.root"),
        "categorized_histogram_sample"             : ("categorized_histogram", "{sample}_{category}.root"),
        # machine learning model
        "kfold_model"                              : ("model", "{channel}_{fold}.h5"),
        "basic_model"                              : ("model", "{channel}.h5"),        
        # yield data
        "merged_yield_data"                        : ("yield", "yields.json"),
        "merged_yield_err_data"                    : ("yield", "yields_err.json"),
        "merged_yield_data_ggF_param"              : ("yield", "yields_ggF_param.json"),
        "merged_yield_err_data_ggF_param"          : ("yield", "yields_err_ggF_param.json"),
        "yield_data"                               : ("yield", "yields_{category}.json"),
        "yield_err_data"                           : ("yield", "yields_err_{category}.json"),
        "yield_data_ggF_param"                     : ("yield", "yields_{category}_ggF_param.json"),
        "yield_err_data_ggF_param"                 : ("yield", "yields_err_{category}_ggF_param.json"),
        # signal modelling data
        "model_parameter"                          : ("signal_model", "model_parameters.json"),
        "signal_modelling_data"                    : ("signal_model", "model_parameters.json"),
        "signal_modelling_summary"                 : ("signal_model", "model_summary_{category}.json"),
        # summary data
        "category_summary"                         : ("summary", "category_summary_{channel}.json"),
        "boundary_data"                            : ("summary", "boundary_tree_{channel}.json"),
        "benchmark_significance"                   : ("summary", "benchmark_significance.json"),
        "VBF_validation_data"                      : ("summary", "VBF_validation.json"),
        "ggF_validation_data"                      : ("summary", "ggF_validation.json"), 
        "ggF_param"                                : ("summary", "ggF_param.json"),
        "VBF_param"                                : ("summary", "VBF_param.json"),
        "ggF_param_err"                            : ("summary", "ggF_param_err.json"),
        "VBF_param_err"                            : ("summary", "VBF_param_err.json"),
        "limit_summary"                            : ("summary", "limit_summary.json"),
        "likelihood_summary"                       : ("summary", "likelihood_summary.json"),
        "limit_summary_mH"                         : ("summary", "limit_summary_mH_{mH_value}.json"),
        "likelihood_summary_mH"                    : ("summary", "likelihood_summary_mH_{mH_value}.json"),
        # xml workspace related
        "data_point"                               : ("data", "data_points_{category}.txt"),
        "input_xml_non_param"                      : ("xml_config", "input_non_param.xml"),
        "input_xml_param"                          : ("xml_config", "input_param.xml"),
        "xml_data_point"                           : ("xml_data", "data_points_{category}.txt"),
        "xml_yy_histogram"                         : ("xml_data", "yy_{category}.root"),
        "model_xml"                                : ("xml_model", "{sample}_{category}.xml"),
        "yield_systematics_xml"                    : ("xml_systematics", "syst_yield_{sample}_{category}.xml"),
        "shape_systematics_xml"                    : ("xml_systematics", "syst_shape_{sample}_{category}.xml"),
        "category_xml_non_param"                   : ("xml_category", "category_non_param_{category}.xml"),
        "category_xml_param"                       : ("xml_category", "category_param_{category}.xml"),
        "workspace_non_param"                      : ("workspace", "WS-bbyy-non-resonant-non-param.root"),
        "workspace_param"                          : ("workspace", "WS-bbyy-non-resonant-param.root"),
        "workspace_asimov_param"                   : ("workspace", "WS-bbyy-non-resonant-param-asimov.root"),
        "workspace_non_param_mH"                   : ("workspace", "WS-bbyy-non-resonant-non-param-mH-{mH_value}.root"),
        "workspace_param_mH"                       : ("workspace", "WS-bbyy-non-resonant-param-mH-{mH_value}.root"),
        "workspace_asimov_param_mH"                : ("workspace", "WS-bbyy-non-resonant-param-asimov-mH-{mH_value}.root"),
        # statistical result
        "likelihood_data"                          : ("likelihood", "likelihoods_{target}.json"),        
        "limit_data"                               : ("limit", "limits_{target}.json"),
        "likelihood_data_mH"                       : ("likelihood", "likelihoods_{target}_mH_{mH_value}.json"),        
        "limit_data_mH"                            : ("limit", "limits_{target}_mH_{mH_value}.json"),
        # plots
        "score_distribution_plot"                  : ("plot", "score_distribution_{channel}.pdf"),
        "variable_distribution_plot"               : ("plot", "distribution_{variable}_{category}.pdf"),
        "signal_modelling_plot"                    : ("plot", "modelling_{category}.pdf"),
        "VBF_validation_plot"                      : ("plot", "VBF_validation_{category}.pdf"),
        "ggF_validation_plot"                      : ("plot", "ggF_validation_{category}.pdf"), 
        "limit_scan_plot"                          : ("plot", "{target}_limit_scan.pdf"),
        "likelihood_scan_plot"                     : ("plot", "{target}_likelihood_scan.pdf"),
        "limit_scan_plot_mH"                       : ("plot", "{target}_limit_scan_mH_{mH_value}.pdf"),
        "likelihood_scan_plot_mH"                  : ("plot", "{target}_likelihood_scan_mH_{mH_value}.pdf"),        
        "discriminant_fit_plot"                    : ("plot", "discriminant_fit.pdf"),
        "total_bkg_discriminant_fit_plot"          : ("plot", "total_bkg_discriminant_fit.pdf"),
        "total_bkg_cont_bkg_discriminant_fit_plot" : ("plot", "total_bkg_continuum_bkg_discriminant_fit.pdf")
    }