from typing import Dict, Optional, List, Union
import os
import glob
import json
import time
import types

import pandas as pd
import numpy as np
from sklearn.metrics import roc_curve, auc
import sklearn

from quickstats import Timer
from quickstats.maths.numerics import is_nan_or_inf
from quickstats.analysis.data_loading import KFoldSplitAlgo

from bbyy_analysis_framework.components import DataLoader, EventCategorization


class BDTTrainer(DataLoader):
    
    REQUIRED_CONFIG_COMPONENTS = ["paths:arrays", "paths:models",
                                  "samples:*", "variables:*",
                                  "training:*", "channels:*",
                                  "names:weight", "names:event_number"]
    
    def __init__(self, study_name:str, analysis_config:Union[Dict, str],
                 array_dir:Optional[str]=None,
                 model_dir:Optional[str]=None,
                 categorizer:Optional[EventCategorization]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super().__init__(analysis_config=analysis_config,
                         array_dir=array_dir,
                         model_dir=model_dir,
                         verbosity=verbosity,
                         **kwargs)
        # append the study name to the output directory
        self.set_study_name(study_name)
        self.categorizer = categorizer
        self.reset()
        
    def save_models(self):
        self.path_manager.makedirs(["model"])
        study_name = self.get_study_name()
        for channel, model in self.models.items():
            if isinstance(model, list):
                for i, model_i in enumerate(model):
                    model_savepath = self.get_file("kfold_model", channel=channel, fold=i)
                    model_i.save_model(model_savepath)
                    self.stdout.info(f"INFO: Saved {study_name} model (fold {i}) for the channel "
                                     f"{channel} as {model_savepath}")
            else:
                model_savepath = self.get_file("basic_model", channel=channel)
                model.save_model(model_savepath)
                self.stdout.info(f"INFO: Saved {study_name} model for the channel "
                                 f"{channel} as {model_savepath}")
                
    def reset(self):
        self.models = {}
        self.results = {}
        self.eval_results = {}
        self.index_train = {}
        self.index_eval = {}
    
    def train_single_BDT(self, data:Dict, hyperparameters:Optional[Dict]=None,
                         num_rounds:int=3000, early_stopping_rounds:int=20,
                         verbose:bool=True):
        import xgboost as xgb
        dTrain = xgb.DMatrix(data['x_train'], label=data['y_train'], weight=data['weight_train'].values)
        dVal   = xgb.DMatrix(data['x_val'], label=data['y_val'], weight=data['weight_val'].values)
        #dTest  = xgb.DMatrix(data['x_test'], label=data['y_test'], weight=data['weight_test'].values)
        if hyperparameters is None:
            hyperparameters = {}
        evallist  = [(dTrain, 'train'), (dVal, 'eval')]
        evals_result = {}
        eval_result_history = []
        model = xgb.train(hyperparameters, 
                          dTrain, 
                          num_rounds, 
                          evals=evallist, 
                          early_stopping_rounds=early_stopping_rounds, 
                          evals_result=evals_result,
                          verbose_eval=verbose)
        results = {}
        metrics = list(evals_result['eval'])
        primary_metric = metrics[0]
        best_index = np.argmax(evals_result['eval'][primary_metric])
        for metric in metrics:
            results[metric] = evals_result['eval'][metric][best_index]
        return model, results, evals_result
    
    def train(self, channel:str, scale_factors:Optional[Dict]=None,
              hyperparameters:Optional[Dict]=None,
              num_rounds:int=3000,
              custom_indices:Optional[Dict[str, Dict[str, np.ndarray]]]=None,
              verbose:bool=True):
        if hyperparameters is None:
            hyperparameters = self.config['channels'][channel]['hyperparameters']
        import xgboost as xgb
        train_data = self.load_channel_train_data(channel, scale_factors=scale_factors,
                                                  custom_indices=custom_indices)
        cross_validation = isinstance(self.split_algo, KFoldSplitAlgo)
        if cross_validation:
            assert isinstance(train_data, types.GeneratorType)
            self.stdout.info(f"INFO: Begin BDT training with the cross-validation method.")
            data_sequence = train_data
        else:
            self.stdout.info(f"INFO: Begin BDT training with the standard train-val-test method.")
            data_sequence = [train_data]
        models, results, eval_results, index_train, index_eval = [], [], [], [], []
        with Timer() as t:
            for i, data_i in enumerate(data_sequence):
                if cross_validation:
                    self.stdout.info(f"INFO: Cross-validation fold {i + 1}")
                model, result, eval_result = self.train_single_BDT(data_i, hyperparameters,
                                                                   num_rounds=num_rounds,
                                                                   verbose=verbose)
                if cross_validation:
                    index_train.append(data_i['index_train'])
                    index_eval.append(data_i['index_val'])
                models.append(model)
                if self.categorizer is not None:
                    benchmarks = list(self.config['benchmark']['counting_significance'])
                    try:
                        self.categorizer.set_models({channel: model})
                        cat_indices = None
                        if cross_validation:
                            # change categorization event indices to those selected from k-fold
                            cat_indices = data_i["index_val"]
                        if custom_indices:
                            if cross_validation:
                                raise RuntimeError("cannot use custom event indices and k-fold "
                                                   "generated event indices at the same time")
                            cat_indices = custom_indices
                        self.categorizer.run_channel_categorization(channel, cache_boundaries=False,
                                                                    event_indices=cat_indices)
                        Z_benchmarks = self.categorizer.benchmark_significance
                        for name in Z_benchmarks:
                            result[name] = Z_benchmarks[name][f"{channel}_combined"]
                    except Exception as e:
                        if self.stdout.verbosity == "DEBUG":
                            raise RuntimeError(f"{e}")
                        self.stdout.error(f"{e}")
                        for name in benchmarks:
                            result[name] = 0.
                    if any(is_nan_or_inf(result[name]) for name in benchmarks):
                        for name in benchmarks:
                            result[name] = 0.
                results.append(result)
                eval_results.append(eval_result)
            if len(models) == 1:
                self.models[channel]  = models[0]
                self.results[channel] = results[0]
                self.eval_results[channel] = eval_results[0]
                if index_train and index_eval:
                    self.index_train[channel] = index_train[0]
                    self.index_eval[channel] = index_eval[0]
            else:
                self.models[channel]  = models
                self.results[channel] = results
                self.eval_results[channel] = eval_results
                self.index_train[channel] = index_train
                self.index_eval[channel] = index_eval
        self.stdout.info(f"INFO: BDT training finished in {t.interval}s.")
        return self.results[channel]