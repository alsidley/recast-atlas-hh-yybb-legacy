from typing import Union, List, Optional, Dict
from math import remainder, tau
from itertools import repeat
import os
import sys

import numpy as np
import pandas as pd

from quickstats import semistaticmethod
from quickstats.analysis import NTupleConversionTool as QSNTupleConversionTool
from quickstats.maths.numerics import str_encode_value
from .kinematics import zero_pad #,getTopness

from bbyy_analysis_framework.components import AnalysisBase

class NTupleConversionTool(AnalysisBase, QSNTupleConversionTool):
    
    REQUIRED_CONFIG_COMPONENTS = ["paths:*", "kl_reweighting:*", "names:tree_name"]
    
    DEFAULT_COLUMNS = {
        'event_number': 'eventNumber',
        'weight': 'weight',
        'mass_yy': 'mass_yy',
        'yy_pt': 'yy_pt',
        'rel_photon1_pt': 'photon_pt[0]/mass_yy',
        'photon1_eta': 'photon_eta[0]', 
        'photon1_phi': 'photon_phi[0]',
        'rel_photon2_pt': 'photon_pt[1]/mass_yy',
        'photon2_eta': 'photon_eta[1]',
        'photon2_phi': 'photon_phi[1]',
        'met_pt': 'met_pt',
        'met_phi': 'met_phi',
        'jet1_pt': 'jet_bcal_pt[bjet_bcal_index[0]]',
        'jet1_eta': 'jet_bcal_eta[bjet_bcal_index[0]]',
        'jet1_phi': 'jet_bcal_phi[bjet_bcal_index[0]]',
        'jet1_E': 'jet_bcal_E[bjet_bcal_index[0]]',
        'jet1_btag': 'jet_bcal_pseudo_score[bjet_bcal_index[0]] >= -1 ? 6 - jet_bcal_pseudo_score[bjet_bcal_index[0]] : -9',
        'jet2_pt':'jet_bcal_pt[bjet_bcal_index[1]]',
        'jet2_eta':'jet_bcal_eta[bjet_bcal_index[1]]',
        'jet2_phi':'jet_bcal_phi[bjet_bcal_index[1]]',
        'jet2_E': 'jet_bcal_E[bjet_bcal_index[1]]',
        'jet2_btag':'jet_bcal_pseudo_score[bjet_bcal_index[1]] >= -1 ? 6 - jet_bcal_pseudo_score[bjet_bcal_index[1]] : -9',
        'bb_pt': 'bb_bcal_pt',
        'bb_eta': 'bb_bcal_eta',
        'bb_phi': 'bb_bcal_phi',
        'bb_M': 'bb_bcal_M',
        'HT': 'HT_jet_bcal',
        'jet_pt': 'jet_bcal_pt',
        'jet_eta':'jet_bcal_eta',
        'jet_phi': 'jet_bcal_phi',
        'jet_E': 'jet_bcal_E',
        'jet_btag': 'jet_bcal_pseudo_score',
        'jet_index': 'bjet_bcal_index',
        'mHH': 'mass_yybb_bcal_mod',
        'ggF_cat': 'yybb_nonRes_XGBoost_BCal_Cat',
        'SM_score': 'yybb_nonRes_XGBoost_BCal_withTop_highMass_Score',
        'BSM_score': 'yybb_nonRes_XGBoost_BCal_withTop_lowMass_Score',
        'dR_jj': 'dR_jj',
        'dR_yy': 'dR_yy',
        'm_yyjj': 'm_yyjj',    
        'sphericityT': 'sphericityT',
        'planarFlow': 'planarFlow',
        'pt_balance': 'pt_balance',
        'vbf_jj_deta': 'vbf_jj_deta',
        'vbf_jj_m': 'vbf_jj_m',
        'single_topness': 'singleTopness',
        'truth_m_hh': 'm_hh',
        'jet1_truth_id': 'HadronConeExclTruthLabelID[bjet_bcal_index[0]]',
        'jet2_truth_id': 'HadronConeExclTruthLabelID[bjet_bcal_index[1]]',
        'run_number': 'runNumber',
        'mc_channel_number': 'mcChannelNumber',
        'HTXS_Stage1_2_Category_pTjet30': 'HTXS_Stage1_2_Category_pTjet30',
        'HTXS_Stage1_2_Fine_Category_pTjet30': 'HTXS_Stage1_2_Fine_Category_pTjet30'        
    }
    
    DEFAULT_COLUMNS_LEGACY = {
        'event_number': 'eventNumber',
        'weight': 'weight',
        'mass_yy': 'mass_yy',
        'yy_pt': 'yy_pt',
        'rel_photon1_pt': 'photon_pt[0]/mass_yy',
        'photon1_eta': 'photon_eta[0]', 
        'photon1_phi': 'photon_phi[0]',
        'rel_photon2_pt': 'photon_pt[1]/mass_yy',
        'photon2_eta': 'photon_eta[1]',
        'photon2_phi': 'photon_phi[1]',
        'met_pt': 'met_pt',
        'met_phi': 'met_phi',
        'jet1_pt': 'jet_bcal_pt[bjet_bcal_index[0]]',
        'jet1_eta': 'jet_bcal_eta[bjet_bcal_index[0]]',
        'jet1_phi': 'jet_bcal_phi[bjet_bcal_index[0]]',
        'jet1_E': 'jet_bcal_E[bjet_bcal_index[0]]',
        'jet1_btag': 'jet_bcal_pseudo_score[bjet_bcal_index[0]] >= -1 ? 6 - jet_bcal_pseudo_score[bjet_bcal_index[0]] : -9',
        'jet2_pt':'jet_bcal_pt[bjet_bcal_index[1]]',
        'jet2_eta':'jet_bcal_eta[bjet_bcal_index[1]]',
        'jet2_phi':'jet_bcal_phi[bjet_bcal_index[1]]',
        'jet2_E': 'jet_bcal_E[bjet_bcal_index[1]]',
        'jet2_btag':'jet_bcal_pseudo_score[bjet_bcal_index[1]] >= -1 ? 6 - jet_bcal_pseudo_score[bjet_bcal_index[1]] : -9',
        'bb_pt': 'bb_bcal_pt',
        'bb_eta': 'bb_bcal_eta',
        'bb_phi': 'bb_bcal_phi',
        'bb_M': 'bb_bcal_M',
        'HT': 'HT_jet_bcal',
        'jet_pt': 'jet_bcal_pt',
        'jet_eta':'jet_bcal_eta',
        'jet_phi': 'jet_bcal_phi',
        'jet_E': 'jet_bcal_E',
        'jet_btag': 'jet_bcal_pseudo_score',
        'jet_index': 'bjet_bcal_index',
        'mHH': 'mass_yybb_bcal_mod',
        'cat': 'yybb_nonRes_XGBoost_BCal_Cat',
        'score': 'mass_yybb_bcal_mod >= 350000 ? yybb_nonRes_XGBoost_BCal_highMass_Score : yybb_nonRes_XGBoost_BCal_lowMass_Score',
        'max_jjscore': 'yybb_BCal_max_jjscore',
        'dR_jj': 'dR_jj',
        'dR_yy': 'dR_yy',
        'm_yyjj': 'm_yyjj',    
        'sphericityT': 'sphericityT',
        'planarFlow': 'planarFlow',
        'pt_balance': 'pt_balance',
        'vbf_jj_deta': 'vbf_jj_deta',
        'vbf_jj_m': 'vbf_jj_m',
        'single_topness': 'singleTopness',
        'truth_m_hh': 'm_hh',
        'jet1_truth_id': 'HadronConeExclTruthLabelID[bjet_bcal_index[0]]',
        'jet2_truth_id': 'HadronConeExclTruthLabelID[bjet_bcal_index[1]]',
        'run_number': 'runNumber',
        'mc_channel_number': 'mcChannelNumber',
        'HTXS_Stage1_2_Category_pTjet30': 'HTXS_Stage1_2_Category_pTjet30',
        'HTXS_Stage1_2_Fine_Category_pTjet30': 'HTXS_Stage1_2_Fine_Category_pTjet30'        
    }

    def __init__(self, analysis_config:Optional[Union[Dict, str]]=None,
                 ntuple_dir:Optional[str]=None,
                 array_dir:Optional[str]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super(NTupleConversionTool, self).__init__(analysis_config=analysis_config,
                                                   ntuple_dir=ntuple_dir,
                                                   array_dir=array_dir,                         
                                                   verbosity=verbosity,
                                                   **kwargs)
        rescale_weights = self.config.get("rescale_weights", {})
        self.weight_scales = rescale_weights.get("ntuple_conversion", None)
        
    def convert_samples(self, samples:List[str]=None,
                        kinematic_regions:Optional[List]=None,
                        columns:Optional[Union[List, Dict]]=None,
                        apply_columns:Optional[Dict]=None,
                        drop_columns:Optional[List]=None,
                        library:str="quickstats",
                        fmt:Optional[str]=None,
                        downcast:Optional[bool]=None,
                        complevel:int=None,
                        chunksize:int=100000,
                        parallel:int=0,
                        save_train_val_test:bool=False,
                        **kwargs):
        
        weight_scales = self.config.get("rescale_weights", None)
        super().convert_samples(samples=samples,
                                kinematic_regions=kinematic_regions,
                                columns=columns,
                                apply_columns=apply_columns,
                                drop_columns=drop_columns,
                                library=library,
                                fmt=fmt,
                                downcast=downcast,
                                complevel=complevel,
                                chunksize=chunksize,
                                parallel=parallel,
                                weight_scales=weight_scales,
                                save_train_val_test=save_train_val_test,
                                **kwargs)     
    
    @semistaticmethod
    def _postprocess(self, df, **kwargs):
        # Get more jet variables
        max_jet = 4
        jet_pt   = np.array([zero_pad(df['jet_pt'][i][indices], max_jet) for i, indices in enumerate(df['jet_index'])]) 
        jet_eta  = np.array([zero_pad(df['jet_eta'][i][indices], max_jet) for i, indices in enumerate(df['jet_index'])]) 
        jet_phi  = np.array([zero_pad(df['jet_phi'][i][indices], max_jet) for i, indices in enumerate(df['jet_index'])]) 
        jet_E    = np.array([zero_pad(df['jet_E'][i][indices], max_jet) for i, indices in enumerate(df['jet_index'])]) 
        jet_btag = np.array([zero_pad(df['jet_btag'][i][indices], max_jet) for i, indices in enumerate(df['jet_index'])])

        if max_jet > 2:
            for i in range(2, max_jet):
                jet_name = f"jet{i+1}"
                df[f'{jet_name}_pt']   = jet_pt[:, i]
                df[f'{jet_name}_eta']  = jet_eta[:, i]
                df[f'{jet_name}_phi']  = jet_phi[:, i]
                df[f'{jet_name}_E']  = jet_E[:, i]
                df[f'{jet_name}_btag'] = jet_btag[:, i]
                bjet_mask = (df[f'{jet_name}_btag'] != 0) & (df[f'{jet_name}_btag'] >= - 1)
                df.loc[bjet_mask, f'{jet_name}_btag']  = 6 - df[bjet_mask][f'{jet_name}_btag']
                df.loc[~bjet_mask, f'{jet_name}_btag'] = -9

        # Rotate all phi angles according to photon 1 phi
        df['temp'] = df['photon1_phi']
        for col in df.columns:
            if ('phi' in col and 'jet_phi' != col):
                if 'jet' not in col:
                    df[col] = df.apply(lambda x: remainder(x[col] - x['temp'], tau), axis = 1)
                else:
                    pt_col = col.replace("phi", "pt")
                    df[col] = df.apply(lambda x: remainder(x[col] - x['temp'], tau) if x[pt_col] != 0 else 0, axis = 1)
                    
        df = df.drop('temp', axis = 1)
        df = df.drop('jet_pt', axis = 1)
        df = df.drop('jet_eta', axis = 1)
        df = df.drop('jet_phi', axis = 1)
        df = df.drop('jet_E', axis = 1)
        df = df.drop('jet_btag', axis = 1)
        df = df.drop('jet_index', axis = 1)

        # rescale the weight for a given sample
        weight_scales = kwargs.get("weight_scales", None)
        sample_name = kwargs["sample_name"]
        if (weight_scales is not None) and (sample_name in weight_scales):
            df['weight'] = df['weight'] * weight_scales[sample_name]
        return df
    
    @semistaticmethod
    def _finalize(self, df, **kwargs):
        save_train_val_test = kwargs["save_train_val_test"]
        if not save_train_val_test:
            return None
        iteration   = kwargs["iteration"]
        fmt         = kwargs["fmt"]
        complevel   = kwargs["complevel"]
        outdir      = kwargs["outdir"]
        sample_name = kwargs["sample_name"]
        df_train, df_val, df_test = self._get_train_val_test_dataframes(df)
        for df_i, label in [(df_train, "train"), (df_val, "val"), (df_test, "test")]:
            outpath = self.get_array_sample_path(sample_name=f"{sample_name}_{label}",
                                                 dirname=outdir, fmt=fmt)
            if iteration == 0:
                self.stdout.info(f'INFO: Saving data array as "{outpath}"')
                mode = 'w'
            else:
                mode = 'a'
            self._save(df_i, outpath=outpath, fmt=fmt, mode=mode, complevel=complevel)
    
    @staticmethod
    def _read_array(filename:str, fmt:str):
        assert fmt in ["csv", "h5"]
        if fmt == "csv":
            return pd.read_csv(filename)
        elif fmt == "h5":
            return pd.read_hdf(filename)
        else:
            return None
    
    @staticmethod
    def _get_train_val_test_dataframes(df):
        df_train = df.query('event_number % 4 <= 1',  engine='python')
        df_val   = df.query('event_number % 4 == 2',  engine='python')
        df_test  = df.query('event_number % 4 == 3',  engine='python')
        return df_train, df_val, df_test
            
    def create_ggF_reweighting_reference_sample(self, reweight_options:Optional[str]=None,
                                                fmt:Optional[str]=None, complevel:int=None,
                                                save_train_val_test:bool=False):
        if fmt is None:
            fmt = self.get_analysis_data_format()
        if reweight_options is None:
            reweight_options = self.config["kl_reweighting"]
        # basis sample
        base_sample            = reweight_options['base_sample']
        base_sample_array_path = self.get_file("train_sample", sample=base_sample, validate=True, fmt=fmt)
        # sample containing reweighting factors
        reference_sample             = reweight_options['reference_sample']
        reference_sample_ntuple_path = self.get_file("ntuple_sample", sample=reference_sample, validate=True)
        reference_sample_array_path  = self.get_file("train_sample", sample=reference_sample, fmt=fmt)
        self.stdout.info(f'INFO: Processing file "{reference_sample_ntuple_path}"')
        
        # make sure the event number between the two samples match
        from quickstats.utils.data_conversion import root2dataframe      
        reference_sample_df = root2dataframe(reference_sample_ntuple_path, self.treename, library="root",
                                             multithread=False)
        base_sample_df = self._read_array(base_sample_array_path, fmt)
        event_match = np.array_equal(reference_sample_df['eventNumber'].values, base_sample_df['event_number'].values)
        if not event_match:
            raise RuntimeError("event number mismatch between ggF basis sample and reference sample for kl reweighting")
        # drop columns containing error information
        columns_to_drop = ['eventNumber'] + [c for c in reference_sample_df.columns if '_err_' in c]
        reference_sample_df = reference_sample_df.drop(columns_to_drop, axis=1)
        # we can simply concat without sorting since the event order is identical between the two dataframes
        df = pd.concat([base_sample_df, reference_sample_df], axis=1)
        iterables = [(df, "")]
        if save_train_val_test:
            df_train, df_val, df_test = self._get_train_val_test_dataframes(df)
            iterables.extend([(df_train, "_train"), (df_val, "_val"), (df_test, "_test")])
        array_dir = self.get_directory("array")
        for df_i, label in iterables:
            outpath = self.get_array_sample_path(sample_name=f"{reference_sample}{label}",
                                                 dirname=array_dir, fmt=fmt)
            self.stdout.info(f'INFO: Saving data array as "{outpath}"')
            self._save(df_i, outpath, fmt=fmt, mode="w", complevel=complevel)      
    
    def create_ggF_reweighted_sample(self, klambda:float, reweight_options:Optional[str]=None,
                                     fmt:Optional[str]=None, complevel:int=None,
                                     save_train_val_test:bool=False):
        if fmt is None:
            fmt = self.get_analysis_data_format()
        if reweight_options is None:
            reweight_options = self.config["kl_reweighting"]
        reference_sample = reweight_options["reference_sample"]
        reference_sample_array_path  = self.get_file("train_sample", sample=reference_sample, fmt=fmt)
        reference_sample_df = self._read_array(reference_sample_array_path, fmt)
        # the column names are hard-coded
        columns_to_drop = [c for c in reference_sample_df.columns if 'crossSectionBR' in c or 'kappa_lambda_weight' in c]
        klambda_str = str_encode_value(round(klambda, 8))
        weight_expr = reweight_options["weight"].format(param_val=klambda_str)
        # apply reweighting factor
        reference_sample_df["weight"] = reference_sample_df["weight"] * reference_sample_df.eval(weight_expr)
        df = reference_sample_df.drop(columns_to_drop, axis=1)
        iterables = [(df, "")]
        if save_train_val_test:
            df_train, df_val, df_test = self._get_train_val_test_dataframes(df)
            iterables.extend([(df_train, "_train"), (df_val, "_val"), (df_test, "_test")])
        reweighted_sample = reweight_options['reweighted_sample'].format(param_val=klambda_str)
        array_dir = self.get_directory("array")
        for df_i, label in iterables:
            outpath = self.get_array_sample_path(sample_name=f"{reweighted_sample}{label}",
                                                 dirname=array_dir, fmt=fmt)
            self.stdout.info(f'INFO: Saving data array as "{outpath}"')
            self._save(df_i, outpath, fmt=fmt, mode="w", complevel=complevel)
