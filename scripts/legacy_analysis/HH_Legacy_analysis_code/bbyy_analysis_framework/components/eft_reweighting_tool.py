from typing import Dict, Optional, List, Union
import os
import re
import json

import numpy as np
import pandas as pd

from quickstats import AbstractObject
from quickstats.maths.numerics import str_decode_value, str_encode_value
from quickstats.utils.common_utils import combine_dict

import bbyy_analysis_framework
from .parameter_reweighting_tool import ParameterReweightingTool

class EFTReweightingTool(ParameterReweightingTool):
    
    CONFIG = {
        "reweight_filename"      : "dummy", # file containing the weight information used for reweighting
        "branch_name_observable" : "dummy", # branch name of the observable used in reweighting
        "extra_branches"         : "all",   # extra branches to save in the output, use "all" to save all branches
        "branch_name_weight"     : "HEFT_weight.{param_expr}", # branch name of the EFT weight variable to be added
        "observable_scale"       : 1        # scale factor applied to the observable values
    }
    
    EFT_MODEL = "EFT"
    
    PARAMETER_SET = ()
    
    POLYNOMIAL = "DUMMY"
        
    def initialize(self):
        data_path = bbyy_analysis_framework.data_path
        filename  = os.path.join(data_path, self.config["reweight_filename"])
        if not os.path.exists(filename):
            raise FileNotFoundError(f"missing weight file for {self.EFT_MODEL} reweighting: {filename}")
        self.stdout.info(f'INFO: Initializing reweight information from "{filename}"')
        with open(filename, "r") as file:
            data = json.load(file)
        df = pd.DataFrame(data["weights"]).set_index("bins")
        self.reweight_data      = df
        self.reweight_bin_range = data["bin_range"]
        self.reweight_bin_width = data["bin_width"]
        
    def get_param_expr(self, eft_parameters:Dict[str, float], precision:int=3):
        """Return the string representation of the EFT parameters and their values
        """
        expressions = [f"{param}_{str_encode_value(value)}" for param, value in eft_parameters.items()]
        expressions = sorted(expressions)
        param_expr = "_".join(expressions)
        return param_expr
    
    def get_bin_centers(self):
        """Return the bin centers of the observable bins used for reweighting
        """        
        bin_range, bin_width = self.reweight_bin_range, self.reweight_bin_width
        bin_centers = np.arange(bin_range[0], bin_range[1] + bin_width, bin_width)
        return bin_centers
    
    def get_bin_indices(self, x:np.ndarray):
        """
        Calculate the corresponding reweight bin indices for a given set of observable values
           
        Parameters:
            x: numpy array of float
                Values of observable based on which the bin indices are calculated.
        """
        bin_centers = self.get_bin_centers()
        bins = bin_centers + bin_width / 2
        # make the max bin large enough so no value will fall in the overflow bin
        bins[-1] = 1e9
        bin_indices =  np.digitize(x, bins)
        return bin_indices
    
    def get_bin_weights(self, x:np.ndarray, weights:Optional[np.ndarray]=None):
        """
        Calculate the weight in each observable bin for a given set of observable values
        
        Parameters:
            x: numpy array of float
                Values of observable based on which the bin weights are calculated.
            weights: numpy array of float
                Weight of the observable values.
        """
        bin_centers = self.get_bin_centers()
        bins = bin_centers + bin_width / 2
        bins = np.concatenate([[-1e9], bins])
        bins[-1] = 1e9
        bin_weights, bin_edges = np.histogram(x, bins=bins, weights=weights)
        return dict(zip(bin_centers, bin_weights))
    
    def get_Rhh(self, observable_bin:Optional[float], eft_parameters:Dict[str, float]) -> float:
        """Return Rhh value for the given observable bin
        """
        raise NotImplementedError
    
    def get_Rhh_map(self, eft_parameters:Dict[str, float]):
        """Return a map between the observable bin value and the corresponding Rhh value
        """
        self.validate_eft_parameters(eft_parameters)
        df = self.reweight_data.copy()
        for param in self.PARAMETER_SET:
            df[param] = eft_parameters[param]
        Rhh_map = df.eval(self.POLYNOMIAL).to_dict()
        return Rhh_map
    
    def get_Rhh_array(self, eft_parameters:Dict[str, float]):
        """Return an array of Rhh values, with array index matching the observable bin index
        """
        Rhh_map = self.get_Rhh_map(eft_parameters)        
        bin_centers = self.get_bin_centers()
        Rhh_array = np.array([-1] * len(bin_centers), dtype="float32")
        for i, bin_center in enumerate(bin_center):
            bin_center = round(bin_center, 8)
            if bin_center in Rhh_map:
                Rhh_array[i] = Rhh_map[bin_center]
        return Rhh_array
            
    def get_Rhh_expr_map(self):
        variable_regex = re.compile(r"\b[a-zA-Z]\w+\b")
        variables = variable_regex.findall(self.POLYNOMIAL)
        coefficients = sorted(set(variables) - set(self.PARAMETER_SET))
        coefficient_regex = {c:re.compile(r"\b" + c + r"\b") for c in coefficients}
        Rhh_expr_map = {}
        for bin_center in self.reweight_data.index:
            polynomial = self.POLYNOMIAL
            series = self.reweight_data.loc[bin_center]
            for coefficient, regex in coefficient_regex.items():
                polynomial = regex.sub(f"{series[coefficient]}", polynomial)
            Rhh_expr_map[bin_center] = polynomial
        return Rhh_expr_map        
    
    def get_eft_weights(self, x:np.ndarray, eft_parameters:Dict[str, float]):
        """Return the eft weights corresponding to the observable values
        
        Parameters:
            x: numpy array of float
                Values of observable based on which the bin weights are calculated.      
        """
        bin_indices = self.get_bin_indices(x)
        Rhh_array = self.get_Rhh_array(eft_parameters)
        eft_weights = Rhh_array[bin_indices]
        if len(eft_weights[eft_weights < 0]) > 0:
            raise RuntimeError("found observable bin without polynomial weights")
        return eft_weights
            
    def get_den_weight(self, eft_parameters:Dict[str, float]):
        """Return the density weight
        """
        if "inclusive" in self.reweight_data:
            den_weight = self.get_Rhh(None, eft_parameters)
        else:
            den_weight = 1
        return den_weight            
    
    def validate_eft_parameters(self, eft_parameters:Dict[str, float]):
        if set(eft_parameters.keys()) != set(self.PARAMETER_SET):
            raise ValueError(f"invalid {self.EFT_MODEL} parameters received; "
                             f"required parameters: {', '.join(self.PARAMETER_SET)}")
            
    def _get_reweight_branch_data(self, observable_values:np.ndarray,
                                  eft_parameters:List[Dict[str, float]]):
        raise NotImplementedError
            
    def get_reweight_branch_data(self, observable_values:np.ndarray,
                                 eft_parameters:List[Dict[str, float]]):
        branch_data = {}
        for parameters in eft_parameters:
            branch_data_i = self._get_reweight_branch_data(observable_values, parameters)
            branch_data.update(branch_data_i)
        return branch_data
    
    def run(self, eft_parameters:Union[List[Dict[str, float]], Dict[str, float]],
            infile:str, outfile:str,
            treename:str="CollectionTree"):
        if not isinstance(eft_parameters, list):
            eft_parameters = [eft_parameters]
        for parameters in eft_parameters:
            self.validate_eft_parameters(parameters)
        super().run(infile=infile, outfile=outfile,
                    treename=treename, eft_parameters=eft_parameters)