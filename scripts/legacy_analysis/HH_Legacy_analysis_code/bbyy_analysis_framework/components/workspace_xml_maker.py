from typing import Optional, List, Dict, Union
import os
import json
import glob

import numpy as np

from quickstats.utils.xml_tools import TXMLTree
from quickstats.utils.roofit_utils import translate_formula
from quickstats.maths.numerics import is_float

from bbyy_analysis_framework.components import AnalysisBase

class WorkspaceXMLMaker(AnalysisBase):
    def __init__(self, categories:List[str],
                 analysis_config:Union[Dict, str],
                 outdir:Optional[str]=None,
                 do_blind:bool=True,
                 systematics:Optional[Dict]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super().__init__(analysis_config=analysis_config,
                         outdir=outdir,
                         verbosity=verbosity,
                         **kwargs)
        self.initialize(categories, do_blind=do_blind,
                        systematics=systematics)
        
    def initialize(self, categories:List[str], do_blind:bool,
                   systematics:Optional[Dict]=None):
        
        self.categories = categories
        
        # load sample yields
        yield_path = self.get_file("merged_yield_data", validate=True)
        with open(yield_path, "r") as file:
            sample_yields = json.load(file)
        workspace_sample_yields = {}
        sample_map = self.config['workspace']['sample_map']
        for category in categories:
            workspace_sample_yields[category] = {}
            for old_name, new_name in sample_map.items():
                workspace_sample_yields[category][new_name] = sample_yields[category][old_name]
        self.sample_yields = workspace_sample_yields
        
        # load model parameters
        model_parameter_path = self.get_file("model_parameter", validate=True)
        with open(model_parameter_path, "r") as file:
            model_parameters = json.load(file)
        self.model_parameters = model_parameters
            
        # load paramterisation coefficients
        ggF_param_path = self.get_file("ggF_param", validate=True)
        VBF_param_path = self.get_file("VBF_param", validate=True)
        with open(ggF_param_path, "r") as file:
            ggF_param_data = json.load(file)
        with open(VBF_param_path, "r") as file:
            VBF_param_data = json.load(file)
        ggF_sample = self.config['ggF_param']['workspace_sample']
        VBF_sample = self.config['VBF_param']['workspace_sample']
        self.param_config = {}
        self.param_definition = {}
        self.add_yield_parameterization(ggF_sample, ggF_param_data)
        self.add_yield_parameterization(VBF_sample, VBF_param_data)
        self.param_sample_yields = {}
        param_sample_map = self.config['workspace'].get('param_sample_map', {})
        for category in categories:
            self.param_sample_yields[category] = {}
            param_samples = ggF_param_data['sample'] + VBF_param_data['sample']
            for old_name in param_samples:
                new_name = param_sample_map.get(old_name, old_name)
                self.param_sample_yields[category][new_name] = sample_yields[category][old_name]
        self.dataset_source = "obs_data"

        self.do_blind = do_blind
        
        self.systematics = systematics
        
    def add_yield_parameterization(self, orig_sample:str, param_data:Dict):
        param_sample_map = self.config['workspace'].get('param_sample_map', {})
        self.param_config[orig_sample] = {}
        for basis_sample, expression in zip(param_data['sample'], param_data['expression']):
            sample_name = param_sample_map.get(basis_sample, basis_sample)
            formula_name = f"coeff_{sample_name}"
            translated_expr, _ = translate_formula(formula_name, expression)
            self.param_config[orig_sample][sample_name] = translated_expr                    
        param_data = self.config["parameterization"]
        for parameter in param_data:
            param_nominal = param_data[parameter]['nominal']
            if 'scan_range' in param_data[parameter]:
                param_min = min(param_data[parameter]['min'], param_data[parameter]['scan_range'][0])
                param_max = max(param_data[parameter]['max'], param_data[parameter]['scan_range'][1])
            else:
                param_min = param_data[parameter]['min']
                param_max = param_data[parameter]['max']
            self.param_definition[parameter] = f"{parameter}[{param_nominal},{param_min},{param_max}]"
            
    def get_parameterised_yields(self, category:str):
        param_data = self.config["parameterization"]
        parameterised_yields = {}       
        for sample, config in self.param_config[category].items():
            param_expr = config['expression']
            param_coeff = config['coefficients']
            n_coeff = len(param_coeff)
            params = config['parameters']
            n_params = len(params)
            var_components = []
            for i, (name, value) in enumerate(param_coeff.items()):
                param_expr = param_expr.replace(name, f"@{i}")
                var_components.append(f"{sample}_{name}[{value}]")
            for i, name in enumerate(params):
                param_expr = param_expr.replace(name, f"@{i + n_coeff}")
                param_nominal = param_data[name]['nominal']
                if 'scan_range' in param_data[name]:
                    param_min = min(param_data[name]['min'], param_data[name]['scan_range'][0])
                    param_max = max(param_data[name]['max'], param_data[name]['scan_range'][1])
                else:
                    param_min = param_data[name]['min']
                    param_max = param_data[name]['max']
                var_components.append(f"{name}[{param_nominal},{param_min},{param_max}]")
            param_str_1 = param_expr
            param_str_2 = ", ".join(var_components)
            param_str_combined = f"expr::yield_{sample}(\'{param_str_1}\', {param_str_2})"
            parameterised_yields[sample] = param_str_combined
        return parameterised_yields
    
    def is_H_sample(self, sample:str):
        return (('H' in sample) or (sample == "VBF")) and ('HH' not in sample)
    
    def is_HH_sample(self, sample:str):
        return 'HH' in sample
    
    # hard-coded for now
    def get_HH_ggF_sample_name(self):
        return self.config['workspace']['sample_map']['HHbbyy_cHHH01d0']
    
    # hard-coded for now
    def get_HH_VBF_sample_name(self):
        return self.config['workspace']['sample_map']['hh_bbyy_vbf_l1cvv1cv1']
    
    def is_HH_ggF_sample(self, sample:str):
        return 'ggFHH' in sample
    
    def is_HH_VBF_sample(self, sample:str):
        return 'VBFHH' in sample
    
    def get_xml_relpath(self, filename:str, validate:bool=False, **kwargs):
        file_path = self.get_file(filename, validate=validate, **kwargs)
        xml_path = self.get_directory("xml")
        relpath = os.path.relpath(file_path, xml_path)
        return relpath
    
    def get_workspace_sample_name(self, sample:str):
        sample_map = self.config['workspace']['sample_map']
        return sample_map.get(sample, None)
    
    def get_original_sample_name(self, sample:str):
        sample_map = self.config['workspace']['sample_map']
        for orig_name, workspace_name in sample_map.items():
            if sample == workspace_name:
                return orig_name
        return sample
    
    def get_sample_systematics(self, category:str, sample:str, syst_type:str):
        if self.systematics is None:
            return None
        if syst_type not in self.systematics:
            return None
        if category not in self.systematics[syst_type]:
            return None
        orig_sample_name = self.get_original_sample_name(sample)
        if orig_sample_name not in self.systematics[syst_type][category]:
            return None
        sample_systematics = self.systematics[syst_type][category][orig_sample_name]
        if not sample_systematics:
            return None
        return sample_systematics
    
    def has_yield_systematics(self, category:str, sample:str):
        return self.get_sample_systematics(category, sample, 'yield') is not None
    
    def has_scale_systematics(self, category:str, sample:str):
        return self.get_sample_systematics(category, sample, 'scale') is not None
    
    def has_resolution_systematics(self, category:str, sample:str):
        return self.get_sample_systematics(category, sample, 'resolution') is not None
    
    def has_shape_systematics(self, category:str, sample:str):
        return (self.has_scale_systematics(category, sample) or 
                self.has_resolution_systematics(category, sample))
    
    def has_spurious_signal_systematics(self, category:str, sample:str):
        return self.get_sample_systematics(category, sample, 'spurious_signal') is not None
    
    def add_sample_node(self, xml, sample:str, category:str,
                        model_xml_path:str, sample_yield:float,
                        param_expr:Optional[str]=None,
                        extra_norm_factors:Optional[List[str]]=None):
        kwargs = {
            "Name": sample,
            "XSection": "1",
            "SelectionEff": "1",
            "InputFile": model_xml_path,
        }
        syst_sample = self.get_syst_sample(sample, 'yield', parameterise=param_expr is not None)
        import_syst_list = []
        if self.has_yield_systematics(category, ":common:"):
            import_syst_list.append(":common:")
        if self.has_yield_systematics(category, syst_sample):
            import_syst_list.append(syst_sample)
        # only import systematics if exist
        if len(import_syst_list) > 0:
            kwargs['ImportSyst'] = ",".join(import_syst_list)
        kwargs['MultiplyLumi'] = "1"
        sample_node = xml.add_node('Sample', **kwargs)
        sample_node.add_node('NormFactor', Name=f"yield_{sample}[{sample_yield}]")
        if param_expr is not None:
            sample_node.add_node('NormFactor', Name=param_expr)
        if self.is_HH_sample(sample):
            # ignore custom sample name
            if self.is_HH_ggF_sample(sample):
                sample_node.add_node('NormFactor', Name=f"mu_HH_ggF[1]")
            elif self.is_HH_VBF_sample(sample):
                sample_node.add_node('NormFactor', Name=f"mu_HH_VBF[1]")
        else:
            sample_node.add_node('NormFactor', Name=f"mu_{sample}[1]")
        sample_node.add_node('NormFactor', Name=f"SF_XS_{sample}[1]", Correlate="1")
        if extra_norm_factors is not None:
            for extra_norm_factor in extra_norm_factors:
                sample_node.add_node('NormFactor', Name=extra_norm_factor)
        if self.is_H_sample(sample):
            sample_node.add_node('NormFactor', Name=f"mu_H[1]")
        if self.is_HH_sample(sample):
            sample_node.add_node('NormFactor', Name=f"mu_HH[1]")
        sample_node.add_node('NormFactor', Name=f"mu_{category}[1]")
        sample_node.add_node('NormFactor', Name=f"mu_H_HH[1]")
        
    def create_yield_systematics_xml(self, category:str, sample:str, parameterise:bool=False):
        xml = TXMLTree(doctype='SampleItems', system='AnaWSBuilder.dtd')
        xml.new_root('SampleItems')
        # no systematics defined for that sample in the given category, return an empty xml file
        yield_systematics = self.get_sample_systematics(category, sample, syst_type='yield')
        if yield_systematics is not None:
            self.write_systematics_node(xml, "yield", yield_systematics, process=sample)
        return xml
    
    def create_shape_systematics_xml(self, category:str, sample:str, parameterise:bool=False):
        xml = TXMLTree(doctype='SampleItems', system='AnaWSBuilder.dtd')
        xml.new_root('SampleItems')
        scale_systematics = self.get_sample_systematics(category, sample, syst_type='scale')
        if scale_systematics is not None:
            xml.add_comment("Scale Systematics")
            self.write_systematics_node(xml, "shape", scale_systematics, process=f'{sample}_scale')
        resolution_systematics = self.get_sample_systematics(category, sample, syst_type='resolution')
        if resolution_systematics is not None:
            xml.add_comment("Resolution Systematics")
            self.write_systematics_node(xml, "shape", resolution_systematics, process=f'{sample}_resolution')
        return xml
    
    def write_systematics_node(self, root_node, whereto:str, systematics_data:Dict,
                               process:Optional[str]=None):
        for syst_name, data in systematics_data.items():
            magnitudes = data['Mag']
            if isinstance(magnitudes, list):
                magnitudes = ",".join([f"{magnitude:.5f}" if is_float(magnitude) else f"{magnitude}"
                                       for magnitude in magnitudes ])
            else:
                magnitudes = f"{magnitudes:.5f}" if is_float(magnitudes) else f"{magnitudes}"
            kwargs = {} if process is None else {"Process": process}
            root_node.add_node('Systematic',
                               Name=syst_name,
                               Constr=data['Constr'],
                               CentralValue=str(data['CentralValue']),
                               Mag=magnitudes,
                               WhereTo=whereto,
                               **kwargs)                               
        
    def get_syst_sample(self, sample:str, syst_type:str, parameterise:bool=False):
        if not parameterise:
            return sample
        for base_sample in self.param_config:
            param_samples = list(self.param_config[base_sample])
            if sample in param_samples:
                sample = base_sample
                break
        # hard-coded for now
        if parameterise and (sample in self.param_config):
            if syst_type.lower() == 'yield':
                if sample == self.config['ggF_param']['workspace_sample']:
                    return "ggFHH_param"
                elif sample == self.config['VBF_param']['workspace_sample']:
                    return self.config['workspace']['sample_map']['hh_bbyy_vbf_l1cvv1cv1']
                else:
                    raise ValueError(f'unknown parameterised sample: {sample}')
            elif syst_type.lower() == 'shape':
                if sample == self.config['ggF_param']['workspace_sample']:
                    return self.config['workspace']['sample_map']['HHbbyy_cHHH01d0']
                elif sample == self.config['VBF_param']['workspace_sample']:
                    return self.config['workspace']['sample_map']['hh_bbyy_vbf_l1cvv1cv1']
                else:
                    raise ValueError(f'unknown parameterised sample: {sample}')
            else:
                raise ValueError(f'unknown systematics type: {syst_type}')
        return sample
    
    def create_category_xml(self, category:str, parameterise:bool=True):
        xml = TXMLTree(doctype='Channel', system='AnaWSBuilder.dtd')
        xml.new_root('Channel', Name=category, Type="shape", Lumi="1")
        xml.add_comment('Data Source')
        # write Data node
        if self.dataset_source == "obs_data":
            data_path = self.get_xml_relpath("xml_data_point", category=category)
            file_type = "ascii"
        elif self.dataset_source == "yy_MC":
            data_path = self.get_xml_relpath("xml_yy_histogram", category=category)
            file_type = "histogram"
        else:
            raise RuntimeError(f'unsupported dataset mode "{self.dataset_source}", please choose '
                               'between "yy_MC" or "obs_data"')
        blind_range = self.config["observable"]["blind_range"]
        bin_range   = self.config["observable"]["bin_range"]
        n_bins      = self.config["observable"]["n_bins"]
        data_node_kwargs = {
            "InputFile"   : data_path,
            "FileType"    : file_type,
            "Observable"  : f"atlas_invMass_{category}[{bin_range[0]},{bin_range[1]}]",
            "Binning"     : f"{n_bins}",
            "InjectGhost" : "1",
            "BlindRange"  : f"{blind_range[0]},{blind_range[1]}"
        }
        if file_type == "histogram":
            data_node_kwargs['HistName'] = self.config["observable"]["name"]
        data_node = xml.add_node('Data', **data_node_kwargs)

        # write Item node (e.g. define parameters)
        if parameterise:
            xml.add_comment('Coupling modifiers and EW corrections')
            for parameter, expression in self.param_definition.items():
                xml.add_node('Item', Name=expression)
                
        if (self.systematics is not None) and parameterise:
            xml.add_comment('Parameterised XS Uncertainties for HH ggF sample')
            xml.add_node('Item', Name="expr::uncert_xs_combined_hh_ggf_up('TMath::Max(76.6075 - 56.4818*@0 + 12.635*@0*@0, 75.4617 - 56.3164*@0 + 12.7135*@0*@0)/(70.3874-50.4111*@0+11.0595*@0*@0)-1',klambda)")
            xml.add_node('Item', Name="expr::uncert_xs_combined_hh_ggf_down('1-TMath::Min(57.6809 - 42.9905*@0 + 9.58474*@0*@0, 58.3769 - 43.9657*@0 + 9.87094*@0*@0)/(70.3874-50.4111*@0+11.0595*@0*@0)',klambda)")
        
        # write common systematics
        common_yield_systematics = self.get_sample_systematics(category, ':common:', syst_type='yield')
        common_scale_systematics = self.get_sample_systematics(category, ':common:', syst_type='scale')
        common_resolution_systematics = self.get_sample_systematics(category, ':common:', syst_type='resolution')
        if ((common_yield_systematics is not None) or (common_scale_systematics is not None) or
            (common_resolution_systematics is not None)):
            xml.add_comment('Common Systematics')
        if common_yield_systematics is not None:
            self.write_systematics_node(xml, "yield", common_yield_systematics)
        if common_scale_systematics is not None:
            self.write_systematics_node(xml, "shape", common_scale_systematics, process='common_scale')
        if common_resolution_systematics is not None:
            self.write_systematics_node(xml, "shape", common_resolution_systematics, process='common_resolution')
        
        # import sample systematics from dedicated xml files
        if self.systematics is not None:
            xml.add_comment('Yield Systematics')
            for sample in self.sample_yields[category]:
                syst_sample = self.get_syst_sample(sample, 'yield', parameterise=parameterise)
                if self.has_yield_systematics(category, syst_sample):
                    syst_xml_path = self.get_xml_relpath("yield_systematics_xml",
                                                         category=category,
                                                         sample=syst_sample)
                    xml.add_node('ImportItems', FileName=syst_xml_path)
            xml.add_comment('Shape Systematics')
            for sample in self.sample_yields[category]:
                syst_sample = self.get_syst_sample(sample, 'shape', parameterise=parameterise)
                if self.has_shape_systematics(category, syst_sample):
                    syst_xml_path = self.get_xml_relpath("shape_systematics_xml",
                                                         category=category,
                                                         sample=syst_sample)
                    xml.add_node('ImportItems', FileName=syst_xml_path)
        
        # write Sample node
        if category not in self.sample_yields:
            raise RuntimeError(f'missing sample yield data for the category "{category}"')

        # write yields
        xml.add_comment('Single Higgs and DiHiggs Sample Yields and Systematics')
        for sample, sample_yield  in self.sample_yields[category].items():
            model_xml_path = self.get_xml_relpath("model_xml", sample=sample, category=category)
            if parameterise and (sample in self.param_config):
                for basis_sample, param_expr in self.param_config[sample].items():
                    param_sample_yield = self.param_sample_yields[category][basis_sample]
                    self.add_sample_node(xml, basis_sample, category, model_xml_path,
                                         param_sample_yield,
                                         param_expr=param_expr)
            else:
                self.add_sample_node(xml, sample, category, model_xml_path, sample_yield)
            
        # write non-resonant background
        xml.add_comment('Non-Resonant Background Yields (DUMMY VALUE)')
        model_xml_path = self.get_xml_relpath("model_xml", sample="background", category=category)
        bkg_node = xml.add_node('Sample', Name="background",
                                InputFile=model_xml_path,
                                ImportSyst=":self:",
                                XSection="1",
                                SelectionEff="1",
                                MultiplyLumi="0")
        # dummy value, actually value from fitting workspacee
        bkg_node.add_node('NormFactor', Name=f"nbkg_{category}[10.0,0,10000000]")
        
        # write spurious signal
        
        spurious_signal_systematics = self.get_sample_systematics(category, 'spurious_signal', syst_type='yield')
        if spurious_signal_systematics is not None:
            xml.add_comment('Spurious Signal')
            model_xml_path = self.get_xml_relpath("model_xml", sample=self.get_HH_ggF_sample_name(),
                                                  category=category)
            kwargs = {
                "Name": 'spurious',
                "InputFile": model_xml_path,
                "ImportSyst": ":self:",
                "XSection": "1",
                "SelectionEff": "1",
                "MultiplyLumi": "0"
            }
            sample_node = xml.add_node('Sample', **kwargs)
            self.write_systematics_node(sample_node, "yield", spurious_signal_systematics)
        
        return xml
    
    def create_category_xmls(self, do_non_param:bool=True, do_param:bool=True):
        category_xmls_non_param = {}
        category_xmls_param = {}
        self.path_manager.makedirs(["xml_category"])
        self.path_manager.makedirs(["xml_systematics"])
        if do_non_param:
            for category in self.categories:
                category_xmls_non_param[category] = self.create_category_xml(category, parameterise=False)
                savepath = self.get_file("category_xml_non_param", category=category)
                category_xmls_non_param[category].save(savepath)
                for sample in self.sample_yields[category]:
                    if self.has_yield_systematics(category, sample):
                        yield_syst_xmls_non_param = self.create_yield_systematics_xml(category=category,
                                                                                      sample=sample)
                        savepath = self.get_file("yield_systematics_xml", category=category, sample=sample)
                        yield_syst_xmls_non_param.save(savepath)
                        self.stdout.info(f'INFO: Saved yield systematics xml as "{savepath}"')
                    if self.has_shape_systematics(category, sample):
                        shape_syst_xmls_non_param = self.create_shape_systematics_xml(category=category,
                                                                                      sample=sample)
                        savepath = self.get_file("shape_systematics_xml", category=category, sample=sample)
                        shape_syst_xmls_non_param.save(savepath)
                        self.stdout.info(f'INFO: Saved shape systematics xml as "{savepath}"')
        if do_param:
            for category in self.categories:
                category_xmls_param[category] = self.create_category_xml(category, parameterise=True)
                savepath = self.get_file("category_xml_param", category=category)
                category_xmls_param[category].save(savepath)
                self.stdout.info(f'INFO: Saved category xml as "{savepath}"')
                for sample in self.sample_yields[category]:
                    if (sample not in self.param_config):
                        continue
                    syst_sample = self.get_syst_sample(sample, 'yield', parameterise=True)
                    if self.has_yield_systematics(category, syst_sample):
                        yield_syst_xmls_param = self.create_yield_systematics_xml(category=category,
                                                                                  sample=syst_sample)
                        savepath = self.get_file("yield_systematics_xml", category=category, sample=syst_sample)
                        yield_syst_xmls_param.save(savepath)
                        self.stdout.info(f'INFO: Saved yield systematics xml as "{savepath}"')

    def create_signal_model_xml(self, sample:str, category:str):
        if category not in self.model_parameters:
            raise RuntimeError(f'missing model parameters for the category "{category}"')
        mH_value = round(self.config["workspace"]["Higgs_mass"], 8)
        model_parameters = self.model_parameters[category]
        xml = TXMLTree(doctype='Model', system='AnaWSBuilder.dtd')
        xml.new_root('Model', Type="UserDef", CacheBinning="100000")
        xml.add_node('Item', Name=f"mH[{mH_value}]")
        xml.add_node('Item', Name=f"expr::muCBNom_{sample}('{model_parameters['muCBNom']}-125+@0',mH)")
        xml.add_node('Item', Name=f"sigmaCBNom_{sample}[{model_parameters['sigmaCBNom']}]")
        xml.add_node('Item', Name=f"alphaCBLo_{sample}[{model_parameters['alphaCBLo']}]")
        xml.add_node('Item', Name=f"alphaCBHi_{sample}[{model_parameters['alphaCBHi']}]")
        xml.add_node('Item', Name=f"nCBLo_{sample}[{model_parameters['nCBLo']}]")
        xml.add_node('Item', Name=f"nCBHi_{sample}[{model_parameters['nCBHi']}]")
        
        # take care of shape systematics
        scale_response_functions = [f"muCBNom_{sample}"]
        sample_scale_systematics = self.get_sample_systematics(category, sample, syst_type='scale')
        if sample_scale_systematics is not None:
            scale_response_functions.append(f"response::{sample}_scale")
        common_scale_systematics = self.get_sample_systematics(category, ":common:", syst_type='scale')
        if common_scale_systematics is not None:
            scale_response_functions.append(f"response::common_scale")
        if len(scale_response_functions) > 1:
            muCB_expr = f"prod::muCB_{sample}({','.join(scale_response_functions)})"
        else:
            muCB_expr = f"muCBNom_{sample}"
        resolution_response_functions = [f"sigmaCBNom_{sample}"]
        sample_resolution_systematics = self.get_sample_systematics(category, sample, syst_type='resolution')
        if sample_resolution_systematics is not None:
            resolution_response_functions.append(f"response::{sample}_resolution")
        common_resolution_systematics = self.get_sample_systematics(category, ":common:", syst_type='resolution')
        if common_resolution_systematics is not None:
            resolution_response_functions.append(f"response::common_resolution")
        if len(resolution_response_functions) > 1:
            sigmaCB_expr = f"prod::sigmaCB_{sample}({','.join(resolution_response_functions)})"
        else:
            sigmaCB_expr = f"sigmaCBNom_{sample}"
            
        model_item_str = (f"RooTwoSidedCBShape::signal(:observable:, {muCB_expr}, "
                          f"{sigmaCB_expr}, alphaCBLo_{sample}, nCBLo_{sample}, "
                          f"alphaCBHi_{sample}, nCBHi_{sample})")
        xml.add_node('ModelItem', Name=model_item_str)
        return xml   
    
    def create_background_model_xml(self, category, functional_form:str="exponential"):
        xml = TXMLTree(doctype='Model', system='AnaWSBuilder.dtd')
        xml.new_root('Model', Type="UserDef")
        if functional_form in ["exp", "exponential"]:
            model_item_str = (f"EXPR::bkgPdf_{category}('exp((@0-100)*@1/100)', :observable:,  "
                              f"BKG_p0_{category}[-1,-1000,1000])")
        elif functional_form == "epoly2":
            model_item_str = (f"EXPR::bkgPdf_{category}('exp((@0-100)/100*(@1 + @2*(@0-100)/100))', "
                              f":observable:,  BKG_p0_{category}[-1,-1000,1000], BKG_p1_{category}[-1,-1000,1000])")
        else:
            raise ValueError(f"background model \"{model}\" not supported")
        xml.add_node('ModelItem', Name=model_item_str)
        return xml

    def create_model_xmls(self):
        signal_model_xmls = {}
        background_model_xmls = {}
        self.path_manager.makedirs(["xml_model"])
        bkg_functional_form = self.config["background_modelling"]["functional_form"]
        for category in self.categories:
            signal_model_xmls[category] = {}
            background_model_xmls[category] = self.create_background_model_xml(category, bkg_functional_form)
            savepath = self.get_file("model_xml", sample="background", category=category)
            background_model_xmls[category].save(savepath)
            for sample in self.sample_yields[category]:
                signal_model_xmls[category][sample] = self.create_signal_model_xml(sample, category)
                savepath = self.get_file("model_xml", sample=sample, category=category)
                signal_model_xmls[category][sample].save(savepath)
                self.stdout.info(f'INFO: Saved model xml as "{savepath}"')

    def get_poi_list(self, parameterise:bool=False, categories:Optional[List[str]]=None):
        pois = []
        if parameterise:
            param_names = self.config["parameterization"]
            pois.extend(list(param_names))
        pois += ['mu_H_HH', 'mu_H', 'mu_HH']
        if categories is None:
            categories = self.categories
        sample_pois = set()
        for category in categories:
            cat_sample_pois = []
            for sample in self.sample_yields[category]:
                if (self.is_HH_sample(sample)):
                    if self.is_HH_ggF_sample(sample):
                        cat_sample_pois.append('mu_HH_ggF')
                    elif self.is_HH_VBF_sample(sample):
                        cat_sample_pois.append('mu_HH_VBF')
                else:
                    cat_sample_pois.append(f"mu_{sample}")
            sample_pois |= set(cat_sample_pois)
        pois.extend(list(sample_pois))
        for category in categories:
            pois.append(f"mu_{category}")
        return pois
    
    def get_setup_poi_list(self, parameterise:bool=False):
        pois = []
        if parameterise:
            param_names = self.config["parameterization"]
            pois.extend(list(param_names))
        pois.extend(['mu_HH', 'mu_HH_ggF', 'mu_HH_VBF'])
        return pois
        
    def create_input_xml(self, parameterise:bool=True, categories:Optional[List[str]]=None):
        if parameterise:
            workspace_path = self.get_xml_relpath("workspace_param")
            prefix = "param"
        else:
            workspace_path = self.get_xml_relpath("workspace_non_param")
            prefix = "non_param"
        if categories is None:
            categories = self.categories
        else:
            # workspace with subset of categories
            workspace_path = workspace_path.replace(".root", "_" + "_".join(categories) + ".root")
        pois = self.get_poi_list(parameterise=parameterise, categories=categories)
        setup_pois = self.get_setup_poi_list()
        
        xml = TXMLTree(doctype='Combination', system='AnaWSBuilder.dtd')
        xml.new_root('Combination', WorkspaceName="combWS",
                     ModelConfigName="ModelConfig", DataName="combData",
                     OutputFile=workspace_path, Blind=str(int(self.do_blind)))
        for category in categories:
            if parameterise:
                category_xml_path = self.get_xml_relpath("category_xml_param", category=category)
            else:
                category_xml_path = self.get_xml_relpath("category_xml_non_param", category=category)
            xml.add_node('Input', text=category_xml_path)
        xml.add_node("POI", text=",".join(pois))
        setup_str = ",".join([f"{poi}=1" for poi in setup_pois])
        xml.add_node("Asimov", Name="setup", Setup=setup_str, Action="")
        xml.add_node("Asimov", Name="POISnap", Setup="", Action="savesnapshot",
                     SnapshotPOI="nominalPOI")
        xml.add_node("Asimov", Name="NPSnap", Setup="mu_H_HH=0",
                     Action="fixsyst:fit:float:savesnapshot:nominalPOI",
                     SnapshotNuis="nominalNuis", SnapshotGlob="nominalGlobs")
        return xml
        
    def create_input_xmls(self, do_non_param:bool=True, do_param:bool=True,
                          do_per_category:bool=False):
        self.path_manager.makedirs(["xml_config"])
        if do_non_param:
            input_xml = self.create_input_xml(parameterise=False)
            savepath = self.get_file("input_xml_non_param")
            input_xml.save(savepath)
            if do_per_category:
                for category in self.categories: 
                    input_xml_cat = self.create_input_xml(parameterise=False,
                                                          categories=[category])
                    savepath = self.get_file("input_xml_non_param")
                    savepath = f"{os.path.splitext(savepath)[0]}_{category}.xml"
                    input_xml_cat.save(savepath)
        if do_param:
            input_xml = self.create_input_xml(parameterise=True)
            savepath = self.get_file("input_xml_param")
            input_xml.save(savepath)
            if do_per_category:
                for category in self.categories: 
                    input_xml_cat = self.create_input_xml(parameterise=True,
                                                          categories=[category])
                    savepath = self.get_file("input_xml_param")
                    savepath = f"{os.path.splitext(savepath)[0]}_{category}.xml"
                    input_xml_cat.save(savepath)
                    self.stdout.info(f'INFO: Saved input xml as "{savepath}"')
                    
    def create_systematics_xmls(self):
        pass
        
    def create_xmls(self, do_non_param:bool=True, do_param:bool=True, do_per_category:bool=False):
        self.create_category_xmls(do_non_param=do_non_param, do_param=do_param)
        self.create_model_xmls()
        #self.create_systematics_xmls()
        self.create_input_xmls(do_non_param=do_non_param,
                               do_param=do_param,
                               do_per_category=do_per_category)