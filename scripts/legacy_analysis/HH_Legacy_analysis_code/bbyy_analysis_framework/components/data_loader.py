from typing import List, Dict, Union, Optional
import os

from quickstats.analysis.data_loading import DataLoader as QSDataLoader
from bbyy_analysis_framework.components import AnalysisBase

class DataLoader(AnalysisBase, QSDataLoader):
    
    REQUIRED_CONFIG_COMPONENTS = ["paths:arrays",
                                  "samples:*", "variables:*",
                                  "training:*", "channels:*",
                                  "names:weight", "names:event_number"] 
    
    def __init__(self, analysis_config:Union[Dict, str],
                 array_dir:Optional[str]=None,
                 model_dir:Optional[str]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super().__init__(analysis_config=analysis_config,
                         array_dir=array_dir,
                         model_dir=model_dir,
                         verbosity=verbosity,
                         **kwargs)