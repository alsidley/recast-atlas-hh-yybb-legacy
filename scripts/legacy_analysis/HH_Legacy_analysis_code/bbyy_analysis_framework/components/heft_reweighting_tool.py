from typing import Dict, Optional, List, Union
import os
import json

import numpy as np
import pandas as pd

from quickstats import AbstractObject, semistaticmethod
from quickstats.maths.numerics import str_decode_value, str_encode_value
from quickstats.utils.common_utils import combine_dict

import bbyy_analysis_framework
from .eft_reweighting_tool import EFTReweightingTool

class HEFTReweightingTool(EFTReweightingTool):
    
    CONFIG = {
        "reweight_filename"        : "NLO_norm_values.json",
        "branch_name_observable"   : "m_hh", # branch name of the variable used in reweighting
        "reweight_var_scale"       : 0.001, # scale factor applied to the branch variable values
        "branch_name_weight"       : "HEFT_weight.{param_expr}",
        "branch_name_den_weight"   : "HEFT_den_weight.{param_expr}",
        "extra_branches"           : "all"
    }
    
    EFT_MODEL = "HEFT"
    
    PARAMETER_SET = ("ctt", "cgghh", "cggh", "chhh", "ct")
    #PARAMETER_SET = ("c2", "c2g", "cg", "kl", "kt")
    
    POLYNOMIAL = ("A1 * ct * ct * ct * ct + "
                  "A2 * ctt * ctt + "
                  "A3 * ct * ct * chhh * chhh + "
                  "A4 * cggh * cggh * chhh * chhh + "
                  "A5 * cgghh * cgghh + "
                  "A6 * ctt * ct * ct + "
                  "A7 * ct * ct * ct * chhh + "
                  "A8 * ctt * ct * chhh + "
                  "A9 * ctt * cggh * chhh + "
                  "A10 * ctt * cgghh + "
                  "A11 * ct * ct * cggh * chhh + "
                  "A12 * ct * ct * cgghh + "
                  "A13 * ct * chhh * chhh * cggh + "
                  "A14 * ct * chhh * cgghh + "
                  "A15 * cggh * chhh * cgghh + "
                  "A16 * ct * ct * ct * cggh + "
                  "A17 * ct * ctt * cggh + "
                  "A18 * ct * cggh * cggh * chhh + "
                  "A19 * ct * cggh * cgghh + "
                  "A20 * ct * ct * cggh * cggh + "
                  "A21 * ctt * cggh * cggh + "
                  "A22 * cggh * cggh * cggh * chhh"
                  "A23 * cggh * cggh * cgghh")
    
    def get_Rhh(self, observable_bin:Optional[float], eft_parameters:Dict[str, float]) -> float:
        """Return Rhh value for the given observable bin
        """
        ctt   = eft_parameters["ctt"]
        cgghh = eft_parameters["cgghh"]
        cggh  = eft_parameters["cggh"]
        chhh  = eft_parameters["chhh"]
        ct    = eft_parameters["ct"]
        if observable_bin is None:
            A = self.reweight_data["inclusive"].values
        else:
            A = self.reweight_data[observable_bin].values
        Rhh = (A[0] * (ct ** 4) + A[1] * (ctt ** 2) + (A[2] * (ct ** 2) + A[3] * (cggh ** 2)) * (chhh ** 2) + 
               A[4] * (cgghh ** 2) + (A[5] * ctt + A[6] * ct * chhh) * (ct ** 2) + 
               (A[7] * ct * chhh + A[8] * cggh * chhh) * ctt + A[9] * ctt * cgghh + 
               (A[10] * cggh * chhh + A[11] * cgghh) * (ct **2) + (A[12] * chhh * cggh + A[13] * cgghh) * ct * chhh + 
               A[14] * cggh * cgghh * chhh + A[15] * cggh * (ct **3) + A[16] * ctt * ct * cggh +
               A[17] * ct * chhh * (cggh **2) + A[18] * ct * cggh * cgghh + 
               A[19] * (ct **2) * (cggh ** 2) + A[20] * ctt * (cggh **2) + 
               A[21] * chhh * (cggh ** 3) + A[22] * cgghh * ( cggh **2))
        return Rhh
        
    def _get_reweight_branch_data(self, observable_values:np.ndarray,
                                 eft_parameters:Dict[str, float]):
        # account for differences between BSM and SM XS 
        correction = 1
        weights = correction * self.get_eft_weights(observable_values, eft_parameters)
        den_weight = self.get_den_weight(eft_parameters)
        n_entry = len(observable_values)
        den_weights = np.full((n_entry,), den_weight, dtype=np.float32)
        param_expr = self.get_param_expr(eft_parameters)
        branch_name_weight     = self.config["branch_name_weight"].format(param_expr=param_expr)
        branch_name_den_weight = self.config["branch_name_den_weight"].format(param_expr=param_expr)
        branch_data = {
            branch_name_weight     : weights,
            branch_name_den_weight : den_weights
        }
        return branch_data
    
    def run(self, heft_parameters:Dict[str, float], infile:str, outfile:str,
            treename:str="CollectionTree"):
        super().run(infile=infile, outfile=outfile,
                    treename=treename, eft_parameters=heft_parameters)