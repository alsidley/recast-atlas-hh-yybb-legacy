from typing import Optional, List

from quickstats import DescriptiveEnum

class LimitResultSetup(DescriptiveEnum):
    SM_LIMIT     = (0, "Limit on mu_HH (HH production signal strength); ggF + VBF", "SM",
                    False, False, "", None)
    SM_VBF_LIMIT = (1, "Limit on mu_HH (HH production signal strength); VBF only", "SM_VBF",
                    False, True, "", None)
    KL_SCAN      = (2, "Scan of HH cross-section limit as a function of klambda; ggF + VBF", "klambda",
                    True, False, "klambda", "THEO_XS_*=0")
    K2V_SCAN     = (3, "Scan of HH cross-section limit as a function of k2V; ggF + VBF", "k2v",
                    True, False, "k2v", "THEO_XS_*=0")
    K2V_VBF_SCAN = (4, "Scan of HH cross-section limit as a function of k2V; VBF only", "k2v_vbf_only",
                    True, True, "k2v", "THEO_XS_*=0")
    
    def __new__(cls, value:int, description:str="", abbrev:str="", is_parameterized:bool=False, vbf_only:bool=False,
                 parameter:str="", fix_expr:Optional[str]=None):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.description = description
        obj.is_parameterized = is_parameterized
        obj.vbf_only = vbf_only
        obj.abbrev = abbrev
        obj.parameter = parameter
        obj.fix_expr = fix_expr
        return obj
        
class LikelihoodResultSetup(DescriptiveEnum):
    KL_SCAN     = (0, "Likelihood scan as function of klambda", "klambda", ["klambda"])
    K2V_SCAN    = (1, "Likelihood scan as function of k2v", "k2v", ["k2v"])
    KL_K2V_SCAN = (2, "Likelihood scan as function of (klambda, k2v)", "klambda_k2v", ["klambda", "k2v"])
    
    def __new__(cls, value:int, description:str="", abbrev:str="", parameters:Optional[List[str]]=None):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.description = description
        obj.abbrev = abbrev
        obj.parameters = parameters
        return obj