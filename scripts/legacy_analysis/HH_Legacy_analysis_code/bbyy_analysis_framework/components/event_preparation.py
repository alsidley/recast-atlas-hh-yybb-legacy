import os
import json
import hashlib
from typing import Optional, Union, List, Dict, Tuple


import numpy as np
import pandas as pd

from bbyy_analysis_framework.components import AnalysisBase

class EventPreparation(AnalysisBase):
    
    def __init__(self, config_path:str, array_dir:Optional[str]=None,
                 outdir:Optional[str]=None, verbosity:Optional[Union[int, str]]="INFO"):
        super().__init__(config_path=config_path, array_dir=array_dir,
                         outdir=outdir, verbosity=verbosity)
        self.train_samples = {}
        self.test_samples  = {}
        self.event_numbers = {}
        
    def load_samples(self):
        train_samples = {}
        test_samples = {}
        event_numbers = {}
        column_name = self.config['names']['fold']
        for channel in self.config['channels']:
            selection = self.config['channels'][channel]['selection']
            samples = self.config['channels'][channel]['train_samples']
            train_samples[channel] = self.resolve_samples(samples)
            samples = self.config['channels'][channel]['test_samples']
            test_samples[channel] = self.resolve_samples(samples)
            event_numbers[channel] = {}
            for sample in test_samples[channel]:
                self.stdout.info(f"INFO: Loading sample \"{sample}\" for the channel \"{channel}\"...")
                sample_path = os.path.join(self.array_dir, f"{sample}.csv")
                df = pd.read_csv(sample_path)
                df = df.query(selection)
                event_numbers[channel][sample] = df[column_name]
        self.train_samples = train_samples
        self.test_samples = test_samples
        self.event_numbers = event_numbers
        
    def prepare_event_number_based_samples(self, train_remainder:Union[Tuple, List]=(0, 1, 2, 3),
                                           test_remainder:Union[Tuple, List]=(4,), new_modulo:int=5, 
                                           base_remainder:Union[Tuple, List]=(0, 1), base_modulo:int=4):
        train_remainder = list(train_remainder)
        test_remainder = list(test_remainder)
        base_remainder = list(base_remainder)
        selected_event_numbers = {'train':{}, 'test':{}}
        for channel in self.event_numbers:
            selected_event_numbers['train'][channel] = {}
            selected_event_numbers['test'][channel] = {}
            train_samples = self.train_samples[channel]
            for sample in self.event_numbers[channel]:
                events = self.event_numbers[channel][sample]
                # not used in training
                if sample not in train_samples:
                    selected_event_numbers['train'][channel][sample] = np.array([])
                    selected_event_numbers['test'][channel][sample] = np.sort(events.values)
                    continue
                mask = (events % base_modulo).isin(base_remainder)
                base_events = events[mask]
                mask = (base_events % new_modulo).isin(train_remainder)
                train_events = np.sort(base_events[mask].values)
                mask = (base_events % new_modulo).isin(test_remainder)
                test_events = np.sort(base_events[mask].values)
                selected_event_numbers['train'][channel][sample] = train_events
                selected_event_numbers['test'][channel][sample]  = test_events
        return selected_event_numbers
    def prepare_random_selected_samples(self, train_size:float=0.8, test_size:float=0.2,
                                        hash_fmt:str="{channel}_{sample}", base_seed:int=1,
                                        base_remainder:Union[Tuple, List]=(0, 1), base_modulo:int=4):
        from sklearn.model_selection import train_test_split
        selected_event_numbers = {'train':{}, 'test':{}}
        for channel in self.event_numbers:
            selected_event_numbers['train'][channel] = {}
            selected_event_numbers['test'][channel] = {}
            train_samples = self.train_samples[channel]
            for sample in self.event_numbers[channel]:
                events = self.event_numbers[channel][sample]
                mask = (events % base_modulo).isin(base_remainder)
                base_events = events[mask]
                # not used in training
                if sample not in train_samples:
                    selected_event_numbers['train'][channel][sample] = np.array([])
                    selected_event_numbers['test'][channel][sample] = np.sort(events.values)
                    continue
                hash_str = hash_fmt.format(channel=channel, sample=sample)
                seed = base_seed + int.from_bytes(hashlib.sha256(hash_str.encode()).digest()[:4], 'little')
                result = train_test_split(base_events.values, test_size=test_size, train_size=train_size,
                                          random_state=seed)
                selected_event_numbers['train'][channel][sample] = result[0]
                selected_event_numbers['test'][channel][sample]  = result[1]
        return selected_event_numbers