from typing import Dict, List, Optional, Union
import os
import copy
import json

import numpy as np
import pandas as pd

from quickstats.utils.common_utils import combine_dict
from quickstats.maths.numerics import str_decode_value
from quickstats.analysis import SamplePolyParamTool

from bbyy_analysis_framework.components import AnalysisBase

class SignalParameterization(AnalysisBase):
    
    TASKS = ("ggF", "VBF")
    
    def __init__(self, analysis_config_path:str,
                 study_name:Optional[str]=None,
                 outdir:Optional[str]=None,
                 tasks:Optional[List[str]]=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super().__init__(analysis_config=analysis_config_path,
                         outdir=outdir,
                         verbosity=verbosity,
                         **kwargs)
        if study_name is not None:
            self.set_study_name(study_name)
        self.param_tool = SamplePolyParamTool(verbosity=verbosity)
        self.setup_tasks(tasks)
    
    def setup_tasks(self, tasks:Optional[List[str]]=None):
        self.param_configs = {}
        self.param_data    = {}
        if tasks is None:
            tasks = self.TASKS
            
        for task in tasks:
            if f"{task}_param" not in self.config:
                raise RuntimeError(f'missing config for {task} parameterisation')
            self.param_configs[task] = combine_dict(self.config[f"{task}_param"])
            self.param_data[task] = None
        self.tasks = tasks
    
    def run_single_task(self, task:str, eval_err:bool=True):
        param_config = self.param_configs[task]
        param_method = param_config['method']
        kwargs = {
            "formula"       : param_config['formula'],
            "parameters"    : param_config['parameters'],
            "coefficients"  : param_config['coefficients'],
            "basis_samples" : param_config['basis_samples'],
            "latex_map"     : param_config.get('latex_map', {})
        }
        
        self.stdout.info(f"INFO: Running {task} parameterisation using the {param_method} method")
        if param_method in ["linear_combination"]:
            result = self.param_tool.run_parameterization(**kwargs)
        else:
            raise RuntimeError(f'unsupported parameterisation method "{param_method}"')
            
        self.param_data[task] = result
        
    def run_param(self):
        for task in self.tasks:
            self.run_single_task(task)

    def save_param(self, save_dir:Optional[str]=None):
        temp_dir = self.path_manager.directories["signal_param"]
        if save_dir is not None:
            self.path_manager.set_directory("signal_param", save_dir, absolute=True)

        for task in self.param_data:
            if self.param_data[task] is None:
                raise RuntimeError(f"{task} parameterisation not done")
            # create save directory if not already exists
            self.path_manager.makedir_for_files([f"{task}_param"])
        
            param_savepath = self.get_file(f"{task}_param")
            with open(param_savepath, "w") as outfile:
                json.dump(self.param_data[task], outfile, indent=2)
            self.stdout.info(f'INFO: Saved {task} parameterization data to "{param_savepath}"')
    
        # restore yield directory in case a custom yield_directory is set
        self.path_manager.set_directory("signal_param", temp_dir)                