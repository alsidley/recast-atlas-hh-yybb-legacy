from typing import Dict, Optional, List, Union
import os
import math
import json

import numpy as np
import pandas as pd

from quickstats import AbstractObject, semistaticmethod
from quickstats.maths.numerics import str_decode_value, str_encode_value
from quickstats.utils.common_utils import combine_dict

import bbyy_analysis_framework
from .eft_reweighting_tool import EFTReweightingTool

class SMEFTReweightingTool(EFTReweightingTool):
    
    TRANSLATION_PARAMETERS = {
        "vev"     : 246.,
        "m"       : 125.,
        "lambda"  : 1000.,
        "mt"      : 173.,
        "alpha_s" : 0.1198
    }
    
    CONFIG = {
        "reweight_filename"      : "Weights_SMEFT_bin_240_1040_40_cpg_incl.json",
        "branch_name_observable" : "m_hh", # branch name of the observable used in reweighting
        "reweight_var_scale"     : 0.001, # scale factor applied to the branch variable values
        "order"                  : "NLO",
        "branch_name_weight"     : "SMEFT_{order}_weight.{param_expr}",
        "extra_branches"         : "all",
        "param_str_precision"    : 3,
        "apply_kfactor"          : True
    }
    
    EFT_MODEL = "SMEFT"
    
    PARAMETER_SET = ("cH", "cHbox", "ctG", "ctH", "cHG") # ATLAS notation
    #PARAMETER_SET = ("cp", "cdp", "ctg", "ctp", "cpg") #MadGraph notation
    
    POLYNOMIAL = ("A1 + A2 * cHbox * cHbox + A3 * cHbox + A4 * cHbox * cH + A5 * cH + A6 * cH * cH + "
                  "A7 * ctH * ctH + A8 * ctH + A9 * ctH * ctG + A10 * ctG + A11 * ctG * ctG + "
                  "A12 * cHbox * ctG + A13 * ctG * cH + A14 * cHbox * ctH + A15 * ctH * cH + "
                  "A16 * cHG + A17 * cHG * cHG + A18 * cHG * cHbox + A19 * cHG * cH + "
                  "A20 * cHG * ctH + A21 * cHG * ctG") # ATLAS notation
    #POLYNOMIAL = ("A1 + A2 * cdp * cdp + A3 * cdp + A4 * cdp * cp + A5 * cp + A6 * cp * cp + "
    #              "A7 * ctp * ctp + A8 * ctp + A9 * ctp * ctg + A10 * ctg + A11 * ctg * ctg + "
    #              "A12 * cdp * ctg + A13 * ctg * cp + A14 * cdp * ctp + A15 * ctp * cp + "
    #              "A16 * cpg + A17 * cpg * cpg + A18 * cpg * cdp + A19 * cpg * cp + "
    #              "A20 * cpg * ctp + A21 * cpg * ctg") #MadGraph notation
    
    @semistaticmethod
    def to_madgraph_repr(self, cH:float, cHbox:float, ctG:float, ctH:float, cHG:float):
        """Convert SMEFT couplings from ATLAS convension to MadGraph convensnion
        """
        # reference: https://indico.cern.ch/event/1212704/contributions/5100982/attachments/2537899/4368179/EFT_HH_Recomendations.pdf
        result = {
            "cp" : cH,
            "cdp": cHbox,
            "ctg": ctG,
            "ctp": ctH,
            "cpg": cHG
        }
        return result
    
    @semistaticmethod
    def to_atlas_repr(self, cp:float, cdp:float, ctg:float, ctp:float, cpg:float):
        """Convert SMEFT couplings from MadGraph convension to ATLAS convensnion
        """
        # reference: https://indico.cern.ch/event/1212704/contributions/5100982/attachments/2537899/4368179/EFT_HH_Recomendations.pdf
        result = {
            "cH"   : cp,
            "cHbox": cdp,
            "ctG"  : ctg,
            "ctH"  : ctp,
            "cHG"  : cpg
        }
        return result    
        
    @semistaticmethod
    def get_HEFT_couplings(self, cH:float, cHbox:float, ctG:float, ctH:float, cHG:float):
        """Convert from SMEFT coupling parameters to HEFT coupling parameters
        """
        vev      = self.TRANSLATION_PARAMETERS["vev"]
        m        = self.TRANSLATION_PARAMETERS["m"]
        Lambda   = self.TRANSLATION_PARAMETERS["lambda"]
        mt       = self.TRANSLATION_PARAMETERS["mt"]
        alpha_s  = self.TRANSLATION_PARAMETERS["alpha_s"]
        lambda_v = (vev / Lambda) ** 2
        
        cHkin = cHbox - 0.25 * cHD
        chhh  = 1 - 2 * ((vev / m) **2) * lambda_v * cH + 3 * lambda_v * cHkin
        ct    = 1 + lambda_v * cHkin - lambda_v * (vev / (math.sqrt(2) * mt)) * cuH
        ctt   = lambda_v * cHkin - lambda_v * (3 * vev / (2 * math.sqrt(2) * mt)) * cuH
        cggh  = 8 * (math.pi / alpha_s) * lambda_v * cHG
        cgghh = 4 * (math.pi / alpha_s) * lambda_v * cHG
        
        heft_couplings = {
            "chhh"  : chhh,
            "ct"    : ct,
            "ctt"   : ctt,
            "cggh"  : cggh,
            "cgghh" : cgghh
        }
        return heft_couplings
    
    @semistaticmethod
    def get_HEFT_couplings_expr(self) -> Dict[str, str]:
        """Returns the string representation of a conversion from SMEFT couplings to HEFT couplings
        """
        vev      = self.TRANSLATION_PARAMETERS["vev"]
        m        = self.TRANSLATION_PARAMETERS["m"]
        Lambda   = self.TRANSLATION_PARAMETERS["lambda"]
        mt       = self.TRANSLATION_PARAMETERS["mt"]
        alpha_s  = self.TRANSLATION_PARAMETERS["alpha_s"]
        lambda_v = (vev / Lambda) ** 2
        couplings_expr = {
            "cHkin" : "cHbox - 0.25 * cHD",
            "chhh"  : f"1 - {2 * ((vev / m) **2) * lambda_v} * cH + {3 *lambda_v} * cHkin",
            "ct"    : f"1 + {lambda_v} * cHkin - {lambda_v * (vev / (math.sqrt(2) * mt))} * cuH",
            "ctt"   : f"{lambda_v} * cHkin - {lambda_v * (3 * vev / (2 * math.sqrt(2) * mt))} * cuH",
            "cggh"  : f"{8 * (math.pi / alpha_s) * lambda_v} * cHG",
            "cgghh" : f"{4 * (math.pi / alpha_s) * lambda_v} * cHG"
        }
        return couplings_expr
    
    @semistaticmethod
    def get_k_factor_expr(self) -> Dict[str, str]:
        """Returns the string representation of the k-factor in terms of HEFT couplings
        """
        heft_nlo_xs_expr = ("2.23389 * ct * ct * ct * ct + 12.4598 * ctt * ctt + "
                            "(0.342248 * ct * ct + 0.346822 * cggh * cggh) * chhh * chhh + "
                            "13.0087 * cgghh * cgghh + (-9.6455 * ctt - 1.57553 * ct * chhh) * ct * ct + "
                            "(3.43849 * ct * chhh + 2.86694 * cggh * chhh) * ctt + 16.6912 * ctt * cgghh + "
                            "(-1.25293 * cggh * chhh - 5.81216 * cgghh) * ct * ct + "
                            "(0.649714 * chhh * cggh + 2.85933 * cgghh) * ct * chhh + "
                            "3.14475 * cggh * cgghh * chhh - 0.00816241 * cggh * ct * ct * ct + "
                            "0.0208652 * ctt * ct * cggh + 0.0168157 * ct * chhh * cggh * cggh + "
                            "0.0298576 * ct * cggh * cgghh - 0.0270253 * ct * ct * cggh * cggh + "
                            "0.0726921 * ctt * cggh * cggh + 0.0145232 * chhh * cggh * cggh * cggh + "
                            "+ 0.123291 * cgghh * cggh * cggh")
        heft_lo_xs_expr = ("2.08059 * ct * ct * ct * ct + 10.2011 * ctt * ctt + "
                           "(0.27814 * ct * ct + 0.314043 * cggh * cggh) * chhh * chhh + "
                           "12.2731 * cgghh * cgghh + (-8.49307 * ctt - 1.35873 * ct * chhh) * ct * ct + "
                           "(2.80251 * ct * chhh + 2.48018 * cggh * chhh) * ctt + 14.6908 * ctt * cgghh + "
                           "(-1.15916 * cggh * chhh -  5.51183 * cgghh) * ct * ct + "
                           "(0.560503 * chhh * cggh + 2.47982 * cgghh) * ct * chhh + "
                           "2.89431 * cggh * cgghh * chhh")
        expr = {
            "HH_ggF_HEFT_NLO_XS" : heft_nlo_xs_expr,
            "HH_ggF_HEFT_LO_XS"  : heft_lo_xs_expr,
            "HH_ggF_HEFT_kfactor": f"HH_ggF_HEFT_NLO_XS / HH_ggF_HEFT_LO_XS"
        }        
    
    @semistaticmethod
    def get_heft_nlo_xs(self, ctt:float, cgghh:float, cggh:float, chhh:float, ct:float) -> float:
        """Returns the NLO cross section for the ggF HH process for a given set of HEFT couplings
        """
        # The constants in this functions are the Ai values (for NLO) from Table 1 in the paper arxiv:1806.05162. 
        # NOTE: This values are not normalised to SM (unlike those in the last line of  NLO_norm_values.json)
        xs = (2.23389 * (ct ** 4) + 12.4598 * (ctt ** 2) + (0.342248 * (ct ** 2) + 0.346822 * (cggh ** 2)) * (chhh ** 2) + 
             13.0087 * (cgghh ** 2) + (-9.6455 * ctt - 1.57553 * ct * chhh) * (ct ** 2) + 
             (3.43849 * ct * chhh + 2.86694 * cggh * chhh) * ctt + 16.6912 * ctt * cgghh + 
             (-1.25293 * cggh * chhh - 5.81216 * cgghh) * (ct ** 2) + (0.649714 * chhh * cggh + 2.85933 * cgghh) * ct * chhh +
             3.14475 * cggh * cgghh * chhh - 0.00816241 * cggh * (ct ** 3) + 0.0208652 * ctt * ct * cggh +
             0.0168157 * ct * chhh * (cggh ** 2) + 0.0298576 * ct * cggh * cgghh - 0.0270253 * (ct ** 2) * (cggh ** 2) +
             0.0726921 * ctt * (cggh ** 2) + 0.0145232 * chhh * (cggh ** 3) + 0.123291 * cgghh * (cggh ** 2))
        return xs    
    
    @semistaticmethod
    def get_heft_lo_xs(self, ctt:float, cgghh:float, cggh:float, chhh:float, ct:float) -> float:
        """Returns the LO cross section for the ggF HH process for a given set of HEFT couplings
        """     
        # The constants in this functions are the Ai values (for LO) from Table 1 in the paper arxiv:1806.05162. 
        # NOTE: This values are not normalised to SM (unlike those in the last line of  NLO_norm_values.json)
        xs =  (2.08059 * (ct ** 4) + 10.2011 * (ctt ** 2) + (0.27814 * (ct ** 2) + 0.314043 * (cggh ** 2)) * (chhh ** 2) +
               12.2731 * (cgghh ** 2) + (-8.49307 * ctt - 1.35873 * ct * chhh) * (ct ** 2) +
               (2.80251 * ct * chhh + 2.48018 * cggh * chhh) * ctt + 14.6908 * ctt * cgghh +
               (-1.15916 * cggh * chhh -  5.51183 * cgghh) * (ct ** 2) +
               (0.560503 * chhh * cggh + 2.47982 * cgghh) * ct * chhh +
               2.89431 * cggh * cgghh * chhh)
        return xs
    
    @semistaticmethod
    def get_k_factor(self, cH:float, cHbox:float, cHD:float, cuH:float, cHG:float):
        """Returns the k-factor (i.e. NLO xs / LO xs) for the ggF HH process for a given set of HEFT couplings
        """
        heft_couplings = self.get_HEFT_couplings(cH=cH, cHbox=cHbox, cHD=cHD, cuH=cuH, cHG=cHG)
        xs_nlo = self.get_heft_nlo_xs(**heft_couplings)
        xs_lo  = self.get_heft_lo_xs(**heft_couplings)
        k_factor = xs_nlo / xs_lo
        return k_factor
    
    def get_Rhh(self, observable_bin:Optional[float], eft_parameters:Dict[str, float]) -> float:
        """Return Rhh value for the given observable bin
        """
        cH    = eft_parameters["cH"]
        cHbox = eft_parameters["cHbox"]
        cHD   = eft_parameters["cHD"]
        cuH   = eft_parameters["cuH"]
        cHG   = eft_parameters["cHG"]
        if observable_bin is None:
            A = self.reweight_data["inclusive"].values
        else:
            A = self.reweight_data[observable_bin].values
        Rhh = A[0] + A[1] * (cHbox ** 2) + A[2] * cHbox + A[3] * cHbox * cH + A[4] * cH + A[5] * (cH ** 2) +\
              A[6] * (cuH ** 2) + A[7] * cuH + A[8] * cuH * cHD + A[9] * cHD + A[10] * (cHD ** 2) +\
              A[11] * cHbox * cHD + A[12] * cHD * cH + A[13] * cHbox * cuH + A[14] * cuH * cH + \
              A[15] * cHG + A[16] * (cHG ** 2) + A[17] * cHG * cHbox + A[18] * cHG * cH +\
              A[19] * cHG * cuH + A[20] * cHG * cHD
        return Rhh

    def get_param_expr(self, eft_parameters:Dict[str, float], precision:int=3):
        """Get the string representation of the EFT parameters and their values
        """
        kl_only = not any(eft_parameters[name] != 0 for name in ["cdp", "ctp", "ctp", "cpg"])
        # if SMEFT parameters correspond to a variation in klambda (chhh) only, express it
        # as kl_<value_str> instead
        if kl_only:
            heft_couplings = self.get_SMEFT_to_HEFT_couplings(**eft_parameters)
            kl = round(heft_couplings["chhh"], precision)
            param_expr = f"kl_{str_encode_value(kl)}"
        else:
            param_expr = super().get_param_expr(eft_parameters, precision=precision)
        return param_expr

    def _get_reweight_branch_data(self, observable_values:np.ndarray,
                                  eft_parameters:Dict[str, float]):
        """Get the per-event weight for a given SMEFT couplings via reweighting from given observable value
        """        
        order = self.config["order"]
        if order.upper() not in ["LO", "NLO"]:
            raise ValueError(f"unknown qcd order: {order}")
        # account for differences between BSM and SM XS 
        correction = 1
        weights = correction * self.get_eft_weights(observable_values, eft_parameters)
        den_weight = self.get_den_weight(eft_parameters)
        n_entry = len(observable_values)
        den_weights = np.full((n_entry,), den_weight, dtype=np.float32)
        final_weights = weights / den_weights
        if (order == "NLO") and (self.config["apply_kfactor"]):
            if (eft_parameters["ctG"] != 0):
                raise RuntimeError("Your translation and therefore k-factor is unreliable because ctG !=0 "
                                   "and the translation is only valid for ctG=0. \n Use SMEFT at LO or set "
                                   "ctG=0 to obtain the correct k-factor!")
            k_factor = self.get_k_factor(**eft_parameters)
            final_weights = final_weights * k_factor
        param_expr = self.get_param_expr(eft_parameters, self.config["param_str_precision"])
        branch_name_weight = self.config["branch_name_weight"].format(order=order,
                                                                      param_expr=param_expr)
        branch_data = {
            branch_name_weight: final_weights
        }
        return branch_data
    
    def run(self, smeft_parameters:Dict[str, float], infile:str, outfile:str,
            treename:str="CollectionTree"):
        super().run(infile=infile, outfile=outfile,
                    treename=treename, eft_parameters=smeft_parameters)