from typing import Dict, Optional, List, Union
import os

import numpy as np


from quickstats import AbstractObject
from quickstats.maths.numerics import str_decode_value, str_encode_value
from quickstats.utils.common_utils import combine_dict

import bbyy_analysis_framework
from .parameter_reweighting_tool import ParameterReweightingTool

class KLambdaReweightingTool(ParameterReweightingTool):
    
    CONFIG = {
        "reweight_filename"        : "weight-mHH-from-cHHHp01d0-to-cHHHpx_{bin_width}GeV_Jul28.root",
        "reweight_histname"        : "reweight_mHH_1p0_to_{param_str_value}",
        "branch_name_observable"   : "m_hh", # branch name of the observable used in reweighting
        "branch_name_weight"       : "{param_name}_weight_{bin_width}GeV_{param_str_value}",
        "branch_name_weight_err"   : "{param_name}_weight_err_{bin_width}GeV_{param_str_value}",
        "branch_name_xs_br"        : "crossSectionBR_{param_str_value}",
        "param_name"               : "kappa_lambda",  # name of parameter, just for naming the output weights
        "bin_width"                : 10, # weights have been saved for different binning options: 5, 10 or 20 GeV.
        "extra_branches"           : ("eventNumber", "m_hh", "weight"), # extra branches to save in the output
        "observable_scale"         : 0.001, # scale factor applied to the observable values
        # Branching ratio info can be found at https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageBR
        "BR_H_bb"                  : 0.5809,
        "BR_H_yy"                  : 0.002270,
        # polynomial coefficients for ggF production cross-section as a function of klambda
        # reference: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWGHH?redirectedfrom=LHCPhysics.LHCHXSWGHH
        "XS_coefficients"          : (70.3874, -50.4111, 11.0595),
        # Cross section correction: e.g. for bbyy we need to use XS(mh=125.09)/XS(mh=125)
        # but if your analysis uses XS(mh=125) you should probably set it to 1.
        'XS_correction'            : 31.02/31.0358, # XS(mh=125.09)/XS(mh=125 -> corrected to match MxAOD XS) for bbyy
        'param_min'                : -10, # minimum parameter value
        'param_max'                : 20,  # maximum parameter value
        'param_step'               : 0.2  # stepsize of parameter value
    }
    
    def initialize(self):
        filename  = self.config["reweight_filename"]
        bin_width = self.config["bin_width"]
        data_path = bbyy_analysis_framework.data_path
        filename  = os.path.join(data_path, filename.format(bin_width=bin_width))
        if not os.path.exists(filename):
            raise FileNotFoundError(f"missing weight file for klambda reweighting: {filename}")
        self.stdout.info(f'INFO: Initializing reweight information from "{filename}"')
        import ROOT
        f = ROOT.TFile(filename)
        from quickstats.utils.root_utils import is_corrupt
        if is_corrupt(f):
            raise RuntimeError(f"corrupted root file: {filename}")
        self.reweight_file = f
        
    def get_weight_histogram(self, param_str_value:str):
        """
        Extract weight histogram in mHH bins at a specific klambda scenario
        
        Parameters:
            param_str_value: str
                String representation of klambda value.
        """
        histname = self.config["reweight_histname"].format(param_str_value=param_str_value)
        h = self.reweight_file.Get(histname)
        if not h:
            raise RuntimeError(f'missing reweight histogram "{hist_name}" from the weight file')
        from quickstats.interface.root import TH1
        # pythonize histogram data
        py_h = TH1(h, dtype='float', overflow_bin=1)
        return py_h
    
    def get_weight_data(self, reweight_var_values:np.ndarray,
                        param_str_values:List[str]):
        """
        Extract the weight values and their errors for a given set of mHH values
        under different klambda scenarios
           
        Parameters:
            reweight_var_values: numpy array of float
                Values of mHH based on which the weights are extracted.
            param_str_values: list of str
                Array of string representation of klambda values.
        """
        weight_data     = {}
        weight_err_data = {}
        for param_str_value in param_str_values:
            weight_hist = self.get_weight_histogram(param_str_value)
            bins = weight_hist.bin_low_edge
            # make the max bin large enough so no mHH value will fall in the overflow bin
            bins[-1] = 1e9
            bin_index = np.digitize(reweight_var_values, bins) - 1
            weight_data[param_str_value]     = weight_hist.bin_content[bin_index]
            weight_err_data[param_str_value] = weight_hist.bin_error[bin_index]
        return weight_data, weight_err_data

    def get_xs_br(self, param_values:np.ndarray):
        """
        Get the ggF production cross-section times branching ratios for a given set of klambda values.
        
        Parameters:
            param_values: numpy array of float
                klambda values
        """
        param_values = np.array(param_values)
        br_hh = 2 * self.config["BR_H_bb"] * self.config["BR_H_yy"]
        xs_correction = self.config['XS_correction']
        p0, p1, p2 = self.config["XS_coefficients"]
        xs = (p0 + p1 * param_values + p2 * param_values * param_values) * xs_correction
        xs_br = br_hh * xs / 1000
        return xs_br
    
    def get_reweight_branch_data(self, reweight_var_values:np.ndarray):
        bin_width    = self.config['bin_width']
        param_min    = self.config['param_min']
        param_max    = self.config['param_max']
        param_step   = self.config['param_step']
        param_name   = self.config['param_name']
        param_values = np.arange(param_min, param_max + param_step, param_step)
        reweight_filename = self.config["reweight_filename"]
        # remove the base kl value which does not need to be reweighted
        if "cHHHp01d0" in reweight_filename:
            param_values = np.array([i for i in param_values if round(i, 8) != 1])
        elif "cHHHp10d0" in reweight_filename:
            param_values = np.array([i for i in param_values if round(i, 8) != 10])
        else:
            raise RuntimeError("unknown file for klambda reweighting")
        branch_data = {}
        # get string representation
        param_str_values = [str_encode_value(round(v, 8)) for v in param_values]
        weight_data, weight_err_data = self.get_weight_data(reweight_var_values, param_str_values)
        xs_br_data = self.get_xs_br(param_values)
        n_entry = len(reweight_var_values)
        for i, param_str_value in enumerate(param_str_values):
            branch_name_xs_br = self.config["branch_name_xs_br"].format(param_str_value=param_str_value)
            branch_data[branch_name_xs_br] = np.full((n_entry,), xs_br_data[i], dtype=np.float32)
            branch_name_weight     = self.config["branch_name_weight"].format(param_name=param_name,
                                                                              param_str_value=param_str_value,
                                                                              bin_width=bin_width)
            branch_name_weight_err = self.config["branch_name_weight_err"].format(param_name=param_name,
                                                                                  param_str_value=param_str_value,
                                                                                  bin_width=bin_width)
            branch_data[branch_name_weight]     = weight_data[param_str_value]
            branch_data[branch_name_weight_err] = weight_err_data[param_str_value]
        return branch_data
    
    def run(self, infile:str, outfile:str, treename:str="CollectionTree"):
        super().run(infile=infile, outfile=outfile, treename=treename)