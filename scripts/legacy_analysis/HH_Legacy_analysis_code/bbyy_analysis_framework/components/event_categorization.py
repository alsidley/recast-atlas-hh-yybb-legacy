from typing import Dict, List, Optional, Union, Tuple
import os
import json

import numpy as np

from quickstats.analysis import EventCategorization as QSEventCategorization
from quickstats.analysis import CategorizationOutputMode, CategorizationOutputFormat
from quickstats.maths.numerics import str_encode_value
from quickstats.parsers import ParamParser

from bbyy_analysis_framework.components import AnalysisBase

class EventCategorization(AnalysisBase, QSEventCategorization):
    
    REQUIRED_CONFIG_COMPONENTS = ["paths:*", "kl_reweighting:*", "names:tree_name"]
    
    def __init__(self, study_name:str, target_channels:List[str],
                 analysis_config:Optional[Union[Dict, str]]=None,
                 array_dir:Optional[str]=None, outdir:Optional[str]=None,
                 data_preprocessor=None, do_blind:bool=True,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        super(EventCategorization, self).__init__(study_name=study_name,
                                                  target_channels=target_channels,
                                                  analysis_config=analysis_config,
                                                  array_dir=array_dir,
                                                  outdir=outdir,
                                                  data_preprocessor=data_preprocessor,                         
                                                  verbosity=verbosity,
                                                  **kwargs)
        self.category_ggF_param_yields     = {}
        self.category_ggF_param_yields_err = {}
        
    def _resolve_reweighted_train_samples(self, train_samples:List, test_samples:List,
                                          reweight_config:Optional[Dict]=None) -> List[str]:
        resolved_train_samples = set(train_samples)
        base_sample            = reweight_config["base_sample"]
        reweighted_sample_expr = reweight_config["reweighted_sample"]
        reweighted_sample_expr = reweighted_sample_expr.replace("{param_val}", "<param_val[P]>")
        regex = ParamParser.get_fname_regex(reweighted_sample_expr, "")
        reweighted_train_samples = [i for i in train_samples if regex.match(i)]
        reweighted_test_samples  = [i for i in test_samples if regex.match(i)]
        reweighted_test_samples = list(set(reweighted_test_samples) - set(reweighted_train_samples))
        if (len(reweighted_train_samples) > 0) and (base_sample not in train_samples):
            self.stdout.info(f'INFO: The following training samples are reweighted from the base sample '
                             f'"{base_sample}": {", ".join(reweighted_train_samples)}')
            extra_text = ""
            if len(reweighted_test_samples) > 0:
                extra_text = f'and the associated reweighted samples ({", ".join(reweighted_test_samples)}) '
                resolved_train_samples |= set(reweighted_test_samples)
            self.stdout.info(f"INFO: The base sample {extra_text}will be treated as part of the train samples "
                             f"during categorization, yield calculation and minitree creation.")
            resolved_train_samples |= set([base_sample])
            reweight_sample = reweight_config["reference_sample"]
            resolved_train_samples |= set([reweight_sample])
        elif (base_sample in train_samples) and (len(reweighted_test_samples) > 0):
            self.stdout.info(f"INFO: The following test samples are reweighted from the base sample \"{base_sample}\" "
                             f"used in the training: {', '.join(reweighted_test_samples)}")
            self.stdout.info("INFO: They will be treated as part of the train samples during "
                             "categorization, yield calculation and minitree creation.")
            resolved_train_samples |= set(reweighted_test_samples)
        resolved_train_samples = list(resolved_train_samples)
        return resolved_train_samples
        
    def resolve_train_samples(self, channel:str) -> List[str]:
        channel_config  = self.config['channels'][channel]
        train_samples   = self.resolve_samples(channel_config['train_samples'])
        test_samples    = self.resolve_samples(channel_config['test_samples'])
        # add samples that are indirectly used in the training
        indirect_train_samples = self.resolve_samples(channel_config.get('indirect_train_samples', []))
        train_samples.extend(indirect_train_samples)
        for config_name in ["kl_reweighting", "kl_reweighting_alt"]:
            if config_name not in self.config:
                continue
            reweight_config = self.config[config_name]
            reweighted_train_samples = self._resolve_reweighted_train_samples(train_samples,
                                                                              test_samples,
                                                                              reweight_config)
            train_samples.extend(reweighted_train_samples)
        train_samples = list(set(train_samples))
        return train_samples
    
    def transform_cat(self, orig_cat_name:str, new_cat_name:str, cat_map:Dict) -> None:
        self.validate(check_df=True)
        for sample in self.channel_df:
            df = self.channel_df[sample]
            for orig_val, new_val in cat_map.items():
                mask = (df[orig_cat_name]//1000 == orig_val//1000)
                df.loc[mask, [new_cat_name]] = new_val
                
    def transform_score(self, orig_score_name:str, new_score_name:str) -> None:
        self.validate(check_df=True)
        for sample in self.channel_df:
            df = self.channel_df[sample]
            self.channel_df[sample] = df.rename(columns={orig_score_name:new_score_name})
    
    def get_blind_selection(self):
        """
        Return the selection that should be applied to obtain blinded data
        """        
        observable  = self.config["observable"]["name"]
        blind_range = self.config["observable"]["blind_range"]
        query_str = f"(({observable} < {blind_range[0]}) or ({observable} > {blind_range[1]}))"
        return query_str
    
    def get_samples_to_load(self, channel:Optional[str]=None):
        channel = self._parse_channel(channel)
        channel_config = self.channel_configs[channel]
        samples = list(channel_config['test_samples'])
        kl_reweighting_sample = self.config['kl_reweighting']['reference_sample']
        samples.append(kl_reweighting_sample)
        return samples
    
    def _fix_event_indices(self, event_indices:Optional[Dict[str, np.ndarray]]=None) -> None:
        """
        Correct the custom event numbers for samples used in reweighing.
        """
        samples = list(event_indices.keys())
        reweighted_sample_expr = self.config['kl_reweighting']['reweighted_sample']
        reweighted_sample_expr = reweighted_sample_expr.replace("{param_val}", "<param_val[P]>")
        regex = ParamParser.get_fname_regex(reweighted_sample_expr, "")
        reweighted_samples = [i for i in samples if regex.match(i)]
        channel_train_samples = self.channel_config['train_samples']
        if len(reweighted_samples) > 1:
            raise RuntimeError("can not use custom categorization events for multiple reweighted samples "
                               "(since samples reweighted from the same basis sample are "
                               "kinematically equivalent)")
        if len(reweighted_samples) == 1:
            base_sample = self.config['kl_reweighting']['base_sample']
            reweighted_sample = reweighted_samples[0]
            if reweighted_sample in channel_train_samples:
                event_number = event_indices[reweighted_sample]
                event_indices[base_sample] = event_number
                self.stdout.info("INFO: Fixed custom categorization events for reweighted samples.")    
    
    def load_channel_df(self, samples:Optional[List]=None, apply_score:bool=True,
                        event_indices:Optional[Dict[str, np.ndarray]]=None) -> None:
        self.validate()
        if event_indices is not None:
            self._fix_event_indices(event_indices)
        super().load_channel_df(samples=samples, apply_score=apply_score,
                                event_indices=event_indices)  
    
    def get_category_ggF_param_yields(self, resampling:bool=False,
                                      resampling_random_state:Optional[int]=None):
        self.validate(check_df=True, check_categories=True)
        channel = self.active_channel
        category_indices = self.get_category_indices(valid_only=False)
        ref_sample = self.config['kl_reweighting']['reference_sample']
        kl_min = self.config['parameterization']['klambda']['min']
        kl_max = self.config['parameterization']['klambda']['max']
        kl_step = self.config['parameterization']['klambda']['step']
        weight_expr = self.config['kl_reweighting']['weight']
        kl_values = np.arange(kl_min, kl_max + kl_step, kl_step)
        kl_str_values = [str_encode_value(round(v, 8)) for v in kl_values]
        yields     = {}
        yields_err = {}
        
        for category_index in category_indices:
            category_name = self.get_category_name(category_index)
            yields[category_name] = {}
            yields_err[category_name] = {}            
            df = self.get_category_df(ref_sample, category_index,
                                      resampling=resampling,
                                      resampling_random_state=resampling_random_state)
            for kl_str_value in kl_str_values:
                # skip SM sample
                if kl_str_value == "1p0":
                    continue
                weight_kl = weight_expr.format(param_val=kl_str_value)
                weight_eval = f'{self.names["total_weight"]} * ({weight_kl})'
                yield_kl = df.eval(weight_eval).sum()
                yield_err_kl = np.sqrt((df.eval(weight_eval) ** 2).sum())
                yields[category_name][kl_str_value] = yield_kl
                yields_err[category_name][kl_str_value] = {"errlo": yield_err_kl, "errhi": yield_err_kl}
        return yields, yields_err
    
    def update_category_ggF_param_yields(self):
        """
        Update category yields for ggF reweighted samples in the current channel after category labels are applied.
        """
        self.stdout.info(f"INFO: Evaluating yields for ggF parameterization")
        yields, yields_err = self.get_category_ggF_param_yields()
        channel = self.active_channel
        self.category_ggF_param_yields[channel]     = yields
        self.category_ggF_param_yields_err[channel] = yields_err
        
    def save_channel_ggF_param_yields(self, channel:Optional[str]=None,
                                      resampling:bool=False,
                                      resampling_random_state:Optional[int]=None) -> None:
        """
        Save ggF parameterized yield information for a given channel.
        
        Parameters
        ----------
        channel: (optional) str
            The channel from which the yield information is retrieved.
            By default, the current channel is used.
        resampling: bool, default = False
            Whether to resample the events (for bootstrapping).
        resampling_random_state: (optional) int
            The random state used to resample the events.                
        """
        super().save_channel_yields(channel=channel,
                                    resampling=resampling,
                                    resampling_random_state=resampling_random_state)
        channel = self._parse_channel(channel)
        if (channel not in self.category_ggF_param_yields) or (channel not in self.category_ggF_param_yields_err):
            raise RuntimeError(f'ggF parameterised yield information for the channel "{channel}" not initialized')
        if resampling and (channel != self.active_channel):
            raise RuntimeError('resampling can only be performed on the active channel')
        if resampling:
            yields, yields_err = self.get_category_ggF_param_yields(resampling=resampling,
                                                                    resampling_random_state=resampling_random_state)
        else:
            yields     = self.category_ggF_param_yields[channel]
            yields_err = self.category_ggF_param_yields_err[channel]
        for category, category_yields in yields.items():
            savepath = self.get_file("yield_data_ggF_param", category=category)
            with open(savepath, "w") as outfile:
                json.dump(category_yields, outfile, indent=2)
            self.stdout.info(f'INFO: Saved ggF parameterized yield information to {savepath}')
        for category, category_yields_err in yields_err.items():
            savepath = self.get_file("yield_err_data_ggF_param", category=category)
            with open(savepath, "w") as outfile:
                json.dump(category_yields_err, outfile, indent=2)
            self.stdout.info(f'INFO: Saved ggF parameterized yield uncertainty information to {savepath}')
    
    def save_channel_data_points(self, fmt:str="%1.3f",
                                 resampling:bool=False,
                                 resampling_random_state:Optional[int]=None) -> None:
        """
        Save data points of the discriminant variable event-by-event in the form of a text file
        for the current chanenl.
        
        Parameters
        ----------
        fmt: string, default = "%1.3f"
            Numerical formatting of the data points.
        resampling: bool, default = False
            Whether to resample the events (for bootstrapping).
        resampling_random_state: (optional) int
            The random state used to resample the events.                
        """

        self.validate(check_df=True, check_boundaries=True, check_categories=True)
        
        binary_fmt = fmt.encode('ascii')
        category_indices = self.get_category_indices()
        observable = self.config["observable"]["name"]
        self.path_manager.makedirs(["data"])
        for category_index in category_indices:
            category_name = self.get_category_name(category_index)
            df = self.get_category_df("data", category_index)
            if self.do_blind:
                df = self.get_blind_df(df)
            if resampling:
                df = self.get_resampled_df(df, random_state=resampling_random_state)
            data_points = df[observable].values
            savepath = self.get_file("data_point", category=category_name)
            np.savetxt(savepath, data_points, fmt=binary_fmt)
            self.stdout.info(f'INFO: Exported data points for the variable "{observable}" to {savepath}')
            
    def _get_minimal_sample_outputs(self):
        """Get minimal set of output samples to be saved; analysis specific
        """
        ARRAY     = CategorizationOutputFormat.ARRAY
        MINITREE  = CategorizationOutputFormat.MINITREE
        HISTOGRAM = CategorizationOutputFormat.HISTOGRAM        
        minimal_samples = {
            ARRAY     : [],
            MINITREE  : list(self.config['signal_modelling']['samples']),
            HISTOGRAM : []
        }
        return minimal_samples
    
    def _get_standard_sample_outputs(self):
        """Get minimal set of output samples to be saved; analysis specific
        """
        all_samples = list(self.channel_config['test_samples'])
        ARRAY     = CategorizationOutputFormat.ARRAY
        MINITREE  = CategorizationOutputFormat.MINITREE
        HISTOGRAM = CategorizationOutputFormat.HISTOGRAM        
        minimal_samples = {
            ARRAY     : all_samples,
            MINITREE  : list(self.config['signal_modelling']['samples']),
            HISTOGRAM : []
        }
        return minimal_samples    
    
    def _get_required_sample_outputs(self, mode:Union[str, CategorizationOutputMode]="minimal"):
        """
        Return the list of samples to be saved in array (csv/h5), histogram and minitree formats 
        in the current channel.

        The full sample list is taken from "test_samples" of a channel-level configuration.

        Parameters
        ----------
        mode: str or CategorizationOutputMode
            Output mode. Available choices are
                "minimal"  : custom minimal output mode defined by analysis
                "minitree" : save minitrees for all samples
                "array"    : save array (csv/h5) for all samples
                "standard" : custom standard output mode defined by analysis
                "full"     : save array (csv/h5), histograms and minitrees for all samples
        """
        sample_outputs = super()._get_required_sample_outputs(mode)
        resolved_mode = CategorizationOutputMode.parse(mode)
        MINITREE = CategorizationOutputFormat.MINITREE
        if mode == CategorizationOutputMode.ARRAY:
            sample_outputs[MINITREE] = list(self.config['signal_modelling']['samples'])
        return sample_outputs
    
    def save_channel_outputs(self, cache:bool=True, mode:str="minimal",
                             save_yield:bool=True,
                             save_data:bool=True,
                             save_summary:bool=True,
                             resampling:bool=False,
                             resampling_random_state:Optional[int]=None,
                             **kwargs):
        """
        Save collection of category outputs for the current channel.
        
        Parameters
        ----------
        resampling: bool, default = False
            Whether to resample the events (for bootstrapping).
        resampling_random_state: (optional) int
            The random state used to resample the events.                
        """ 
        super().save_channel_outputs(cache=cache, mode=mode,
                                     save_yield=save_yield,
                                     save_summary=save_summary,
                                     resampling=resampling,
                                     resampling_random_state=resampling_random_state,
                                     **kwargs)
        channel = self.active_channel
        if save_yield:
            self.update_category_ggF_param_yields()
            self.save_channel_ggF_param_yields(channel,
                                               resampling=resampling,
                                               resampling_random_state=resampling_random_state)
            
        if save_data:
            self.save_channel_data_points(resampling=resampling,
                                          resampling_random_state=resampling_random_state)
    
    def get_yield_files_and_description(self):
        result = {
            "yield_data"               : "yield information",
            "yield_err_data"           : "yield uncertainty information",
            "yield_data_ggF_param"     : "ggF parameterized yield information",
            "yield_err_data_ggF_param" : "ggF parameterized yield uncertainty information"
        }
        return result