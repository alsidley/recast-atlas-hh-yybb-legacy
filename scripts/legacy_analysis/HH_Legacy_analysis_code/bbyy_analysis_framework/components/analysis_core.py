from typing import Dict, List, Optional, Union
import os
import glob
import json
import shutil

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

import bbyy_analysis_framework
from bbyy_analysis_framework.components import (AnalysisBase, EventCategorization,
                                                SignalParameterization, WorkspaceXMLMaker)
from quickstats.utils.common_utils import combine_dict
from quickstats.maths.numerics import str_encode_value

class AnalysisCore(AnalysisBase):
    
    REQUIRED_CONFIG_COMPONENTS = ["*"]
    
    @property
    def study_name(self):
        return self.path_manager.study_name
    
    @property
    def target_channels(self):
        return self.categorizer.target_channels
    
    @property
    def data_preprocessor(self):
        return self.categorizer.data_preprocessor
    
    @property
    def benchmark_significance(self):
        return self.categorizer.benchmark_significance
    
    @property
    def score_boundaries(self):
        score_boundaries = {}
        for channel in self.target_channels:
            if channel in self.categorizer.summary:
                # make a copy
                channel_boundary = combine_dict(self.categorizer.summary[channel]["boundary"])
                score_boundaries[channel] = channel_boundary
        return score_boundaries
    
    @property
    def class_significance(self):
        class_significance = {}
        for channel in self.target_channels:
            if channel in self.categorizer.summary:
                # make a copy
                channel_significance = combine_dict(self.categorizer.summary[channel]["significance"])
                class_significance[channel] = channel_significance
        return class_significance    
    
    @property
    def models(self):
        return self.categorizer.models
    
    def __init__(self, study_name:str, target_channels:List[str],
                 analysis_config:Optional[Union[Dict, str]]=None,
                 plot_config:Optional[Union[Dict, str]]=None, outdir:Optional[str]=None,
                 array_dir:Optional[str]=None, output_mode:str="minimal",
                 cache:bool=True, do_blind:bool=True, data_preprocessor=None,
                 verbosity:Optional[Union[int, str]]="INFO",
                 **kwargs):
        """
        
        Module for performing the statistical analysis pipelines.
        
        Arguments:
            study_name: str
                Name of the study (for grouping outputs)
            target_channels: list of str
                List of channels to be included in the study
            analysis_config: str or dictionary
                Path to the central configuration file of the analysis;
                Alternatively, configuration in the form of a dictionary can be supplied.
            plot_config: (optional) str or dictionary
                Path to the configuration file used for plotting. If not specified, the
                default configuration file from the framework will be used.
                Alternatively, configuration in the form of a dictionary can be supplied.
            outdir: (optional) str
                Save outputs to a custom directory instead of the default directory
                from the configuration file (i.e. config['paths']['outputs'])
            array_dir: (optional) str
                Load csv array inputs from a custom directory instead of the default
                directory from the configuration file (i.e. config['paths']['arrays'])
            output_mode: str, default = "minimal"
                Determine the type of outputs to save after event categorization. Available modes:
                    "minimal"  : Only save minitrees needed for signal modelling
                    "minimal"  : Save minitrees for all samples
                    "csv"      : Save csv for all samples and only minitrees needed for signal modelling
                    "standard" : Save csv and minitrees for all samples
                    "full"     : Save csv, hist and minitrees for all samples
            do_blind: bool
                Perform a blind analysis. This will remove data events from the signal region when
                saving outputs for statistical tests.
            data_preprocessor: callable function
                Function for preprocessing input data after which the output is
                fed to the machine learning model for prediction.
            verbosity: (optional) str
                Message verbosity level. Available chocie: ['DEBUG', 'INFO', 'WARNING', 'ERROR']
        """
        
        super().__init__(analysis_config=analysis_config,
                         plot_config=plot_config,
                         outdir=outdir,
                         array_dir=array_dir,
                         verbosity=verbosity,
                         **kwargs)
        
        # append the study name to the output directory
        self.set_study_name(study_name)
        
        self.categorizer = EventCategorization(study_name=study_name,
                                               target_channels=target_channels,
                                               analysis_config=analysis_config,
                                               array_dir=array_dir, outdir=outdir,
                                               data_preprocessor=data_preprocessor,
                                               do_blind=do_blind,
                                               disable_config_message=True,
                                               verbosity=verbosity)
        
        # use same path manager for both analysis core and event categorizer
        self.categorizer.path_manager = self.path_manager
        
        self.output_mode = output_mode
        self.cache = cache
        self.kl_intersections = None
        self.k2v_intersections = None
        self._categories = None
        self.do_blind = do_blind
        self.dataset_source = "obs_data"        
        self.limits = None
        self.likelihoods = None
        
    def _get_plot_options(self, name:str, custom_configs:Optional[Dict]=None):
        from quickstats.utils.common_utils import combine_dict
        analysis_label_options = self.plot_config["general"]["analysis_label_options"]
        styles = self.plot_config["general"]["styles"]
        draw_options = {}
        plot_options = {}
        label_map    = {}
        if name in self.plot_config:
            analysis_label_options = combine_dict(analysis_label_options,
                                                  self.plot_config[name].get("analysis_label_options", {}))
            styles = combine_dict(styles, self.plot_config[name].get("styles", {}))
            draw_options = combine_dict(draw_options, self.plot_config[name].get("draw_options", {}))
            plot_options = combine_dict(plot_options, self.plot_config[name].get("plot_options", {}))
            label_map    = combine_dict(label_map, self.plot_config[name].get("label_map", {}))
        if custom_configs is not None:
            analysis_label_options = combine_dict(analysis_label_options,
                                                  custom_configs.get("analysis_label_options", {}))
            styles = combine_dict(styles, custom_configs.get("styles", {}))
            draw_options = combine_dict(draw_options, custom_configs.get("draw_options", {}))
            plot_options = combine_dict(plot_options, custom_configs.get("plot_options", {}))
            label_map    = combine_dict(label_map, custom_configs.get("label_map", {}))
        general_options = {
            "analysis_label_options": analysis_label_options,
            "styles": styles
        }
        return general_options, draw_options, plot_options, label_map
    
    def set_models(self, models:Dict)->None:
        self.categorizer.set_models(models)  
    
    def get_pretty_category_name(self, category:str):
        category = category.replace("BSM", "Low mass region")
        category = category.replace("LM", "Low mass region")
        category = category.replace("SM", "High mass region").replace("_", " ")
        category = category.replace("HM", "High mass region")
        return category
    
    def register_categories(self, categories:List[str]):
        self._categories = categories        
        
    def get_categories(self):
        if self._categories is None:
            raise RuntimeError("categories not defined")
        return self._categories    
        
    def load_boundaries(self, boundary_dir:Optional[str]=None):
        for channel in self.target_channels:
            if boundary_dir is None:
                self.categorizer.load_boundaries(channel=channel)
            else:
                basename = self.path_manager.files["boundary_data"].basename.format(channel=channel)
                boundary_path = os.path.join(boundary_dir, basename)
                self.categorizer.load_boundaries(boundary_path, channel=channel)
        categories = self.categorizer.get_all_category_names()
        self.register_categories(categories)

    def run_categorization(self, event_indices:Optional[Dict[str, Dict[str, np.ndarray]]]=None,
                           cache_boundaries:Optional[bool]=None,
                           cache_categories:Optional[bool]=None,
                           cache_scores:Optional[bool]=None,
                           cache_output:Optional[bool]=None):
        """
        Perform event categorization for the target channels and save the output files accordingly.
            Arguments:
                event_indices: Dictionary of numpy arrays / list
                    A map from the sample name to the list of event numbers to be used in event
                    categorization.
                cache_boundaries: (optional) bool, default = None
                    Whether to cache boundary data. If not specified, the internal caching mode is used.
                cache_categories: (optional) bool, default = None
                    Whether to cache application of category columns. If not specified, the internal caching mode is used.
                cache_categories: (optional) bool, default = None
                    Whether to cache application of score columns. If not specified, the internal caching mode is used.
                cache_output: (optional) bool, default = None
                    Whether to cache categorization output. If not specified, the internal caching mode is used.                    
        """
        if cache_boundaries is None:
            cache_boundaries = self.cache
        if cache_categories is None:
            cache_categories = self.cache
        if cache_scores is None:
            cache_scores = self.cache
        if cache_output is None:
            cache_output = self.cache            
        for channel in self.target_channels:
            if event_indices is not None:
                channel_event_indices = event_indices[channel]
            else:
                channel_event_indices = None
            self.categorizer.run_channel_categorization(channel, cache_boundaries=cache_boundaries,
                                                        cache_categories=cache_categories,
                                                        cache_scores=cache_scores,
                                                        event_indices=channel_event_indices)
            self.categorizer.evaluate_benchmark_significance()
            self.categorizer.save_channel_outputs(mode=self.output_mode, cache=cache_output)
        self.load_boundaries()
        self.categorizer.merge_category_outputs(cache=cache_output)

    def create_bootstrap_outputs(self, study_names:Union[str, List[str]],
                                 random_states:Union[int, List[int]],
                                 cache_boundaries:Optional[bool]=None,
                                 cache_output:Optional[bool]=None):
        temp_name = self.path_manager.study_name
        if isinstance(study_names, str):
            study_names = [study_names]
        if isinstance(random_states, int):
            random_states = [random_states]
        if len(random_states) != len(study_names):
            raise RuntimeError("dimension of output names must match the dimension of random states")
        for channel in self.target_channels:
            self.set_study_name(temp_name)
            self.categorizer.run_channel_categorization(channel, cache_boundaries=cache_boundaries)
            for study_name, random_state in zip(study_names, random_states):
                self.set_study_name(study_name)
                self.categorizer.save_channel_outputs(mode=self.output_mode,
                                                      cache=cache_output,
                                                      resampling=True,
                                                      resampling_random_state=random_state)
        for study_name in study_names:
            self.set_study_name(study_name)
            self.categorizer.merge_category_outputs(cache=cache_output)
        self.set_study_name(temp_name)
        
    def hadd_signal_minitrees(self):
        prefix  = self.config['signal_modelling']['minitree_prefix']
        samples = self.config['signal_modelling']['samples']
        categories = self.get_categories()
        for category in categories:
            sample_minitree_paths = []
            for sample in samples:
                minitree_path = self.get_file("categorized_minitree_sample",
                                              sample=sample, category=category)
                if not os.path.exists(minitree_path):
                    raise RuntimeError(f'missing signal minitree "{minitree_path}"')
                sample_minitree_paths.append(minitree_path)
            combined_minitree_path = self.get_file("categorized_minitree_sample",
                                                   sample=prefix, category=category)
            hadd_command = "hadd -f {} {}".format(combined_minitree_path, " ".join(sample_minitree_paths))
            os.system(hadd_command)
    
    def plot_score_distribution_binary(self, plot_siginficance:bool=True,
                                       custom_configs:Optional[Dict]=None):
        
        score_name  = self.names['score']
        weight_name = self.names['total_weight']
        
        if "score_distribution" not in self.plot_config:
            raise RuntimeError("missing plot config for score distribution plot")
        general_options, draw_options, _, _ = self._get_plot_options("score_distribution", custom_configs)
        draw_options["column_name"] = score_name
        draw_options["weight_name"] = weight_name        
        plot_options = self.plot_config["score_distribution"]["plot_options"]
        self.path_manager.makedirs(["plot"])
        from quickstats.plots import ScoreDistributionPlot
        for channel in self.categorizer.target_channels:
            sample_dfs = self.categorizer.load_cached_category_df(channel)
            try:
                if channel not in self.score_boundaries:
                    self.load_boundaries()
                boundaries = self.score_boundaries[channel]
                boundaries = list(set(sum(boundaries.values(), [])))
            except:
                boundaries = []
            category_text = self.get_pretty_category_name(channel)
            extra_text = f"{category_text}//"
            if plot_siginficance:
                significance = self.class_significance[channel]
                extra_text += f"Z = {significance:.2f}"
            general_options["analysis_label_options"]["extra_text"] = extra_text
            
            plotter = ScoreDistributionPlot(sample_dfs, plot_options=plot_options, 
                                            **general_options)
            draw_options["boundaries"] = boundaries
            ax = plotter.draw(**draw_options)
            savepath = self.get_file("score_distribution_plot", channel=channel)
            plt.savefig(savepath, bbox_inches="tight")
            plt.show()
        
    @staticmethod
    def _match_key(dictionary:Dict, category:str):
        channel = [i for i in dictionary.keys() if category.startswith(i)]
        return channel[0]
            
    def run_parameterisation(self):
        self.path_manager.makedirs(["summary"])
        param_tool = SignalParameterization(analysis_config_path=self.get_file("analysis_config"),
                                            study_name=self.study_name,
                                            outdir=self.path_manager.raw_base_path,
                                            verbosity=self.stdout.verbosity._name_)
        param_tool.run_param()
        param_tool.save_param()

    def run_signal_modelling(self, modelling_config:Optional[Dict]=None,
                             plot:bool=True, summary=True, parallel:int=0):
        from quickstats.components.modelling import DataModelling
        minitree_dir = self.get_directory("categorized_minitree")
        categories = self.get_categories()
        if modelling_config is None:
            modelling_config = self.config['signal_modelling']
        else:
            modelling_config = combine_dict(self.config['signal_modelling'], modelling_config)
        
        init_kwargs = {
            "functional_form" : modelling_config['functional_form'],
            "observable"      : self.config['observable']['name'],
            "fit_range"       : modelling_config['fit_range'],
            "n_bins"          : modelling_config['n_bins'],
            "weight"          : self.names['total_weight'],
            "fit_options"     : {
                "asymptotic": True,
                "strategy": 2
            }
        }
        modelling_tool = DataModelling(**init_kwargs)
        
        run_kwargs = {
            "prefix"               : modelling_config['minitree_prefix'],
            "categories"           : categories,
            "tree_name"            : self.treename,
            "input_dir"            : minitree_dir,
            "parallel"             : parallel,
            "save_merged_param_as" : self.get_file("signal_modelling_data")
        }
        
        if plot:
            save_plot_as = []
            plot_options = []
            for category in categories:
                savepath = self.get_file("signal_modelling_plot", category=category)
                save_plot_as.append(savepath)
                plot_range  = modelling_config.get("plot_range", None)
                plot_n_bins = modelling_config.get("plot_n_bins", 60)
                plot_options.append({"xlabel": r"$m_{\gamma\gamma}$ [GeV]",
                                     "annotations": {"text": category, "xy":(0.9, 0.65),
                                     "horizontalalignment":"right"},
                                     "bin_range": plot_range,
                                     "n_bins_data": plot_n_bins})
            run_kwargs["save_plot_as"] = save_plot_as
            run_kwargs["plot_options"] = plot_options

        if summary:
            save_summary_as = []
            for category in categories:
                savepath = self.get_file("signal_modelling_summary", category=category)
                save_summary_as.append(savepath)
            run_kwargs["save_summary_as"] = save_summary_as
        
        self.path_manager.makedirs(["signal_model", "plot"])
        if parallel != 0:
            # import ROOT first to prevent hanging in parallel execution
            import ROOT
        modelling_tool.run_over_categories(**run_kwargs)
    
    def copy_dtd(self):
        resource_dir = bbyy_analysis_framework.resource_path
        resource_path = os.path.join(resource_dir, "AnaWSBuilder.dtd")
        if not os.path.exists(resource_path):
            raise FileNotFoundError(f'missing resource file "{resource_path}"')
            
        self.path_manager.makedirs(["xml_config", "xml_model", "xml_category"])
        
        input_xml_path    = self.get_directory("xml_config")    
        shutil.copy(resource_path, input_xml_path)
        self.stdout.info(f'INFO: Copied AnaWSBuilder.dtd to {input_xml_path}')
        
        model_xml_path    = self.get_directory("xml_model")
        shutil.copy(resource_path, model_xml_path)
        self.stdout.info(f'INFO: Copied AnaWSBuilder.dtd to {model_xml_path}')
        
        category_xml_path = self.get_directory("xml_category")
        shutil.copy(resource_path, category_xml_path)
        self.stdout.info(f'INFO: Copied AnaWSBuilder.dtd to {category_xml_path}')
        
    def copy_data(self):
        self.path_manager.makedirs(["xml_data"])
        xml_data_dir = self.get_directory("xml_data")
        if self.dataset_source == "obs_data":
            for category in self.get_categories():
                data_path = self.get_file("data_point", category=category)
                shutil.copy(data_path, xml_data_dir)
        elif self.dataset_source == "yy_MC":
            for category in self.get_categories():
                hist_path = self.get_file("categorized_histogram", sample="yy", category=category)
                shutil.copy(hist_path, xml_data_dir)
        else:
            raise RuntimeError(f'unsupported dataset mode "{data_mode}", please choose '
                               'between "yy_MC" or "obs_data"')
        
    def create_workspace_xmls(self, categories:Optional[List[str]]=None,
                              systematics:Optional[Dict]=None):
        if categories is None:
            categories = self.get_categories()
        xml_maker = WorkspaceXMLMaker(categories=categories,
                                      analysis_config=self.config,
                                      do_blind=self.do_blind,
                                      path_manager=self.path_manager,
                                      systematics=systematics,
                                      verbosity=self.stdout.verbosity)
        xml_maker.create_xmls()
        self.copy_data()
        self.copy_dtd()
            
    def create_workspace(self):
        from quickstats.components.workspaces import XMLWSBuilder
        xml_dir = self.get_directory("xml")
        input_xml_non_param = self.get_file("input_xml_non_param")
        input_xml_param = self.get_file("input_xml_param")
        for input_xml in [input_xml_non_param, input_xml_param]:
            xml_builder = XMLWSBuilder(input_xml, basedir=xml_dir)
            xml_builder.generate_workspace()
            
    def create_mH_modified_workspace(self, mH_value:float,
                                     modification_config:Union[str, Dict],
                                     minimizer_config:Optional[Dict]=None):
        from quickstats.components.workspaces import XMLWSModifier
        mH_str = str_encode_value(mH_value)
        modifier = XMLWSModifier(modification_config,
                                 minimizer_config=minimizer_config)
        for infile, outfile in [("workspace_non_param", "workspace_non_param_mH"),
                                ("workspace_param", "workspace_param_mH")]:
            input_ws_path = self.get_file(infile, validate=True)
            output_ws_path = self.get_file(outfile, mH_value=mH_str)
            modifier.create_modified_workspace(infile=input_ws_path,
                                               outfile=output_ws_path)
    
    def create_asimov_workspace(self, mH_value:Optional[float]=None):
        if mH_value is None:
            filename = self.get_file("workspace_param", validate=True)
            savename = self.get_file("workspace_asimov_param")
        else:
            mH_str = str_encode_value(mH_value)
            filename = self.get_file("workspace_param_mH", validate=True, mH_value=mH_str)
            savename = self.get_file("workspace_asimov_param_mH", mH_value=mH_str)
        from quickstats.components import AsimovType
        if self.do_blind:
            asimov_types = [AsimovType.S_NP_Nom]
        else:
            asimov_types = [AsimovType.S_NP_Fit]
        from quickstats.components import AsimovGenerator     
        generator = AsimovGenerator(filename, data_name="combData",
                                    poi_name="mu_HH", verbosity=self.stdout.verbosity)
        generator.generate_standard_asimov(asimov_types)
        generator.save(savename)
        
    def run_likelihood_scan(self, kl_scan:bool=True, k2v_scan:bool=True,
                            kl_k2v_scan:bool=False, mH_value:Optional[float]=None,
                            cache:Optional[bool]=None,
                            parallel:int=-1):
        if cache is None:
            cache = self.cache
        from bbyy_analysis_framework.components import LikelihoodResultSetup
        likelihood_tasks = {
            LikelihoodResultSetup.KL_SCAN      : kl_scan,
            LikelihoodResultSetup.K2V_SCAN     : k2v_scan,
            LikelihoodResultSetup.KL_K2V_SCAN  : kl_k2v_scan
        }
        
        from quickstats.concurrent import ParameterisedLikelihood
        
        if ("likelihood_scan" not in self.config):
            raise RuntimeError("missing configuration for likelihood scan")
        
        if self.do_blind:
            data_name = "asimovData_1_NP_Nominal"
        else:
            data_name = "asimovData_1_NP_Profile"
        
        if mH_value is None:
            mH_str = None
            filename = self.get_file("workspace_asimov_param", validate=True)
        else:
            mH_str = str_encode_value(mH_value)
            filename = self.get_file("workspace_asimov_param_mH", mH_value=mH_str, validate=True)
            
        self.path_manager.makedirs(["likelihood"])
        likelihood_dir = self.get_directory("likelihood")
            
        for task in likelihood_tasks:
            
            if not likelihood_tasks[task]:
                continue
                
            if (task.abbrev not in self.config["likelihood_scan"]):
                raise RuntimeError(f"missing likelihood scan configuration for the task: {task.abbrev}")
            param_expr = self.config["likelihood_scan"][task.abbrev]
            
            if mH_str is None:
                outname = self.get_file("likelihood_data", target=task.abbrev)
                cachedir = f"cache/{task.abbrev}"
            else:
                outname = self.get_file("likelihood_data_mH", target=task.abbrev, mH_value=mH_str)
                cachedir = f"cache_{mH_str}/{task.abbrev}"
                
            task_kwargs = {
                "input_file" : filename,
                "param_expr" : param_expr,
                "data_name"  : data_name,
                "outdir"     : likelihood_dir,
                "outname"    : outname,
                "config"     : {"snapshot_name": data_name},
                "cache"      : cache,
                "cachedir"   : cachedir,
                "save_log"   : True,
                "parallel"   : parallel
            }
            likelihood_runner = ParameterisedLikelihood(**task_kwargs)
            likelihood_runner.run()
    
    def run_limits(self, SM_limit:bool=True, SM_VBF_limit:bool=True,
                   kl_scan:bool=True, k2v_scan:bool=True,
                   k2v_vbf_only_scan:bool=True,
                   mH_value:Optional[float]=None,
                   cache:Optional[bool]=None,
                   parallel:int=-1):
        if cache is None:
            cache = self.cache
        self.path_manager.makedirs(["workspace", "limit"])
        workspace_dir = self.get_directory("workspace")
        limit_dir = self.get_directory("limit")
        
        if mH_value is None:
            mH_str = None
        else:
            mH_str = str_encode_value(mH_value)
        
        common_kwargs = {
            "input_path"  : workspace_dir,
            "outdir"      : limit_dir,
            "cache"       : cache,
            "save_log"    : True,
            "save_summary": False,
            "parallel"    : parallel,
            "config"      : {"do_blind": self.do_blind}
        }
        
        from bbyy_analysis_framework.components import LimitResultSetup
        limit_tasks = {
            LimitResultSetup.SM_LIMIT     : SM_limit,
            LimitResultSetup.SM_VBF_LIMIT : SM_VBF_limit,
            LimitResultSetup.KL_SCAN      : kl_scan,
            LimitResultSetup.K2V_SCAN     : k2v_scan,
            LimitResultSetup.K2V_VBF_SCAN : k2v_vbf_only_scan
        }
        
        from quickstats.utils.common_utils import combine_dict
        from quickstats.concurrent import ParameterisedAsymptoticCLs
        
        for task in limit_tasks:
            if not limit_tasks[task]:
                continue
            if task.is_parameterized:
                if mH_str is None:
                    file_expr = self.get_file("workspace_param", validate=True)
                else:
                    file_expr = self.get_file("workspace_param_mH", mH_value=mH_str, validate=True)
                if "limit_scan" in self.config:
                    param_expr = self.config["limit_scan"][task.abbrev]
                else:
                    param_range = self.config['parameterization'][task.parameter]['scan_range']
                    param_min, param_max = param_range[0], param_range[1]
                    param_step = self.config['parameterization'][task.parameter]['step']
                    param_expr = f"{task.parameter}={param_min}_{param_max}_{param_step}"
            else:
                if mH_str is None:
                    file_expr = self.get_file("workspace_non_param", validate=True)
                else:
                    file_expr = self.get_file("workspace_non_param_mH", mH_value=mH_str, validate=True)
                param_expr = None
            file_expr = os.path.splitext(os.path.basename(file_expr))[0]
            if task.vbf_only:
                poi_name = "mu_HH_VBF"
            else:
                poi_name = "mu_HH"
            if mH_str is None:
                outname = self.get_file("limit_data", target=task.abbrev)
            else:
                outname = self.get_file("limit_data_mH", target=task.abbrev, mH_value=mH_str)
            outname = os.path.basename(outname)
            if mH_str is None:
                cachedir = f"cache/{task.abbrev}"
            else:
                cachedir = f"cache_{mH_str}/{task.abbrev}"
            task_kwargs = {
                "file_expr"  : file_expr,
                "param_expr" : param_expr,
                "outname"    : outname,
                "cachedir"   : cachedir,
                "config"     : {"poi_name": poi_name}
            }
            if task.fix_expr is not None:
                task_kwargs['config']['fix_param'] = task.fix_expr
            combined_kwargs = combine_dict(common_kwargs, task_kwargs)
            limit_runner = ParameterisedAsymptoticCLs(**combined_kwargs)
            limit_runner.run()
            
    def gather_limits(self, SM_limit:bool=True, SM_VBF_limit:bool=True, kl_scan:bool=True,
                      k2v_scan:bool=True, k2v_vbf_only_scan:bool=True, save_summary:bool=True,
                      mH_value:Optional[float]=None):
        from bbyy_analysis_framework.plots.limit_scan_plot import DiHiggsXS
        
        from bbyy_analysis_framework.components import LimitResultSetup
        limit_tasks = {
            LimitResultSetup.SM_LIMIT     : SM_limit,
            LimitResultSetup.SM_VBF_LIMIT : SM_VBF_limit,
            LimitResultSetup.KL_SCAN      : kl_scan,
            LimitResultSetup.K2V_SCAN     : k2v_scan,
            LimitResultSetup.K2V_VBF_SCAN : k2v_vbf_only_scan
        }
        
        if mH_value is None:
            mH_str = None
        else:
            mH_str = str_encode_value(mH_value)
        
        limit_results = {}
        for task in limit_tasks:
            if not limit_tasks[task]:
                continue
            if mH_str is None:
                limit_path = self.get_file("limit_data", target=task.abbrev, validate=True)
            else:
                limit_path = self.get_file("limit_data_mH", target=task.abbrev, mH_value=mH_str, validate=True)
            with open(limit_path, "r") as limit_file:
                limit_data = json.load(limit_file)
            if not task.is_parameterized:
                limit_results[f"{task.abbrev}_exp"] = limit_data['0'][0]
                if self.do_blind:
                    limit_results[f"{task.abbrev}_obs"] = 0.
                else:
                    limit_results[f"{task.abbrev}_obs"] = limit_data['obs'][0]
            else:
                df = pd.DataFrame(limit_data).set_index([task.parameter])
                xs_maker = DiHiggsXS(s=13, mH=mH_str, include_ggF=not task.vbf_only, include_VBF=True)
                coupling_values = df.index.astype(float).values
                try:
                    intersections    = xs_maker.get_intersections(task.parameter, coupling_values, df['0'])
                    limit_results[f"{task.abbrev}_exp"] = intersections
                    limit_results[f"{task.abbrev}_range_exp"] = abs(intersections[1] - intersections[0])
                except:
                    limit_results[f"{task.abbrev}_exp"] = None
                    limit_results[f"{task.abbrev}_range_exp"] = None
                if self.do_blind:
                    limit_results[f"{task.abbrev}_obs"] = 0.
                    limit_results[f"{task.abbrev}_range_obs"] = 0.
                else:
                    intersections    = xs_maker.get_intersections(task.parameter, coupling_values, df['obs'])
                    limits[f"{task.abbrev}_obs"] = intersections
                    limits[f"{task.abbrev}_range_obs"] = abs(intersections[1] - intersections[0])
            
        self.limits = limit_results
        if save_summary:
            self.path_manager.makedirs(["summary"])
            if mH_str is None:
                savepath = self.get_file("limit_summary")
            else:
                savepath = self.get_file("limit_summary_mH", mH_value=mH_str)
            with open(savepath, "w") as outfile:
                json.dump(limit_results, outfile, indent=2)
            self.stdout.info(f'INFO: Saved limit summary to "{savepath}".')
        return limit_results
    
    def gather_likelihoods(self, kl_scan:bool=True, k2v_scan:bool=True, save_summary:bool=True,
                           mH_value:Optional[float]=None):
        from bbyy_analysis_framework.components import LikelihoodResultSetup
        likelihood_tasks = {
            LikelihoodResultSetup.KL_SCAN      : kl_scan,
            LikelihoodResultSetup.K2V_SCAN     : k2v_scan
        }
        
        if mH_value is None:
            mH_str = None
        else:
            mH_str = str_encode_value(mH_value)
        
        from quickstats.maths.interpolation import get_intersections
        
        likelihood_results = {}
        for task in likelihood_tasks:
            if not likelihood_tasks[task]:
                continue
            if mH_str is None:
                data_path = self.get_file("likelihood_data", target=task.abbrev, validate=True)
            else:
                data_path = self.get_file("likelihood_data_mH", target=task.abbrev, mH_value=mH_str, validate=True)
            with open(data_path, "r") as file:
                data = json.load(file)
            df = pd.DataFrame(data)
            for sigma_label, qmu in {"one_sigma":1, "two_sigma": 4}.items():
                interval = get_intersections(df[task.abbrev], df["qmu"], qmu)[0]
                interval = list(np.round(interval, 8))
                likelihood_results[f"{task.abbrev}_{sigma_label}_exp"] = interval
                try:
                    likelihood_results[f"{task.abbrev}_{sigma_label}_range_exp"] = interval[1] - interval[0]
                except:
                    likelihood_results[f"{task.abbrev}_{sigma_label}_range_exp"] = None
            
        self.likelihoods = likelihood_results
        if save_summary:
            self.path_manager.makedirs(["summary"])
            if mH_str is None:
                savepath = self.get_file("likelihood_summary")
            else:
                savepath = self.get_file("likelihood_summary_mH", mH_value=mH_str)
            with open(savepath, "w") as outfile:
                json.dump(likelihood_results, outfile, indent=2)
            self.stdout.info(f'INFO: Saved likelihood summary to "{savepath}".')
        return likelihood_results
    
    def plot_limit_scan(self, kl_scan:bool=True, k2v_scan:bool=True,
                        k2v_vbf_only_scan:bool=True,
                        mH_value:Optional[float]=None):
        study_name = self.path_manager.study_name
        
        self.path_manager.makedirs(["plot"])
        
        from bbyy_analysis_framework.components import LimitResultSetup
        limit_tasks = {
            LimitResultSetup.KL_SCAN      : kl_scan,
            LimitResultSetup.K2V_SCAN     : k2v_scan,
            LimitResultSetup.K2V_VBF_SCAN : k2v_vbf_only_scan
        }
        
        if mH_value is None:
            mH_str = None
        else:
            mH_str = str_encode_value(mH_value)
        
        from bbyy_analysis_framework.plots import LimitScanPlot
        
        for task in limit_tasks:
            if not limit_tasks[task]:
                continue
            if mH_str is None:
                data_path = self.get_file("limit_data", target=task.abbrev, validate=True)
                savepath = self.get_file("limit_scan_plot", target=task.abbrev)
            else:
                data_path = self.get_file("limit_data_mH", target=task.abbrev, mH_value=mH_str, validate=True)
                savepath = self.get_file("limit_scan_plot_mH", target=task.abbrev, mH_value=mH_str)
            general_options, draw_options, _, _ = self._get_plot_options(f"{task.abbrev}_limit_scan")
            plotter = LimitScanPlot(data_path, param_name=task.parameter, mH=mH_str, **general_options)
            ax = plotter.draw(study_name, draw_observed=not self.do_blind, 
                              vbf_only=task.vbf_only, **draw_options)
            plt.savefig(savepath, bbox_inches="tight")
            self.stdout.info(f"INFO: Saved limit scan plot to {savepath}")
            plt.show()
            
    def plot_likelihood_scan(self, kl_scan:bool=True, k2v_scan:bool=True,
                             kl_k2v_scan:bool=False,
                             mH_value:Optional[float]=None):
        study_name = self.path_manager.study_name
        
        likelihood_dir = self.get_directory("likelihood")
        
        from bbyy_analysis_framework.components import LikelihoodResultSetup
        likelihood_tasks = {
            LikelihoodResultSetup.KL_SCAN      : kl_scan,
            LikelihoodResultSetup.K2V_SCAN     : k2v_scan,
            LikelihoodResultSetup.KL_K2V_SCAN  : kl_k2v_scan
        }
        
        if mH_value is None:
            mH_str = None
        else:
            mH_str = str_encode_value(mH_value)
        
        self.path_manager.makedirs(["plot"])        
        
        from bbyy_analysis_framework.plots import LikelihoodScanPlot
        
        for task in likelihood_tasks:
            if not likelihood_tasks[task]:
                continue
            if mH_str is None:
                data_path = self.get_file("likelihood_data", target=task.abbrev, validate=True)
                savepath = self.get_file("likelihood_scan_plot", target=task.abbrev)
            else:
                data_path = self.get_file("likelihood_data_mH", target=task.abbrev, mH_value=mH_str, validate=True)
                savepath = self.get_file("likelihood_scan_plot_mH", target=task.abbrev, mH_value=mH_str)
            with open(data_path, "r") as file:
                data = json.load(file)
            df = pd.DataFrame(data)
            general_options, draw_options, plot_options, _ = self._get_plot_options(f"{task.abbrev}_likelihood_scan")
            plotter = LikelihoodScanPlot(df, parameters=task.parameters,
                                         config=plot_options, **general_options)
            ax = plotter.draw(**draw_options)
            plt.savefig(savepath, bbox_inches="tight")
            self.stdout.info(f"INFO: Saved likelihood scan plot to {savepath}")
            plt.show()            
            
    def plot_variable_distributions(self, variables:Optional[Union[List[str], str]]=None,
                                    categories:Optional[Union[List[str], str]]=None,
                                    custom_configs:Optional[Dict]=None,
                                    selection:Optional[str]=None,
                                    cache_categories:bool=False,
                                    cache_scores:bool=False,
                                    use_raw_weight:bool=True):
        general_options, draw_options, plot_options, _ = self._get_plot_options("variable_distribution",
                                                                                custom_configs)
        required_samples = set()
        for key, options in plot_options.items():
            required_samples |= set(options["samples"])
        required_samples = sorted(list(required_samples))
        if use_raw_weight:
            weight_name = self.names["weight"]
        else:
            weight_name = self.names["total_weight"]
        if variables is None:
            variables = []
            for key, option in draw_options.items():
                variables.append(option["column_name"])
        if isinstance(variables, str):
            variables = [variables]
        all_variables = self.config["variables"]["all"]        
        for variable in variables:
            if variable not in all_variables:
                raise RuntimeError(f'"{variable}" is not a variable used in this study')
        if len(variables) == 0:
            raise RuntimeError("no variables specified for plotting variable distributions")
        if categories is None:
            categories = self.target_channels
        if isinstance(categories, str):
            categories = [categories]
        categories_by_channel = {}
        for category in categories:
            # plot channel-level distribution
            if category in self.target_channels:
                channel = category
            else:
                channel = self.categorizer.get_category_channel(category)
            if channel not in categories_by_channel:
                categories_by_channel[channel] = []
            categories_by_channel[channel].append(category)
        samples = []
        for key, options in plot_options.items():
            samples += options["samples"]
        if len(samples) == 0:
            raise RuntimeError("no samples specified for plotting variable distributions")
        from quickstats.plots import VariableDistributionPlot
        import matplotlib.pyplot as plt
        for channel in categories_by_channel:
            channel_categories = categories_by_channel[channel]
            self.categorizer.set_active_channel(channel)
            # only plot channel-level distribution, no need to apply categories
            if (len(channel_categories) == 1) and (channel_categories[0] == channel):
                self.categorizer.load_channel_df(samples=required_samples,
                                                 apply_score=False)
            else:
                # load boundaries from cache if not set
                if (channel not in self.categorizer.boundary_trees):
                    self.categorizer.load_boundaries()
                self.categorizer.load_channel_df(samples=required_samples,
                                                 apply_score=not cache_scores)
                if not cache_categories:
                    self.categorizer.apply_categories()
            for category in channel_categories:
                category_dfs = {}
                for sample in samples:
                    category_dfs[sample] = self.categorizer.get_category_df(sample, category, selection=selection)
                _general_options = combine_dict(general_options)
                if ("analysis_label_options" in _general_options) and \
                   ("extra_text" in _general_options["analysis_label_options"]):
                    pretty_category_name = self.get_pretty_category_name(category)
                    _general_options["analysis_label_options"]["extra_text"] += f"// {pretty_category_name}"
                plotter = VariableDistributionPlot(category_dfs,
                                                   plot_options=plot_options,
                                                   **_general_options)
                for variable in variables:
                    if variable in draw_options:
                        plotter.draw(**draw_options[variable], weight_name=weight_name)
                    else:
                        plotter.draw(column_name=variable, xlabel=variable, weight_name=weight_name)
                    savepath = self.get_file("variable_distribution_plot", variable=variable, category=category)
                    plt.savefig(savepath, bbox_inches="tight")
                    plt.show()
                    
    def plot_vbf_validation(self):
        filename = self.get_file("VBF_validation_data")
        if not os.path.exists(filename):
            raise FileNotFoundError(f"missing VBF validation data file {filename}")
        with open(filename, "r") as file:
            data = json.load(file)
        bases = list(self.config['VBF_param']['reference_points'].values())
        
        from bbyy_analysis_framework.plots import VBFValidationPlot
        
        self.path_manager.makedirs(["plot"])
        for category in data:
            df = pd.DataFrame(data[category])
            general_options, draw_options, _, _ = self._get_plot_options("vbf_validation")
            extra_text = general_options["analysis_label_options"]["extra_text"].replace("<category>", category)
            general_options["analysis_label_options"]["extra_text"] = extra_text
            plotter = VBFValidationPlot(df, bases=bases, **general_options)
            ax = plotter.draw(**draw_options)
            savepath = self.get_file("VBF_validation_plot", category=category)
            plt.savefig(savepath, bbox_inches="tight")
            self.stdout.info(f"INFO: Saved VBF validation plot for the category {category} to {savepath}")
            plt.show()
            
    def plot_ggf_validation(self, custom_configs:Optional[Dict]=None):
        # load yield information
        yield_path = self.get_file("merged_yield_data", validate=True)
        yield_data = json.load(open(yield_path))
        yield_err_path = self.get_file("merged_yield_err_data", validate=True)
        yield_err_data = json.load(open(yield_err_path))
        reweight_yield_path = self.get_file("merged_yield_data_ggF_param", validate=True)
        reweight_yield_data = json.load(open(reweight_yield_path))
        reweight_yield_err_path = self.get_file("merged_yield_err_data_ggF_param", validate=True)
        reweight_yield_err_data = json.load(open(reweight_yield_err_path))
        param_path = self.get_file("ggF_param", validate=True)
        param_data = json.load(open(param_path))
        
        if "validation" in self.config["ggF_param"]:
            ref_samples = {}
            for sample, params in self.config["ggF_param"]["validation"].items():
                param_value = params["klambda"]
                ref_samples[param_value] = sample
        else:
            ref_samples = None
            
        from bbyy_analysis_framework.plots import ggFValidationPlot
        
        general_options, draw_options, plot_options, label_map = self._get_plot_options("ggf_validation",
                                                                                        custom_configs)
        plotter = ggFValidationPlot(param_data, reweight_yield_data,
                                    reweight_yield_err_data, yield_data,
                                    yield_err_data, ref_samples,
                                    label_map=label_map,
                                    plot_options=plot_options,
                                    **general_options)
        plotter.set_error_options("reweight", "fill", 
                                  dict(color="hh:darkpink", alpha=0.2))
        plotter.set_error_options("reco", "errorbar")
        categories = list(param_data)
        for category in categories:
            extra_text = plotter.analysis_label_options.get("extra_text", "")
            plotter.analysis_label_options["extra_text"] = extra_text.replace("<category>", category)
            ax, ax_ratio = plotter.draw(category=category, **draw_options)
            plotter.analysis_label_options["extra_text"] = extra_text
            savepath = self.get_file("ggF_validation_plot", category=category)
            plt.savefig(savepath, bbox_inches="tight")
            self.stdout.info(f"INFO: Saved ggF validation plot for the category {category} to {savepath}")            
            plt.show()
        
    def plot_discriminant_fit(self, custom_configs:Optional[Dict]=None,
                              mH_value:Optional[float]=None):
        general_options, draw_options, _, label_map = self._get_plot_options("discriminant_fit",
                                                                             custom_configs)
        if mH_value is None:
            mH_str = None
            filename = self.get_file("workspace_non_param", validate=True)
        else:
            mH_str = str_encode_value(mH_value)
            filename = self.get_file("workspace_non_param_mH", mH_value=mH_str, validate=True)
        
        if self.do_blind:
            blind_range = self.config["observable"]["blind_range"]
        else:
            blind_range = None
        from bbyy_analysis_framework.plots import DiscriminantFitPlot
        plotter = DiscriminantFitPlot(filename, dataset_name="combData",
                                      blind_range=blind_range,
                                      label_map=label_map,
                                      **general_options)
        savepath = self.get_file("total_bkg_discriminant_fit_plot")
        plotter.draw(snapshots=["total_bkg_snap"],
                     save_as=savepath, **draw_options)
        savepath = self.get_file("total_bkg_cont_bkg_discriminant_fit_plot")
        plotter.draw(snapshots=["total_bkg_snap", "bkg_only_snap"],
                     save_as=savepath, **draw_options)