from tqdm import tqdm

from hpogrid.interface.panda import PandaTaskManager

class MxAODTaskManager(PandaTaskManager):
    def get_mxaod_task_data(self, jeditaskid:int, jobstatus:str=None):
        print(f'INFO: Querying jobs metadata for the taskid: {jeditaskid}.')
        response = self.query_tasks(jeditaskid=jeditaskid, days=999, username='auto', silent=True)
        if not response:
            raise RuntimeError("query failed")
        taskname = response[0]['taskname']
        username = response[0]['username']
        status = response[0]['status']
        task_data = {'jeditaskid': jeditaskid, 'taskname': taskname,
                     'username': username, 'status': status, 'subtasks':[]}
        print('Taskname: '.rjust(10) + f'{taskname}')
        print('Username: '.rjust(10) + f'{username}')
        print('Status: '.rjust(10) + f'{status}')
        response = self.query_jobs(jeditaskid=jeditaskid, jobstatus=jobstatus)
        jobs_data = response['jobs']
        for job_data in jobs_data:
            pandaid = job_data['pandaid']
            jobstatus = job_data['jobstatus']
            transformation = job_data['transformation'].split("-")[0]
            if transformation not in ['runGen', 'runMerge']:
                continue
            subtask_data = {
                'pandaid': pandaid,
                'jobstatus': jobstatus,
                'transformation': transformation
            }
            task_data['subtasks'].append(subtask_data)
        self.fill_missing_subtask_data(task_data)
        unfinished_queries = len([1 for subtask in task_data['subtasks'] if 'files' not in subtask])
        print(f'INFO: There are {unfinished_queries} unfinished queries.')
        return task_data
    
    def fill_missing_subtask_data(self, task_data):
        pbar = tqdm(task_data['subtasks'])
        for subtask_data in pbar:
            pandaid = subtask_data['pandaid']
            transformation = subtask_data['transformation']
            pbar.set_description(f"Processing {transformation} task with pandaid %s" % pandaid)
            if 'files' in subtask_data:
                continue
            response = self.query_by_pandaid(pandaid)
            if response is None:
                continue
            files = response['files']
            subtask_data['files'] = {
                'input': [file['lfn'] for file in files if file['type'] == 'input'],
                'output': [file['lfn'] for file in files if file['type'] == 'output']}