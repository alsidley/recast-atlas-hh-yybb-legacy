import os
import json
import click

class DelimitedStr(click.Option):
    def type_cast_value(self, ctx, value):
        try:
            return [i.strip() for i in value.split(",")]
        except:
            raise click.BadParameter(value)

@click.command(name='create_ntuples')
@click.option('--sample_config', required=True, 
              help='Path to the sample configuration file.')
@click.option('--processor_config', required=True, 
              help='Path to the processor configuration file.')
@click.option('-s', '--samples', default=None, show_default=True,
              help='MxAOD samples to process (separated by commas).')
@click.option('-t', '--sample_types', default=None, show_default=True,
              help='Types of MxAOD samples to process (separated by commas).')
@click.option('-m', '--merge', default=None, show_default=True,
              help='Ntuple (and cutflow) samples to merge (separated by commas).')
@click.option('--do-process/--skip-process', default=True, show_default=True,
              help='Perform the MxAOD processing step.')
@click.option('--do-merge/--skip-merge', default=True, show_default=True,
              help='Perform the ntuple merging step.')
@click.option('--do-cutflow/--skip-cutflow', default=True, show_default=True,
              help='Perform the cutflow merging step.')
@click.option('--multithread/--no-multithread', default=True, show_default=True,
              help='Use multithreading.')
@click.option('-f', '--processor_flags', default=None, show_default=True,
              help='Processor flags to set (separated by commas).')
@click.option('-o', '--outdir', default='outputs', show_default=True,
              help='Output directory.')
@click.option('-v', '--verbosity', default='INFO', show_default=True,
              type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"], case_sensitive=False),
              help='Verbosity level.')
def create_ntuples(**kwargs):
    """
    Create skimmed ntuples from MxAODs 
    """
    from bbyy_analysis_framework.components import MxAODProcessTool
    from quickstats.utils.string_utils import split_str
    for key in ['samples', 'sample_types', 'merge']:
        if kwargs[key] is not None:
            kwargs[key] = split_str(kwargs[key], sep=',')
    init_kwargs = {}
    for key in ['sample_config', 'processor_config',
                'multithread', 'outdir', 'processor_flags',
                'verbosity']:
        init_kwargs[key] = kwargs.pop(key)
    if init_kwargs['processor_flags'] is not None:
        init_kwargs['processor_flags'] = init_kwargs['processor_flags'].split(",")
    processor = MxAODProcessTool(**init_kwargs)
    if kwargs['do_process']:
        processor.process_samples(samples=kwargs['samples'], sample_types=kwargs['sample_types'])
    if kwargs['do_merge']:
        processor.merge_samples(samples=kwargs['merge'])
    if kwargs['do_cutflow']:
        processor.merge_cutflows(samples=kwargs['merge'])
        
@click.command(name='create_reweight_samples')
@click.option('-i', '--reweight_config', required=True, 
              help='Path to the reweight configuration file.')
@click.option('-t', '--treename', default='output', show_default=True,
              help='Tree name of ntuples.')
@click.option('-b', '--base_sample', required=True,
              help='Sample used as a basis for reweighting.')
@click.option('-r', '--reference_sample', required=True,
              help='Sample with weight information appended from the base sample.')
@click.option('-o', '--ntuple_dir', default='outputs/ntuples', show_default=True,
              help='Path the the output (ntuple) directory.')
@click.option('-v', '--verbosity', default='INFO', show_default=True,
              type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"], case_sensitive=False),
              help='Verbosity level.')
def create_reweight_samples(**kwargs):
    """Create ntuple with extended kl weight branches
    """

    from bbyy_analysis_framework.components import KLambdaReweightingTool
    tool = KLambdaReweightingTool(kwargs['reweight_config'], verbosity=kwargs['verbosity'])
    base_fname = os.path.join(kwargs['ntuple_dir'], f"{kwargs['base_sample']}.root")
    reference_sample = os.path.join(kwargs['ntuple_dir'], f"{kwargs['reference_sample']}.root")
    tool.run(base_fname, reference_sample, treename=kwargs['treename'])
        
@click.command(name='convert_ntuples')
@click.option('-i', '--config', required=True, 
              help='Path to the analysis configuration file.')
@click.option('-c', '--columns', default='h027-legacy', show_default=True,
              type=click.Choice(["h027", "h027-legacy"]),
              help='Set of columns to save.')
@click.option('-s', '--samples', default=None, show_default=True,
              help='Ntuple samples to convert (separated by commas).')
@click.option('--do-conversion/--skip-conversion', default=True, show_default=True,
              help='Perform the ntuple conversion.')
@click.option('--do-reweight/--skip-reweight', default=True, show_default=True,
              help='Create reweighted sample in the converted file format.')
@click.option('--reweight_values', default=None, show_default=True,
              help='Values of klambda for which the reweighted samples should be produced (separated by commas). '
                   'The main base sample defined in the config will be used.')
@click.option('--alt_reweight_values', default=None, show_default=True,
              help='Alaternative values of klambda for which the reweighted samples should be produced (separated by commas). '
                   'The alternative base sample defined in the config will be used.')
@click.option('--ntuple_dir', default=None,
              help='Path the the output (ntuple) directory. Use path from config by default.')
@click.option('--array_dir', default=None,
              help='Path the the output (array) directory. Use path from config by default.')
@click.option('-v', '--verbosity', default='INFO', show_default=True,
              type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"], case_sensitive=False),
              help='Verbosity level.')
def convert_ntuples(**kwargs):
    """
    Convert skimmed ntuples to other data formats
    """
    from bbyy_analysis_framework.components import NTupleConversionTool
    tool = NTupleConversionTool(kwargs['config'], verbosity=kwargs['verbosity'])
    column_map = {
        'h027': NTupleConversionTool.DEFAULT_COLUMNS,
        'h027-legacy': NTupleConversionTool.DEFAULT_COLUMNS_LEGACY
    }
    columns = column_map[kwargs['columns']]
    from quickstats.utils.string_utils import split_str
    if kwargs['samples'] is not None:
        kwargs['samples'] = split_str(kwargs['samples'], sep=',')
    if kwargs['do_conversion']:
        tool.convert_samples(samples=kwargs['samples'], columns=columns,
                             save_train_val_test=False)
    if kwargs['do_reweight']:
        for argname, key in [['reweight_values', 'kl_reweighting'],
                           ['alt_reweight_values', 'kl_reweighting_alt']]:
            if kwargs[argname] is None:
                continue
            reweight_options = tool.config[key]
            reweight_values = [round(float(value), 8) for value in kwargs[argname].split(",")]
            tool.create_ggF_reweighting_reference_sample(reweight_options=reweight_options)
            for value in reweight_values:
                tool.create_ggF_reweighted_sample(klambda=value, reweight_options=reweight_options)
                
@click.command(name='stat_eval')
@click.option('-i', '--config', 'analysis_config', required=True, 
              help='Path to the analysis configuration file.')
@click.option('-n', '--study_name', default='h027_legacy', show_default=True,
              help='Name of the study.')
@click.option('-c', '--channels', 'target_channels', default='SM,BSM', show_default=True,
              help='Analysis channels to include (separated by commas).')
@click.option('--output_mode', default='standard', show_default=True,
              type=click.Choice(["minimal", "minitree", "array", "standard", "full"]),
              help='Categorization output mode.')
@click.option('--do-categorization/--skip-categorization', default=True, show_default=True,
              help='Whether to perform the categorization step. This includes evaluation of '
              'event scores, score boundary scans, application of event categories and '
              'creation of categorized outputs (yields table, minitrees and csv files).')
@click.option('--do-signal-modelling/--skip-signal-modelling', default=True, show_default=True,
              help='Whether to perform the singal modelling step.')
@click.option('--do-signal-param/--skip-signal-param', default=True, show_default=True,
              help='Whether to perform the ggF and VBF signal parameterization step.')
@click.option('--do-workspace-creation/--skip-workspace-creation', default=True, show_default=True,
              help='Whether to perform the workspace (and xml) creation step.')
@click.option('--do-limit-setting/--skip-limit-setting', default=True, show_default=True,
              help='Whether to evaluate limits.')
@click.option('--do-likelihood-scan/--skip-likelihood-scan', default=True, show_default=True,
              help='Whether to run likelihood scans.')
@click.option('--plot-limit-scan/--skip-plot-limit-scan', default=True, show_default=True,
              help='Whether to produce plots for limit scans.')
@click.option('--plot-likelihood-scan/--skip-plot-likelihood-scan', default=True, show_default=True,
              help='Whether to produce plots for likelihood scans.')
@click.option('--plot-discriminant-fit/--skip-plot-discriminant-fit', default=True, show_default=True,
              help='Whether to produce plots for fits on the discriminant variable.')
@click.option('--plot-score-distribution/--skip-plot-score-distribution', default=True, show_default=True,
              help='Whether to produce plots on the score distribution in each analysis channel.')
@click.option('--limit_tasks', default='sm_limit, sm_vbf_limit, kl_scan, k2v_scan, k2v_vbf_scan', show_default=True,
              help='Limit setting tasks to run (separated by commas). Allowed tasks: '
              'sm_limit, sm_vbf_limit, kl_scan, k2v_scan, k2v_vbf_scan')
@click.option('--likelihood_tasks', default='kl_scan, k2v_scan', show_default=True,
              help='Likelihood scan tasks to run (separated by commas). Allowed tasks: '
              'kl_scan, k2v_scan, kl_k2v_scan')
@click.option('--higgs_mass', default='125,125.09', show_default=True,
              help='Assumptions of Higgs mass value to use (separated by commas) in limit setting/'
              'likelihood scans. If more than one is given, they will be evaluated separately.')
@click.option('--boundary_dir', default=None, show_default=True,
              help='Path to the directory containing the boundary information. '
              'To be used together with cache-boundary. If not given, the path '
              'will be inferred from the analysis config.')
@click.option('--cache/--no-cache', default=False, show_default=True,
              help='Whether to cache limit and likelihood outputs.')
@click.option('--cache-score/--no-cache-score', default=False, show_default=True,
              help='Whether to skip evaluation of event scores'
              '(use if event scores are already available in the input).')
@click.option('--cache-boundary/--no-cache-boundary', default=False, show_default=True,
              help='Whether to cache score boundaries (skip boundary scan).')
@click.option('--cache-category/--no-cache-category', default=False, show_default=True,
              help='Whether to skip evaluation of event category based on existing event scores '
              '(use if event category is already available in the input).')
@click.option('--extra_variables', default=None, show_default=True,
              help='Additional variables (separated by commas) to be saved in the categorized minitrees.')
@click.option('--array_dir', default=None,
              help='Path to the array directory. Use path from config by default.')
@click.option('--model_dir', default=None,
              help='Path to the MVA model directory. Use path from config by default.')
@click.option('-o', '--outdir', default=None,
              help='Path to the analysis output directory. Use path from config by default.')
@click.option('-v', '--verbosity', default='INFO', show_default=True,
              type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"], case_sensitive=False),
              help='Verbosity level.')
def stat_eval(**kwargs):
    """
    Evaluate statistical results
    """
    import quickstats
    quickstats.load_extensions()
    from bbyy_analysis_framework.components import AnalysisCore
    from quickstats.utils.string_utils import split_str
    from quickstats.plots import AbstractPlot
    for key in ['target_channels', 'higgs_mass', 'extra_variables', 'limit_tasks', 'likelihood_tasks']:
        if kwargs[key] is not None:
            kwargs[key] = split_str(kwargs[key], sep=',')
    mH_values = []
    for mH in kwargs['higgs_mass']:
        if mH not in ['125', '125.09']:
            raise ValueError(f'unsupported Higgs mass assumption: {mH} (allowed values: 125, 125.09)')
        mH_values.append(mH)
    limit_task_map = {
        'sm_limit'     : 'SM_limit',
        'sm_vbf_limit' : 'SM_VBF_limit',
        'kl_scan'      : 'kl_scan',
        'k2v_scan'     : 'k2v_scan',
        'k2v_vbf_scan' : 'k2v_vbf_only_scan'
    }
    limit_tasks = {v:False for v in limit_task_map.values()}
    for task in kwargs['limit_tasks']:
        if task not in limit_task_map:
            raise ValueError(f'unsupported limit task: {task} (allowed tasks: {", ".join(limit_task_map.keys())})')
        limit_tasks[limit_task_map[task]] = True
    likelihood_task_map = {
        'kl_scan'     : 'kl_scan',
        'k2v_scan'    : 'k2v_scan',
        'kl_k2v_scan' : 'kl_k2v_scan',
    }
    likelihood_tasks = {v:False for v in likelihood_task_map.values()}
    for task in kwargs['likelihood_tasks']:
        if task not in likelihood_task_map:
            raise ValueError(f'unsupported limit task: {task} (allowed tasks: {", ".join(likelihood_task_map.keys())})')
        likelihood_tasks[likelihood_task_map[task]] = True        
    init_kwargs = {}
    for key in ['study_name', 'target_channels', 'analysis_config',
                'array_dir', 'model_dir', 'outdir', 'cache',
                'output_mode', 'verbosity']:
        init_kwargs[key] = kwargs.pop(key)
    analysis = AnalysisCore(**init_kwargs)
    if kwargs['cache_boundary']:
        analysis.load_boundaries(boundary_dir=kwargs['boundary_dir'])
    if kwargs['extra_variables'] is not None:
        analysis.categorizer.config["categorization"]["save_variables"] += kwargs['extra_variables']
    if (not kwargs['cache_score']) and kwargs['do_categorization']:
        import xgboost as xgb
        models = {}
        for channel in channels:
            models[channel] = xgb.Booster()
            model_path = analysis.get_file("basic_model", channel=channel)
            models[channel].load_model(model_path)
        analysis.set_models(models)
    if kwargs['do_categorization']:
        analysis.run_categorization(cache_boundaries=kwargs['cache_boundary'],
                                    cache_categories=kwargs['cache_category'],
                                    cache_scores=kwargs['cache_score'])
    if kwargs['do_signal_modelling']:
        analysis.run_signal_modelling(plot=True, parallel=0)
        AbstractPlot.close_all_figures()
    if kwargs['do_signal_param']:
        analysis.run_parameterisation()
    if kwargs['do_workspace_creation']:
        analysis.create_workspace_xmls()
        analysis.create_workspace()
        if '125' in mH_values:
            import yaml
            framework_dir = os.environ["BBYYFRAMEWORKDIR"]
            modification_config_path = os.path.join(framework_dir, "configs", "modification",
                                                    "modification_mH_125p09GeV_to_125GeV.yaml")
            modification_config = yaml.safe_load(open(modification_config_path))
            analysis.create_mH_modified_workspace(mH_value=125,
                                                  modification_config=modification_config)
    for mH_value in mH_values:
        if mH_value == '125.09':
            mH_value = None
        else:
            mH_value = float(mH_value)
        if kwargs['do_limit_setting']:
            analysis.run_limits(**limit_tasks, mH_value=mH_value)
            analysis.gather_limits(**limit_tasks, mH_value=mH_value)
            if kwargs['plot_limit_scan']:
                scan_limit_tasks = {k:v for k,v in limit_tasks.items() if k not in ['SM_limit', 'SM_VBF_limit']}
                analysis.plot_limit_scan(**scan_limit_tasks, mH_value=mH_value)
        if kwargs['do_likelihood_scan']:
            analysis.create_asimov_workspace(mH_value=mH_value)
            analysis.run_likelihood_scan(**likelihood_tasks, mH_value=mH_value)
            gather_likelihood_tasks = {k:v for k,v in likelihood_tasks.items() if k not in ['kl_k2v_scan']}
            analysis.gather_likelihoods(**gather_likelihood_tasks, mH_value=mH_value)
            if kwargs['plot_likelihood_scan']:
                analysis.plot_likelihood_scan(**likelihood_tasks, mH_value=mH_value)
        if kwargs['plot_discriminant_fit']:
            analysis.plot_discriminant_fit(mH_value=mH_value)
        AbstractPlot.close_all_figures()
    if kwargs['plot_score_distribution']:
        analysis.plot_score_distribution_binary()
        AbstractPlot.close_all_figures()