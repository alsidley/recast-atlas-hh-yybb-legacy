from typing import Tuple, List, Optional, Union
import os
import glob

import pandas as pd
import numpy as np

DEFAULT_INDEX_VARIABLES = ("event_number", "run_number", "mc_channel_number")

def check_sample(df1:pd.DataFrame, df2:pd.DataFrame, sort_columns:Optional[Tuple]=DEFAULT_INDEX_VARIABLES, rtol:float=1e-5):
    if sort_columns is not None:
        sort_columns = list(sort_columns)
        df1 = df1.sort_values(sort_columns)
        df2 = df2.sort_values(sort_columns)
    column_1 = list(df1.columns)
    column_2 = list(df2.columns)
    common_columns = np.intersect1d(column_1, column_2)
    diff = []
    for column in common_columns:
        if df1[column].dtype == np.dtype(object):
            allclose = all(np.allclose(v1, v2) for v1, v2 in zip(df1[column].values, df2[column].values))
        else:
            allclose = np.allclose(df1[column].values, df2[column].values, rtol=rtol)
        if not allclose:
            diff.append(column)
    return diff

def check_sample_dir(dir1:str, dir2:str, sort_columns:Optional[Tuple]=DEFAULT_INDEX_VARIABLES, rtol:float=1e-5):
    files1 = [os.path.basename(f) for f in glob.glob(os.path.join(dir1, "*.csv"))]
    files2 = [os.path.basename(f) for f in glob.glob(os.path.join(dir2, "*.csv"))]
    missing_from_1 = list(set(files2) - set(files1))
    missing_from_2 = list(set(files1) - set(files2))
    if len(missing_from_1) > 0:
        print("WARNING: The followings samples are missing from the first directory:", ", ".join(missing_from_1))
    if len(missing_from_2) > 0:
        print("WARNING: The followings samples are missing from the second directory:", ", ".join(missing_from_2))
    common_files = sorted(list(set(files1).intersection(set(files2))))
    for file in common_files:
        path1 = os.path.join(dir1, file)
        path2 = os.path.join(dir2, file)
        df1 = pd.read_csv(path1)
        df2 = pd.read_csv(path2)
        if len(df1) != len(df2):
            print(f"Sample {file}: inconsistent number of events ({len(df1)} vs {len(df2)})")
            continue
        diff = check_sample(df1, df2, sort_columns=sort_columns, rtol=rtol)
        if not diff:
            print(f"Sample {file}: PASS")
        else:
            print(f"Sample {file}: FAIL")
            print(diff)
    
def get_reordered_sample(ref_df, target_df, index_columns:Union[Tuple, List]=DEFAULT_INDEX_VARIABLES, round_by:int=0):
    index_columns = list(index_columns)
    new_index_column = [f"_{column}" for column in index_columns]
    df1 = ref_df.copy(deep=True)
    df2 = target_df.copy(deep=True)
    df1[new_index_column] = df1[index_columns].round(round_by)
    df2[new_index_column] = df2[index_columns].round(round_by)
    df1 = df1.set_index(new_index_column)
    df2 = df2.set_index(new_index_column)
    df = df2.loc[df1.index].reset_index(drop=True)
    return df

def check_ntuple_events(dirname_left:str, dirname_right:str, samples:List[str],
                        columns=("eventNumber", "runNumber")):
    import pandas as pd
    from quickstats.utils.data_conversion import root2dataframe
    columns = list(columns)
    treename = "output"
    event_data = {}
    summary_data = {}
    for sample in samples:
        event_data[sample] = {}
        print(f'INFO: Processing sample "{sample}"')
        filename_left = os.path.join(dirname_left, f"{sample}.root")
        filename_right = os.path.join(dirname_right, f"{sample}.root")
        if not os.path.exists(filename_left):
            print(f'WARNING: File "{filename_left}" not found. Skipping the sample.')
            continue
        if not os.path.exists(filename_right):
            print(f'WARNING: File "{filename_right}" not found. Skipping the sample.')
            continue
        df_left = root2dataframe(filename_left, treename, columns=columns).set_index(columns)
        df_right = root2dataframe(filename_right, treename, columns=columns).set_index(columns)
        common_events = df_left.index.intersection(df_right.index)
        left_only_events = df_left.index.difference(df_right.index)
        right_only_events = df_right.index.difference(df_left.index)
        event_data[sample]['common'] = common_events
        event_data[sample]['unique_left'] = left_only_events
        event_data[sample]['unique_right'] = right_only_events
        summary_data[sample] = {
            "common": len(common_events),
            "unique_left": len(left_only_events),
            "unique_right": len(right_only_events),
        }
    summary_df = pd.DataFrame(summary_data).transpose()
    return event_data, summary_df