from typing import Optional, List, Dict, Union
import re
import json

import pandas as pd

from quickstats.utils.xml_tools import TXMLTree
from quickstats.utils.roofit_utils import recover_formula

# a map from the STXS bin name to the corresponding bin indices
bin_index = {
    "GG2H_PTH_200_300_YY": 101,
    "GG2H_PTH_300_450_YY": 102,
    "GG2H_PTH_GT450_YY": [103, 104],
    "GG2H_0J_PTH_0_10_YY": 105,
    "GG2H_0J_PTH_GT10_YY": 106,
    "GG2H_1J_PTH_0_60_YY": 107,
    "GG2H_1J_PTH_60_120_YY": 108,
    "GG2H_1J_PTH_120_200_YY": 109,
    "GG2H_GE2J_MJJ_0_350_PTH_0_60_YY": 110,
    "GG2H_GE2J_MJJ_0_350_PTH_60_120_YY": 111,
    "GG2H_GE2J_MJJ_0_350_PTH_120_200_YY": 112,
    "GG2H_GE2J_MJJ_350_700_PTH_0_200_YY": [120, 121],
    "GG2H_GE2J_MJJ_700_1000_PTH_0_200_YY": [122, 123],
    "GG2H_GE2J_MJJ_GT1000_PTH_0_200_YY": [124, 125, 126, 127],
    "QQ2HQQ_LE1J_YY": [201, 202],
    "QQ2HQQ_GE2J_MJJ_0_60_OR_120_350_YY": [203, 205],
    "QQ2HQQ_GE2J_MJJ_60_120_YY": 204,
    "QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200_YY": [217, 218],
    "QQ2HQQ_GE2J_MJJ_700_1000_PTH_GT200_YY": [219, 220],
    "QQ2HQQ_GE2J_MJJ_GT1000_PTH_GT200_YY": [221, 222, 223, 224],
    "QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_YY": [209, 210],
    "QQ2HQQ_GE2J_MJJ_700_1000_PTH_0_200_YY": [211, 212],
    "QQ2HQQ_GE2J_MJJ_GT1000_PTH_0_200_YY": [213, 214, 215, 216],
    "QQ2HLNU_PTV_0_150_YY": [301, 302 ,306, 307, 311, 312],
    "QQ2HLNU_PTV_GT150_YY": [303,304,305, 308, 309, 310, 313, 314, 315],
    "ZHLL_PTV_0_150_YY": [401, 402, 406, 407, 411, 412, 501, 502, 506, 507, 511, 512],
    "ZHLL_PTV_GT150_YY": [403, 404, 405, 408, 409, 410, 413, 414, 415, 503, 504, 505, 508, 509, 510, 513, 514, 515],
    "TTH_PTH_0_60_YY": 601 ,
    "TTH_PTH_60_120_YY": 602,
    "TTH_PTH_120_200_YY": 603,
    "TTH_PTH_200_300_YY": 604,
    "TTH_PTH_GT300_YY": [605, 606],
    "TH_YY": 801,
    "BR_YY": -1,
    "width_YY": -2,
    "width_tot": -3
}

bin_branches = {
    "HTXS_Stage1_2_Category_pTjet30": [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112,
                                       201, 202, 203, 204, 205, 601, 602, 603, 604, 701, 801],
    "HTXS_Stage1_2_Fine_Category_pTjet30": [120, 121, 122, 123, 124, 125, 126, 127, 209, 210, 211,
                                            212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222,
                                            223, 224, 301, 302, 303, 304, 305, 306, 307, 308, 309,
                                            310, 311, 312, 313, 314, 315, 401, 402, 403, 404, 405,
                                            406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 501,
                                            502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512,
                                            513, 514, 515, 605, 606],
    "BR_Width": [-1, -2, -3]
}

# change to another naming convention
name_map = {
    'cHG'   : 'cHG',
    'cHbox' : 'cHbox',
    'cuG'   : 'ctG',
    'cuHRe' : 'ctH'
}
polynomial_terms = {
     'A1': (''),
     'A2': ('cHbox', 'cHbox'),
     'A3': ('cHbox',),
     'A4': ('cH', 'cHbox'),
     'A5': ('cH',),
     'A6': ('cH', 'cH'),
     'A7': ('ctH', 'ctH'),
     'A8': ('ctH',),
     'A9': ('ctG', 'ctH'),
     'A10': ('ctG',),
     'A11': ('ctG', 'ctG'),
     'A12': ('cHbox', 'ctG'),
     'A13': ('cH', 'ctG'),
     'A14': ('cHbox', 'ctH'),
     'A15': ('cH', 'ctH'),
     'A16': ('cHG',),
     'A17': ('cHG', 'cHG'),
     'A18': ('cHG', 'cHbox'),
     'A19': ('cH', 'cHG'),
     'A20': ('cHG', 'ctH'),
     'A21': ('cHG', 'ctG')
}

def translate_STXS_formulas(filename:str, outdir:Optional[str]=None):
    xml_data  = TXMLTree(file=filename)
    # convert to dictionary for easier manipulation
    dict_data = xml_data.to_dict()
    all_formulas = [i['attrib']['Name'] for i in dict_data['children']]
    eft_formulas = [i for i in all_formulas if ('expr::eft_int' in i) or ('expr::eft_bsm' in i)]
    formula_regex = re.compile(r"expr::(?P<name>.+)\(.+\)")
    int_formula_by_stxs_bin = {}
    bsm_formula_by_stxs_bin = {}
    for formula in eft_formulas:
        match = formula_regex.match(formula)
        if not match:
            raise RuntimeError(f"invalid formula: {formula}")
        # convert formula to its natural expression, i.e. without @i
        recovered_formula = recover_formula(formula)
        name = match.groupdict()['name']
        if 'eft_int' in name:
            stxs_bin = name.replace("eft_int_", "")
            int_formula_by_stxs_bin[stxs_bin] = recovered_formula
        elif 'eft_bsm' in name:
            stxs_bin = name.replace("eft_bsm_", "")
            bsm_formula_by_stxs_bin[stxs_bin] = recovered_formula
    relevant_variables = ["cHG", "cHbox", "cuG", "cuHRe"]
    component_regex = re.compile(r"(?P<coefficient>[-]?\d*[.]?\d*)\*(?P<variables>[*\w]*)")
    def fill_coefficients(formula_map, degree):
        result = {}
        for bin_name, formula in formula_map.items():
            formula = re.sub(r"\s+", "", formula)
            for match in component_regex.finditer(formula):
                components = match.groupdict()
                coefficient = components['coefficient']
                variables = components['variables'].split("*")
                if len(variables) != degree:
                    raise RuntimeError("polynomial has incorrect degree")
                if any(variable not in relevant_variables for variable in variables):
                    continue
                key = tuple(sorted([name_map[v] for v in variables]))
                if bin_name not in result:
                    result[bin_name] = {}
                result[bin_name][key] = coefficient
        return result
    def refactor_formula(formula_coeff):
        result = {}
        for bin_name, coeff_map in formula_coeff.items():
            formula = " + ".join([f"{v} * {' * '.join(k)}" for k, v in coeff_map.items()])
            formula = formula.replace("+ -", "- ")
            result[bin_name] = formula
        return result
    formula_coefficients = {
        "int": fill_coefficients(int_formula_by_stxs_bin, 1),
        "bsm": fill_coefficients(bsm_formula_by_stxs_bin, 2)
    }
    formula_refactored = {
        "int": refactor_formula(formula_coefficients['int']),
        "bsm": refactor_formula(formula_coefficients['bsm'])
    }
    def fill_weights(formula_coeff):
        result = {k:[] for k in polynomial_terms.keys()}
        result["bin_label"] = []
        result["bins"] = []
        result["branch"] = []
        for bin_name, coeff_map in formula_coeff.items():
            indices = bin_index[bin_name]
            if not isinstance(indices, list):
                indices = [indices]
            for index in indices:
                result["bins"].append(index)
                result["bin_label"].append(bin_name)
                for branch, index_list in bin_branches.items():
                    if index in index_list:
                        result['branch'].append(branch)
                        break
                else:
                    raise RuntimeError(f"no branch information for the STXS bin {bin_name}")
                for term, key in polynomial_terms.items():
                    if key not in coeff_map:
                        result[term].append(0)
                    else:
                        result[term].append(float(coeff_map[key])) 
        return result
    weights = {
        "int": fill_weights(formula_coefficients["int"]),
        "bsm": fill_weights(formula_coefficients["bsm"])
    }
    save_weights(weights, outdir)
    save_formula(formula_refactored, outdir)
    return formula_refactored, weights

def save_formula(formulas, outdir:str):
    if outdir is None:
        return None
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    term_name_map = {
        "int": "interference",
        "bsm": "bsm"
    }
    for term, formula in formulas.items():
        term = term_name_map[term]
        outname = os.path.join(outdir, f"SMEFT_Higgs_STXS_{term}_formula.json")
        with open(outname, "w") as outfile:
            json.dump(formula, outfile, indent=2)
            print(f'INFO: Saved formula  file to "{outname}"')
            
def save_weights(weights, outdir:str):
    if outdir is None:
        return None
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    category_name_map = {
        "HTXS_Stage1_2_Category_pTjet30": "coarse_category",
        "HTXS_Stage1_2_Fine_Category_pTjet30": "fine_category",
        "BR_Width": "BR_Width"
    }
    term_name_map = {
        "int": "interference",
        "bsm": "bsm"
    }
    for term, data in weights.items():
        term = term_name_map[term]
        df = pd.DataFrame(data)
        for branch, category in category_name_map.items():
            df_cat = df[df["branch"] == branch].drop(columns=["branch"])
            outname = os.path.join(outdir, f"SMEFT_Higgs_STXS_{category}_{term}_weights.json")
            result = {
                "weights": df_cat.to_dict("list"),
                "bin_range": [0, 1000],
                "bin_width": 1
            }
            with open(outname, "w") as outfile:
                json.dump(result, outfile)
                print(f'INFO: Saved weight file to "{outname}"')