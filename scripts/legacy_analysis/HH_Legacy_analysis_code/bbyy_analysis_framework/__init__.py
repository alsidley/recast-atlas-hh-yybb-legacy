from bbyy_analysis_framework._version import __version__

import os
import pathlib

module_path = pathlib.Path(__file__).parent.absolute()
macro_path = os.path.join(module_path, "macros")
resource_path = os.path.join(module_path, "resources")
data_path = os.path.join(module_path, "data")

from quickstats.utils.io import VerbosePrint

_PRINT_ = VerbosePrint("INFO")