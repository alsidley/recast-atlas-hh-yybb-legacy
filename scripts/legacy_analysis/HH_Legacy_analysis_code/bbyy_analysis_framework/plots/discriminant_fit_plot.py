from typing import Union, Optional, Dict, List, Tuple

class DiscriminantFitPlot:
    
    def __init__(self, filename:str, dataset_name:str="combData", blind_range:Optional[List]=None,
                 label_map:Optional[Dict]=None, styles:Optional[Dict]=None,
                 analysis_label_options:Optional[Dict]=None):
        self.initialize(filename, dataset_name=dataset_name, blind_range=blind_range)
        self.label_map = label_map
        self.styles = styles
        self.analysis_label_options  = analysis_label_options
    
    def initialize(self, filename:str, dataset_name:str="combData", blind_range:Optional[List]=None):
        from quickstats.components import AnalysisBase
        self.analysis = AnalysisBase(filename, dataset_name=dataset_name, poi_name="mu_XS_HH")
        self.analysis.setup_parameters(fix_param="mu_XS_HH=0,mu_XS_H=1")
        self.analysis.save_snapshot("total_bkg_snap")
        self.analysis.setup_parameters(fix_param="mu_XS_HH=0,mu_XS_H=0")
        self.analysis.save_snapshot("bkg_only_snap")
        if blind_range is not None:
            self.analysis.set_blind_range(blind_range)
        self.do_blind = blind_range is not None
            
    def draw(self, snapshots:List[str],
             n_bins:int=22, n_pdf_bins:int=110,
             discriminant:str="$m_{\gamma\gamma}$",
             unit:str="GeV", save_as:Optional[str]=None):
        dataset_name = self.analysis.model.data.GetName()
        self.analysis.model.plot_distributions(current_distributions=False, datasets=[dataset_name],
                                               snapshots=snapshots,
                                               n_bins=n_bins, n_pdf_bins=n_pdf_bins,
                                               blind=self.do_blind, discriminant=discriminant,
                                               unit=unit, save_as=save_as,
                                               label_map=self.label_map,
                                               styles=self.styles,
                                               analysis_label_options=self.analysis_label_options)