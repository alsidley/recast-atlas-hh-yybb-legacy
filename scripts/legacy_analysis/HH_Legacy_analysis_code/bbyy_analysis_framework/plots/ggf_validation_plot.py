from typing import Dict, Optional, List

import numpy as np
import pandas as pd

from quickstats.maths.numerics import str_decode_value
from quickstats.plots import AbstractPlot
from quickstats.plots.template import draw_analysis_label, draw_text
from quickstats.plots import PdfDistributionPlot

class ggFValidationPlot(PdfDistributionPlot):
    
    COEFFICIENT_LABELS = {
        0: 'p0',
        1: 'p1',
        2: 'p2'
    }
    
    def __init__(self, param_data:Dict,
                 reweight_yield_data:Dict,
                 reweight_yield_err_data:Optional[Dict]=None,
                 yield_data:Optional[Dict]=None,
                 yield_err_data:Optional[Dict]=None,
                 ref_samples:Optional[Dict]=None,
                 label_map:Optional[Dict]=None,
                 plot_options:Optional[Dict]=None,
                 styles:Optional[Dict]=None,
                 analysis_label_options:Optional[Dict]=None):
        
        self.initialize(param_data=param_data,
                        reweight_yield_data=reweight_yield_data,
                        reweight_yield_err_data=reweight_yield_err_data,
                        yield_data=yield_data, 
                        yield_err_data=yield_err_data,
                        ref_samples=ref_samples)
        
        super().__init__({}, label_map=label_map,
                         plot_options=plot_options,
                         styles=styles,
                         analysis_label_options=analysis_label_options)
        
    def initialize(self, param_data:Dict,
                   reweight_yield_data:Dict,
                   reweight_yield_err_data:Optional[Dict]=None,
                   yield_data:Optional[Dict]=None,
                   yield_err_data:Optional[Dict]=None,
                   ref_samples:Optional[Dict]=None):
        """
        
        Parameters:
            param_data: dictionary
                {category: {coefficient: value}}
            ref_samples: dictionary
                {param_value: sample_name}
            yield_data: dictionary
                {category: {sample: yield_value}}
        """
        categories = list(param_data)
        if ref_samples is None:
            ref_samples = {}
            
        yields_df = {}

        for category in categories:
            yields_df[category] = {}
            reweight_yields            = {}
            reco_yields                = {}
            parametric_yields_reweight = {}
            parametric_yields_reco     = {}
            
            # truth reweighting yields
            if category not in reweight_yield_data:
                raise RuntimeError(f'missing ggF reweight yield information for the category "{category}"')
            reweight_yields["x"] = np.array([str_decode_value(k) for k in reweight_yield_data[category].keys()])
            reweight_yields["y"] = np.array([v for v in reweight_yield_data[category].values()])
            sort_indices = np.argsort(reweight_yields["x"])
            reweight_yields["x"] = reweight_yields["x"][sort_indices]
            reweight_yields["y"] = reweight_yields["y"][sort_indices]
            if reweight_yield_err_data is None:
                reweight_yields["yerrlo"] = None
                reweight_yields["yerrhi"] = None
            else:
                if category not in reweight_yield_err_data:
                    raise RuntimeError(f'missing ggF reweight yield error information for the category "{category}"')
                if set(reweight_yield_data[category].keys()) != set(reweight_yield_err_data[category].keys()):
                    raise RuntimeError("inconsistent parameter values between ggF reweight yield and "
                                       "yield error information")
                x = np.array([str_decode_value(k) for k in reweight_yield_err_data[category].keys()])
                sort_indices = np.argsort(x)
                reweight_yields["yerrlo"] = np.array([d["errlo"] for d in reweight_yield_err_data[category].values()])
                reweight_yields["yerrhi"] = np.array([d["errhi"] for d in reweight_yield_err_data[category].values()])
                reweight_yields["yerrlo"] = reweight_yields["yerrlo"][sort_indices]
                reweight_yields["yerrhi"] = reweight_yields["yerrhi"][sort_indices]
            
            # reco-level yields           
            if ref_samples is not None:
                if yield_data is None:
                    raise RuntimeError(f'missing yield information')
                if category not in yield_data:
                    raise RuntimeError(f'missing yield information for the category "{category}"')                
                
                reco_yields["x"] = np.zeros(len(ref_samples))
                reco_yields["y"] = np.zeros(len(ref_samples))
                
                if yield_err_data is None:
                    reco_yields["yerrlo"] = None
                    reco_yields["yerrhi"] = None
                else:
                    reco_yields["yerrlo"] = np.zeros(len(ref_samples))
                    reco_yields["yerrhi"] = np.zeros(len(ref_samples))

                for i, (param_value, sample) in enumerate(ref_samples.items()):
                    reco_yields["x"][i] = param_value
                    if sample not in yield_data[category]:
                        raise RuntimeError(f'missing yield information for the sample "{sample}" in the '
                                           f'category "{category}"')
                    reco_yield = yield_data[category][sample]
                    reco_yields["y"][i] = reco_yield
                    if yield_err_data is not None:
                        if (category not in yield_err_data) or (sample not in yield_err_data[category]):
                            raise RuntimeError(f'missing yield error information for the sample "{sample}" in '
                                               f'the category "{category}"')
                        reco_yields["yerrlo"][i] = yield_err_data[category][sample]["errlo"]
                        reco_yields["yerrhi"][i] = yield_err_data[category][sample]["errhi"]
            
            # parametric yields
            parametric_yields_reweight["x"] = np.array(reweight_yields["x"])
            parametric_yields_reweight["y"] = self.get_parametric_yields(param_data[category],
                                                                         reweight_yields["x"])

            if reco_yields:
                parametric_yields_reco["x"] = np.array(reco_yields["x"])
                parametric_yields_reco["y"] = self.get_parametric_yields(param_data[category],
                                                                         reco_yields["x"])
            
            yields_df[category]["reweight"]       = pd.DataFrame(reweight_yields)
            yields_df[category]["reco"]           = pd.DataFrame(reco_yields)
            yields_df[category]["param_reweight"] = pd.DataFrame(parametric_yields_reweight)
            yields_df[category]["param_reco"]     = pd.DataFrame(parametric_yields_reco)
        self.param_data = param_data  
        self.yields_df = yields_df
        
    def get_parametric_yields(self, param_data:Dict, param_values:np.ndarray):
        p0 = param_data[self.COEFFICIENT_LABELS[0]]
        p1 = param_data[self.COEFFICIENT_LABELS[1]]
        p2 = param_data[self.COEFFICIENT_LABELS[2]]
        return p0 + p1 * param_values + p2 * (param_values ** 2)
    
    def draw(self, category:str, n_point:int=200,
             xlabel:str="", ylabel:str="",
             logx:bool=False, logy:bool=False,
             show_error:bool=True, 
             comparison_options:Optional[str]=None):
        param_min = self.yields_df[category]["reweight"]["x"].min()
        param_max = self.yields_df[category]["reweight"]["x"].max()
        x = np.arange(param_min, param_max, (param_max - param_min) / n_point)
        # use finer binnings for the parametric curve
        self.yields_df[category]["param"] = {
            "x": x,
            "y": self.get_parametric_yields(self.param_data[category], x)
        }
        self.set_data(self.yields_df[category])
        if len(self.collective_data["reco"]) > 0:
            targets = ["reweight", "reco", "param"]
        else:
            targets = ["reweight", "param"]
        return super().draw(xlabel=xlabel, ylabel=ylabel, targets=targets,
                            logx=logx, logy=logy, show_error=show_error,
                            comparison_options=comparison_options)