from typing import Optional, Union, Dict, List

import matplotlib.patches as patches
import matplotlib.lines as lines
import numpy as np
import pandas as pd

from quickstats.plots.template import single_frame, parse_styles, format_axis_ticks, centralize_axis
from quickstats.plots import AbstractPlot

class VBFValidationPlot(AbstractPlot):
    STYLES = {
        'figure':{
            'figsize': (16.111, 8.333),
            'dpi': 72,
            'facecolor': "#FFFFFF"
        },
        'text':{
            'fontsize': 25
        },
        'ylabel':{
            'labelpad': 0
        },
        'legend':{
            'loc': (0.5, 0.8)
        },
        'errorbar':{
            'marker': 'o',
            'markersize': 10,
        }
    }
    
    COLOR_PALLETE = {
        'expected': 'k',
        'observed': 'k',
    }
    
    CONFIG = {     
        'xmargin': 0.3,
        'ypadding': 1,
        'ylabellinebreak': None
    }
    
    
    def __init__(self, validation_df, bases:List,
                 color_pallete:Optional[Dict]=None,
                 styles:Optional[Union[Dict, str]]=None,
                 config:Optional[Dict]=None,
                 analysis_label_options:Optional[Union[Dict, str]]=None):

        super().__init__(color_pallete=color_pallete,
                         styles=styles, config=config,
                         analysis_label_options=analysis_label_options)
        self.validation_df = validation_df
        self.bases = bases
        
    def draw(self, xlabel:str="", ylabel:str=""):
        xmargin = self.config['xmargin']
        n_points = len(self.validation_df.index)
        ax = self.draw_frame()
        xlim = (0 - xmargin, n_points - 1 + xmargin)
        self.draw_axis_components(ax, ylabel=ylabel, xlabel=xlabel, xlim=xlim)
        ax.tick_params(axis="x", which="minor", direction='in', length=0)
        xticklabels = []
        x_bases = []
        y_bases = []
        y_errlo_bases = []
        y_errhi_bases = []
        x_validation = []
        y_validation = []
        y_errlo_validation = []
        y_errhi_validation = []
        param_errlo_bases = []
        param_errhi_bases = []
        param_errlo_validation = []
        param_errhi_validation = []        
        for i, (sample, data) in enumerate(self.validation_df.iterrows()):
            xticklabel = f"({data['klambda']}, {data['k2v']}, {data['kv']})"
            xticklabels.append(xticklabel)
            y = (data['param_yield'] - data['true_yield']) * 100 / data['true_yield']
            if "true_yield_errlo" in data:
                yerrlo = data["true_yield_errlo"] * 100 / data["true_yield"]
                yerrhi = data["true_yield_errhi"] * 100 / data["true_yield"]
            else:
                yerrlo = 0
                yerrhi = 0
            if "param_yield_errlo" in data:
                param_errlo = data["param_yield_errlo"] * 100 / data["true_yield"]
                param_errhi = data["param_yield_errhi"] * 100 / data["true_yield"]
            else:
                param_errlo = 0
                param_errhi = 0              
            if sample in self.bases:
                x_bases.append(i)
                y_bases.append(y)
                y_errlo_bases.append(yerrlo)
                y_errhi_bases.append(yerrhi)
                param_errlo_bases.append(param_errlo)
                param_errhi_bases.append(param_errhi)
            else:
                x_validation.append(i)
                y_validation.append(y)
                y_errlo_validation.append(yerrlo)
                y_errhi_validation.append(yerrhi)
                param_errlo_validation.append(param_errlo)
                param_errhi_validation.append(param_errhi)
                
        ax.errorbar(x=x_bases, y=y_bases,
                    yerr=(y_errlo_bases, y_errhi_bases),
                    color="r", label="Basis sample", **self.styles["errorbar"])
        ax.errorbar(x=x_validation, y=y_validation,
                    yerr=(y_errlo_validation, y_errhi_validation),
                    color="b", label="Validation sample", **self.styles["errorbar"])
        ylim = list(ax.get_ylim())
        
        for x_i, y_i, errlo_i, errhi_i in zip(x_bases, y_bases, param_errlo_bases, param_errhi_bases):
            p = patches.Rectangle((x_i-0.25, y_i-errlo_i), 0.5, errlo_i + errhi_i,
                                  linewidth=0, fill=None, hatch='//', alpha=0.5)
            ax.add_patch(p)
        for x_i, y_i, errlo_i, errhi_i in zip(x_validation, y_validation, param_errlo_validation, param_errhi_validation):
            p = patches.Rectangle((x_i-0.25, y_i-errlo_i), 0.5, errlo_i + errhi_i,
                                  linewidth=0, fill=None, hatch='//', alpha=0.5)
            ax.add_patch(p)
        ax.set_ylim(*ylim)
        ax.set_xticks(range(n_points))
        ax.set_xticklabels(xticklabels, rotation=45)
        centralize_axis(ax, which="y", ref_value=0, padding=self.config['ypadding'])
        ax.axhline(0, color='grey', linestyle='--')
        handles, labels = ax.get_legend_handles_labels()
        handles.append(p)
        labels.append("Linear combination uncertainties")
        legend = ax.legend(handles, labels, frameon=True, **self.styles['legend'])
        legend.get_frame().set_alpha(0.8)
        return ax