from typing import Optional, Dict, List
import os
import glob
import json
import itertools

import pandas as pd

from quickstats.plots import AbstractPlot
from quickstats.utils.common_utils import combine_dict

from bbyy_analysis_framework.components import AnalysisPathManager

class BootstrapDistributionPlot(AbstractPlot):
    
    STYLES = {
        "plot": {
            "linestyle": "None",
            "marker": "x"
        }
    }
    
    CONFIG = {
        "average_line": {
            "color_cycle": None,
            "styles": {
                "label": "average",
                "linestyle": "--",
                "linewidth": 1
            }
        },
        "error_band": {
            "color_cycle": None,
            "styles": {
                "label": r"$\pm 1 \sigma$",
                "alpha": 0.2
            }
        }
    }
    
    def __init__(self, color_cycle:Optional[Dict]=None,
                 config:Optional[Dict]=None,
                 styles:Optional[Dict]=None,
                 analysis_label_options:Optional[Dict]=None):
        super().__init__(color_cycle=color_cycle, config=config,
                         styles=styles, analysis_label_options=analysis_label_options)
        self.collective_data = {}
        self.label_map = {}
    
    def add_study(self, name:str, study_dir:str, limit_path:Optional[str]=None,
                  label:Optional[str]=None):
        if not os.path.exists(study_dir):
            raise FileNotFoundError(f"directory {study_dir} does not exist")
        outdirs = [d for d in glob.glob(os.path.join(study_dir, "*")) if os.path.isdir(d)]
        limit_data = []
        
        path_manager = AnalysisPathManager(base_path=study_dir)
        if limit_path is not None:
            dirname = os.path.dirname(limit_path)
            basename = os.path.basename(limit_path)
            path_manager.set_directory("limit", dirname)
            path_manager.set_file("limit_summary", ("limit", basename))
            
        for outdir in outdirs:
            study_name = os.path.basename(outdir)
            path_manager.set_study_name(study_name)
            path_manager.check_file("limit_summary")
            filename = path_manager.get_file("limit_summary")
            with open(filename, "r") as file:
                data = json.load(file)
            limit_data.append(data)
        df = pd.DataFrame(limit_data).reset_index()
        self.collective_data[name] = df
        if label is not None:
            self.label_map[name] = label
    
    def draw_graph(self, attrib:str, studies:Optional[List[str]]=None,
                   xlabel:str="Bootstrap Trial Number", ylabel:str="",
                   ymin:Optional[float]=None, ymax:Optional[float]=None,
                   xmin:Optional[float]=None, xmax:Optional[float]=None,
                   draw_average_line:bool=True,
                   draw_error_band:bool=True):
        if studies is None:
            studies = list(self.collective_data.keys())
        from quickstats.plots import General1DPlot
        plotter = General1DPlot(self.collective_data, label_map=self.label_map,
                                styles=self.styles,
                                analysis_label_options=self.analysis_label_options)
        plotter.prop_cycle = self.prop_cycle
        if draw_average_line:
            if self.config["average_line"]["color_cycle"] is not None:
                color_cycler = itertools.cycle(self.config["average_line"]["color_cycle"])
            else:
                color_cycler = None
            from quickstats.plots import AverageLineH
            for study in studies:
                styles = self.config["average_line"]["styles"]
                if color_cycler is not None:
                    styles = combine_dict(styles, {"color": next(color_cycler)})
                plotter.configure_stats([AverageLineH(**styles)], study)
        if draw_error_band:
            if self.config["error_band"]["color_cycle"] is not None:
                color_cycler = itertools.cycle(self.config["error_band"]["color_cycle"])
            else:
                color_cycler = None            
            from quickstats.plots import StdBandH
            for study in studies:
                styles = self.config["error_band"]["styles"]
                if color_cycler is not None:
                    styles = combine_dict(styles, {"color": next(color_cycler)})
                plotter.configure_stats([StdBandH(**styles)], study)
        ax = plotter.draw("index", attrib, targets=studies,
                          xlabel=xlabel, ylabel=ylabel,
                          ymin=ymin, ymax=ymax,
                          xmin=xmin, xmax=xmax,
                          draw_stats=True)
        return ax
        
    def draw_histogram(self, studies:Optional[List[str]]=None):
        pass