from typing import Optional, List, Dict

import pandas as pd

class LikelihoodScanPlot:
    def __init__(self, df:pd.DataFrame, parameters:List[str],
                 config:Optional[Dict]=None,
                 styles:Optional[Dict]=None,
                 analysis_label_options:Optional[Dict]=None):
        self.parameters = parameters
        if len(parameters) == 1:
            from quickstats.plots import Likelihood1DPlot
            self.plotter = Likelihood1DPlot(df, config=config,
                                            styles=styles,
                                            analysis_label_options=analysis_label_options)
        elif len(parameters) == 2:
            from quickstats.plots import Likelihood2DPlot
            self.plotter = Likelihood2DPlot(df, config=config,
                                            styles=styles,
                                            analysis_label_options=analysis_label_options)
            self.plotter.add_highlight(1, 1, label="SM prediction")
        else:
            raise RuntimeError("only 1D or 2D likelihood scans are supported")
            
    def draw(self, **kwargs):
        from quickstats.plots import Likelihood1DPlot, Likelihood2DPlot
        if isinstance(self.plotter, Likelihood1DPlot):
            xattrib = self.parameters[0]
            yattrib = "qmu"
            ax = self.plotter.draw(xattrib=xattrib, yattrib=yattrib, **kwargs)
        elif isinstance(self.plotter, Likelihood2DPlot):
            xattrib = self.parameters[0]
            yattrib = self.parameters[1]
            zattrib = "qmu"
            
            ax = self.plotter.draw(xattrib=xattrib, yattrib=yattrib, zattrib=zattrib, **kwargs)
        return ax