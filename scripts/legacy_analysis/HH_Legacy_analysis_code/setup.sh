#!/bin/bash

if [ "$#" -ge 1 ];
then
    EnvironmentName=$1
else
    EnvironmentName="default"
fi


# set up environmental paths
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done


export DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

if [ "$EnvironmentName" = "default" ]; #Default is python3
then
    export PATH=${DIR}/bin:${PATH}
    export PYTHONPATH=${DIR}:${PYTHONPATH}

    #export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    #source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    #lsetup "views LCG_102a x86_64-centos7-gcc11-opt"
    
    export xmlAnaWSBuilder_DIR=${DIR}/submodules/xmlAnaWSBuilder
    export RooFitExtensions_DIR=${DIR}/submodules/RooFitExtensions
    export LD_LIBRARY_PATH=${RooFitExtensions_DIR}/build:${xmlAnaWSBuilder_DIR}/lib:${LD_LIBRARY_PATH}
    export PATH=${xmlAnaWSBuilder_DIR}/bin:${PATH}

    export PATH=${DIR}/bin:${DIR}/submodules/quickstats/bin:$PATH
    export PYTHONPATH=${DIR}/submodules/quickstats:$PYTHONPATH
    export PYTHONPATH=${DIR}/submodules/xgboost/python-package/:$PYTHONPATH

elif [[ "$EnvironmentName" = "dev" ]];
then
    QSPATH=/afs/cern.ch/work/c/chlcheng/Repository/quickstats
    HPOPATH=/afs/cern.ch/work/c/chlcheng/Repository/hpogrid
    export PATH=${DIR}/bin:/afs/cern.ch/work/c/chlcheng/local/miniconda/envs/root-latest/bin:$PATH
    export PATH=${DIR}/bin:${PATH}
    export PATH=${QSPATH}/bin:${PATH}
    export PYTHONPATH=${DIR}:${PYTHONPATH}
    export PYTHONPATH=${QSPATH}:${PYTHONPATH}
    export PYTHONPATH=${HPOPATH}:${PYTHONPATH}
    export LD_LIBRARY_PATH=/afs/cern.ch/work/c/chlcheng/Repository/xgboost/lib:$LD_LIBRARY_PATH

elif [[ "$EnvironmentName" = "release" ]];
then
    export PATH=${DIR}/bin:/afs/cern.ch/work/c/chlcheng/local/miniconda/envs/root-latest/bin:$PATH
    export PATH=${DIR}/bin:${PATH}
    export PYTHONPATH=${DIR}:${PYTHONPATH}
elif [[ "$EnvironmentName" = "athena" ]];
then
    setupATLAS
    lsetup git
    asetup Athena,latest,master
    QSPATH=/afs/cern.ch/work/c/chlcheng/Repository/quickstats
    LOCALPATH=/afs/cern.ch/user/c/chlcheng/.local/bin
    export PATH=$LOCALPATH:${QSPATH}/bin:${DIR}/bin:${PATH}
    export PYTHONPATH=${QSPATH}:${DIR}:${PYTHONPATH}
elif [[ "$EnvironmentName" = "panda" ]];
then
    setupATLAS
    lsetup panda
    lsetup rucio
    export PATH=${DIR}/bin:${PATH}
    export PYTHONPATH=${DIR}:${PYTHONPATH}

    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    lsetup "views LCG_102a x86_64-centos7-gcc11-opt"
    
    #export xmlAnaWSBuilder_DIR=${DIR}/submodules/xmlAnaWSBuilder
    #export RooFitExtensions_DIR=${DIR}/submodules/RooFitExtensions
    #export LD_LIBRARY_PATH=${RooFitExtensions_DIR}/build:${xmlAnaWSBuilder_DIR}/lib:${LD_LIBRARY_PATH}
    #export PATH=${xmlAnaWSBuilder_DIR}/bin:${PATH}

    export PATH=${DIR}/bin:${DIR}/submodules/quickstats/bin:$PATH
    export PATH=${DIR}/submodules/hpogrid/bin:$PATH
    export PYTHONPATH=${DIR}/submodules/quickstats:$PYTHONPATH
    export PYTHONPATH=${DIR}/submodules/hpogrid:$PYTHONPATH
    export PYTHONPATH=${DIR}/submodules/xgboost/python-package/:$PYTHONPATH
fi

export BBYYFRAMEWORKDIR=${DIR}