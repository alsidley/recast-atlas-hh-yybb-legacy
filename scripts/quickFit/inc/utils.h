#ifndef UTILS_HEADER
#define UTILS_HEADER

#include "RooStatsHead.h"
#include "RooFitHead.h"
#include "CommonHead.h"
#include "auxUtils.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

class utils : public TObject {
public:
  static void collectEverything( ModelConfig *mc, RooArgSet *set );
  static void fixRooStarCache( RooWorkspace *ws );
  static void Reset(RooArgSet* original, RooArgSet* snapshot);
  static void setValAndFix(RooRealVar *var, double value);
  static void randomizeSet(RooAbsPdf* pdf, RooArgSet* globs, int seed=0);
  static RooRealVar* setupPOI(ModelConfig *mc, const std::string _poiStr, RooArgSet* fitPOIs, bool mustFloat=false);
  static void loadSnapshot(RooWorkspace *ws, const std::string _snapshot);
  ClassDef(utils, 0);
};

#endif
