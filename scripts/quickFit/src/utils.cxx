#include "utils.h"

ClassImp(utils)

void utils::collectEverything( ModelConfig *mc, RooArgSet *set ) {
  if(mc->GetNuisanceParameters()) set->add(*mc->GetNuisanceParameters());
  else{
    cout<<"\t WARNING: No nuisance parameter set defined in ModelConfig. Inject an empty set"<<endl;
    mc->SetNuisanceParameters(RooArgSet());
  }
  if(mc->GetGlobalObservables()) set->add(*mc->GetGlobalObservables());
  else{
    cout<<"\t WARNING: No global observable set defined in ModelConfig. Inject an empty set"<<endl;
    mc->SetGlobalObservables(RooArgSet());
  }
  if(mc->GetParametersOfInterest()) set->add(*mc->GetParametersOfInterest());
  else{
    cout<<"\t WARNING: No parameter of interest set defined in ModelConfig. Aborting..."<<endl;
    abort();
  }
}

void utils::fixRooStarCache( RooWorkspace *ws ) {
  RooFIter iter = ws->components().fwdIterator();  
  RooAbsArg* arg;
  while ((arg = iter.next())) {
    if (arg->IsA() == RooStarMomentMorph::Class()) {
      ((RooStarMomentMorph*)arg)->fixCache();
    }
  }
}

void utils::Reset(RooArgSet* original, RooArgSet* snapshot){
  *original=*snapshot;
  // Still need to recover the ranges for variables
  unique_ptr<TIterator> iter(original->createIterator());
  RooRealVar* parg = NULL;  
  while((parg=dynamic_cast<RooRealVar*>(iter->Next()))){
    RooRealVar *snapVar=dynamic_cast<RooRealVar*>(snapshot->find(parg->GetName()));
    parg->setRange(snapVar->getMin(), snapVar->getMax());
  }
}

void utils::setValAndFix(RooRealVar *var, double value){
  if ( var->getMax() < value ) {
    var->setMax(value + 1);
  } else if ( var->getMin() > value ) {
    var->setMin(value - 1);
  }
  var->setVal(value);
  var->setConstant(true);
}

void utils::randomizeSet(RooAbsPdf* pdf, RooArgSet* globs, int seed){
  if(seed>=0) RooRandom::randomGenerator() -> SetSeed(seed) ; // This step is necessary
  unique_ptr<RooDataSet> pseudoGlobals(pdf->generateSimGlobal(*globs, 1));
  unique_ptr<RooArgSet> allVars(pdf->getVariables());
  allVars->assignValueOnly(*pseudoGlobals->get(0));
}

RooRealVar* utils::setupPOI(ModelConfig *mc, const std::string _poiStr, RooArgSet* fitPOIs, bool mustFloat){
  RooRealVar *firstPOI=NULL;
  RooWorkspace *ws=mc->GetWS();
  cout << endl << "Preparing parameters of interest : "<< _poiStr << endl;
  std::vector<std::string> poiStrs_temp = auxUtils::Tokenize( _poiStr, "," ),  poiStrs;

  // Replace wildcard
  for(auto poiSetup : poiStrs_temp){
    if(poiSetup.find('*')!=std::string::npos){
      // poiStrs.erase(std::remove(poiStrs.begin(), poiStrs.end(), poiSetup), poiStrs.end());
      std::vector<std::string> poiTerms = auxUtils::Tokenize( poiSetup, "=" );
      TString poiNameWild = (TString) poiTerms[0];
      RooAbsCollection *poiList = mc->GetWS()->allVars().selectByName(poiNameWild);
      for (RooLinkedListIter it = poiList->iterator(); RooRealVar* poi = dynamic_cast<RooRealVar*>(it.Next());) {
	TString poiName=poi->GetName();
	TString newSetup=poiTerms.size()>1?poiName+"="+poiTerms[1]:poiName;
	poiStrs.push_back(newSetup.Data());
	// cout<<"Adding "<<newSetup<<endl;
      }
    }
    else poiStrs.push_back(poiSetup);
  }
    
  for( unsigned int ipoi(0); ipoi < poiStrs.size(); ipoi++ ) {
    std::vector<std::string> poiTerms = auxUtils::Tokenize( poiStrs[ipoi], "=" );
    TString poiName = (TString) poiTerms[0];

    // check if variable is in workspace
    if (not ws->var(poiName))  {
      cout << auxUtils::FAIL << "Variable " << poiName << " not in workspace. Skipping." << auxUtils::ENDC << endl;
      continue;
    }

    // set variable for fit
    fitPOIs->add( *(ws->var(poiName)), true );
    if (poiTerms.size() > 1) {
      std::vector<std::string> poiVals = auxUtils::Tokenize( poiTerms[1], "_" );
      if (poiVals.size() == 3) { // Specify range and central value
	ws->var(poiName)->setRange( std::stof(poiVals[1]), std::stof(poiVals[2]) );
	ws->var(poiName)->setVal( std::stof(poiVals[0]) );
	ws->var(poiName)->setConstant( kFALSE );
      }
      else if(poiVals.size() == 2){ // Specify only range
	ws->var(poiName)->setRange( std::stof(poiVals[0]), std::stof(poiVals[1]) );
	ws->var(poiName)->setConstant( kFALSE );
      }
      else if(poiVals.size() == 1) { // Fix the parameter to current value
	if ( std::stof(poiVals[0]) > ws->var(poiName)->getMax() ) {
	  ws->var(poiName)->setRange( ws->var(poiName)->getMin(), 2*std::stof(poiVals[0]) );
	}
	if ( std::stof(poiVals[0]) < ws->var(poiName)->getMin() ) {
	  ws->var(poiName)->setRange( -2*abs(std::stof(poiVals[0])), ws->var(poiName)->getMax() );
	}
	ws->var(poiName)->setVal( std::stof(poiVals[0]) );
	ws->var(poiName)->setConstant( kTRUE );
      }
    }
    else {
      ws->var(poiName)->setConstant( kFALSE );
    }
    cout << "   ";
    // ws->var(poiName)->Print();
    if(!firstPOI && !ws->var(poiName)->isConstant()){
      firstPOI=ws->var(poiName);
      cout << "\tREGTEST: Set first POI to "<< firstPOI->GetName() <<endl;
    }
  }

  fitPOIs->add(*mc->GetParametersOfInterest(), true );
  if(fitPOIs->getSize()==0){
    std::cout<<auxUtils::FAIL<<"No POI specificied or provided. Aborting..."<<auxUtils::ENDC<<endl;
    abort();
  }

  if(!firstPOI&&mustFloat){
    firstPOI = (RooRealVar*)mc->GetParametersOfInterest()->first();
    cout << endl << "Float the first POI in the ModelConfig " << firstPOI->GetName() << endl;
    firstPOI->setConstant(kFALSE);
    cout << "   ";
    firstPOI->Print();
  }
  return firstPOI;
}

void utils::loadSnapshot(RooWorkspace *ws, const std::string _snapshot){
  if (_snapshot != "") {
    vector<std::string> snapshots=auxUtils::Tokenize(_snapshot, ",");
    for (auto snapshot : snapshots){
      std::cout<<"\tREGTEST: Loading snapshot "<<snapshot<<std::endl;
      if (not ws->loadSnapshot( (TString) snapshot )) {
	std::cout<<auxUtils::FAIL << "Error: Unable to load snapshot " << snapshot << " from workspace. Aborting..."<<auxUtils::ENDC << endl;
	abort();
      }
    }
  }
}
