
#ifndef TXGXEFFBW
#define TXGXEFFBW

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include "RooDataHist.h"
#include "TH1D.h"
#include "TFile.h"
//#include "TH1.h"
//#include "TH1F.h"
#include "TCanvas.h"
#include <vector>
 
//class ABWxG_VBF : public RooAbsCachedPdf {
class ABWxG_VBF : public RooAbsPdf {
public:
  // default constructor
  ABWxG_VBF() { n=0.97; alpha=0.59;  GenerateHistogram(); } ; 
  //ABWxG_VBF() { n=0.97; alpha=0.59;  GenerateHistogram(); } ; 
  // construct ABWxG_VBF using the given RooFit parameters
  ABWxG_VBF(const char *name, const char *title,
	RooAbsReal& _x,
	RooAbsReal& _mass,
	RooAbsReal& _width,
	RooAbsReal& _gausmu,
        RooAbsReal& _gamma,
	RooAbsReal& _ich,
        double _integrationPrecision=0.5,
        bool _coarse=true,
        bool _ztail=false);
  
  ABWxG_VBF(const ABWxG_VBF& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new ABWxG_VBF(*this,newname); }
  inline virtual ~ABWxG_VBF() { }

    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const;
    Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const;
	
  // parameters of the ABW used when caching the ABW values
  mutable double arg;
  mutable double dtau;
  mutable double tau;
  mutable double maxtau;
  mutable double ret;
  mutable double n;
  mutable double alpha;
  mutable int iChannel;
  mutable double integrationRange;
  mutable double integrationPrecision;
  mutable bool coarse;
  mutable bool ztail;

protected:

  // Vectors of the internal cached ABW values
  std::vector<double> bin_widths;
  std::vector<double> bin_values_4mu;
  std::vector<double> bin_values_2mu2e;
  std::vector<double> bin_values_2e2mu;
  std::vector<double> bin_values_4e;
  std::vector<double> bin_center;
  std::vector<double> bin_walls;
  std::vector<double> bin_integral_4mu;
  std::vector<double> bin_integral_2mu2e;
  std::vector<double> bin_integral_2e2mu;
  std::vector<double> bin_integral_4e;
  
  std::vector<double> eff_p0;
  std::vector<double> eff_p1;
  std::vector<double> eff_p2;

  std::vector<double> gaus_eff_norm; 
  std::vector<double> gaus_eff_mu; 
  std::vector<double> gaus_eff_sig; 

  mutable double xhistlow;
  mutable double xhisthigh;
  // variables used repeatedly by code
  mutable int    bin_size;
  mutable double the_dx;
  mutable double the_x;
  mutable double width2;

  mutable double Ratio4mu;
  mutable double Ratio2mu2e;
  mutable double Ratio2e2mu;
  mutable double Ratio4e;

  mutable double _invRootPi;
  mutable double _width2;
  mutable double _c;
  mutable double _a;
  mutable double _arg;
  mutable double _voit;
  mutable double _bw;
  mutable double _gaus;

  // internal RooFit parameters
  RooRealProxy x ;
  RooRealProxy mass ;
  RooRealProxy width ;
  RooRealProxy gausmu ;
  RooRealProxy gamma ;
  RooRealProxy ich ;
 
  // required to implement this since
  // the class inherits from RooAbsPdf
  Double_t evaluate() const ;
  
  // generate and cache ABW values
  void GenerateHistogram();

 Double_t Efficiency(double tmpx) const;
 Double_t Voigt(double tmpx) const;


private:

  ClassDef(ABWxG_VBF,1) // Your description goes here...
};
 
#endif
