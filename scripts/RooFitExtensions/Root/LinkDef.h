//this file is -*- c++ -*- 
#ifndef __RooFitUtilsDICT__
#define __RooFitUtilsDICT__

#include "RooFitExtensions/ABWxG.h"
#include "RooFitExtensions/ABWxG_VBF.h"
#include "RooFitExtensions/ABWxGWithGamma.h"
#include "RooFitExtensions/AsymmBreitWigner.h"
#include "RooFitExtensions/PearsonTypeIV.h"
#include "RooFitExtensions/RooAbsListContainer.h"
#include "RooFitExtensions/RooBSplineBases.h"
#include "RooFitExtensions/RooBSpline.h"
#include "RooFitExtensions/RooExpandedDataHist.h"
#include "RooFitExtensions/RooExpandedHistPdf.h"
#include "RooFitExtensions/RooMCHistConstraint.h"
#include "RooFitExtensions/RooParamHistPdf.h"
#include "RooFitExtensions/RooParamKeysPdfDev.h"
#include "RooFitExtensions/RooParamKeysPdf.h"
#include "RooFitExtensions/RooStarMomentMorph.h"
#include "RooFitExtensions/RooTwoSidedCBShape.h"
#include "RooFitExtensions/Roo1DMomentMorphFunction.h"
#include "RooFitExtensions/Roo2DMomentMorphFunction.h"
#include "RooFitExtensions/RooEmpXHistShape.h"
#include "RooFitExtensions/RooBernsteinM.h"
#include "RooFitExtensions/HggBernstein.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class ABWxG+;
#pragma link C++ class ABWxG_VBF+;
#pragma link C++ class ABWxGWithGamma+;
#pragma link C++ class AsymmBreitWigner+;
#pragma link C++ class PearsonTypeIV+;
#pragma link C++ class RooAbsListContainer+;
#pragma link C++ class RooStats::HistFactory::RooBSplineBases+;
#pragma link C++ class RooStats::HistFactory::RooBSpline+;
#pragma link C++ class RooExpandedDataHist+;
#pragma link C++ class RooExpandedHistPdf+;
#pragma link C++ class RooMCHistConstraint+;
#pragma link C++ class RooParamHistPdf+;
#pragma link C++ class RooParamKeysPdfDev+;
#pragma link C++ class RooParamKeysPdf+;
#pragma link C++ class RooStarMomentMorph+;
#pragma link C++ class RooTwoSidedCBShape+;
#pragma link C++ class Roo1DMomentMorphFunction+;
#pragma link C++ class Roo2DMomentMorphFunction+;
#pragma link C++ class RooEmpXHistShape+;
#pragma link C++ class RooBernsteinM+;
#pragma link C++ class HggBernstein+;

#endif
#endif
