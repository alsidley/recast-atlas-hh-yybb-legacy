import os

import yaml
import json
from xml_tool import TXMLTree
from utils import get_n_digit, to_pc_str, makedirs, mass_points, process_map

res_func_prefix_map = {
    'spread_shape': 'prod::resp_sigma',
    'position_shape': 'prod::resp_mu'
}

_CATEGORY_ = "Resonant"

def parse_syst_file(fname, prefix='ATLAS'):
    '''parsing systematics file according to format from database
    '''
    if not os.path.exists(fname):
        raise ValueError("File {} does not exist".format(fname))
    systematics = {}
    data = [[token.strip() for token in line.split()] for line in open(fname, 'r').readlines()]
    for d in data:
        if len(d) == 1:
            return d[0]
        if prefix:
            syst_name = '{}_{}'.format(prefix, d[0])
        else:
            syst_name = d[0]
        if len(d) == 2:
            err = to_pc_str(d[1])
            systematics[syst_name] = err
        elif len(d) == 3:
            err_lo = to_pc_str(d[1])
            err_hi = to_pc_str(d[2])
            systematics[syst_name] = (err_lo, err_hi) 
        else:
            raise ValueError('unknown systematics format')
    return systematics

def extract_experimental_systematics(base_path):
    syst_type = ['position_shape', 'spread_shape', 'yield']
    exp_syst_processes = ['gg_HH_resonant', 'gg_HH_non_resonant', 'vbf_HH_non_resonant', 'ZH' , 'ttH', 'ggH']
    exp_syst_pattern = {'position_shape': "datacard_experimental_{}_m_yy_proc_{}_Max_cat_Resonant.txt",
                        'spread_shape': "datacard_experimental_{}_m_yy_proc_{}_Max_cat_Resonant.txt",
                        'yield': "datacard_experimental_{}_proc_{}_Max_cat_Resonant.txt"}
    exp_syst_proc_map = {'vbf_HH_non_resonant': 'HH_VBF_non_resonant',
                         'gg_HH_non_resonant': 'HH_ggF_non_resonant',
                         'gg_HH_resonant': 'HH_ggF_resonant'}
    experimental_systematics = {}
    for stype in syst_type:
        experimental_systematics[stype] = {}
        for process in exp_syst_processes:
            syst_path = exp_syst_pattern[stype].format(stype, process)
            if process == "gg_HH_non_resonant":
                syst_path = os.path.join("version_with_gg_HH_from_Py8", syst_path)
            fname = os.path.join(base_path, syst_path)
            proc_name = exp_syst_proc_map.get(process, process)
            experimental_systematics[stype][proc_name] = {}
            experimental_systematics[stype][proc_name][_CATEGORY_] = parse_syst_file(fname)
    return experimental_systematics


def extract_spurious_signal_systematics(base_path):
    spurious_signal_pattern = 'spurious_signal_cat_Resonant_mX{mass}.txt'
    spurious_signal_systematics = {}
    for mass in mass_points:
        mass_str = mass.replace('.','p')
        fname = os.path.join(base_path, spurious_signal_pattern.format(mass=mass))
        spurious_signal_systematics[mass_str] = parse_syst_file(fname)
    return spurious_signal_systematics
    
def extract_theory_systematics(base_path=None):
    """hardcoded value for now
    """
    old_theory_systematics = \
    {'yield': {None: {'Resonant': {'THEO_XS_MHiggs': '0.0019',
        'THEO_Lumi_Run2': '0.017',
        'THEO_BR_Hyy': ('0.029', '-0.0284')}},
      'HH_ggF_non_resonant': {'Resonant': {'THEO_XS_alphas': '0.03',
        'THEO_XS_QCD': ('0.022', '-0.05'),
        'THEO_BR_Hbb': ('0.017', '-0.0173'),
        'THEO_XS_MTop': ('0.026', '0.026')}},
      'HH_VBF_non_resonant': {'Resonant': {'THEO_XS_alphas': '0.03',
        'THEO_BR_Hbb': ('0.017', '-0.0173'),
        'THEO_XS_QCD': ('0.0003', '-0.0004')}},
      'HH_ggF_resonant': {'Resonant': {'THEO_XS_alphas': '0.00131',
        'THEO_XS_Parton_Shower': '0.024'}},
      'ZH': {'Resonant': {'THEO_XS_alphas': '0.07523', 'THEO_XS_QCD': '0.1221'}},
      'ttH': {'Resonant': {'THEO_XS_alphas': '0.12541',
        'THEO_XS_QCD': '0.08776'}}},
     'spread_shape': {'HH_ggF_resonant': {'Resonant': {'THEO_XS_alphas': '0.00277'}},
      'ZH': {'Resonant': {'THEO_XS_alphas': '0.01355', 'THEO_XS_QCD': '0.02956'}},
      'ttH': {'Resonant': {'THEO_XS_alphas': '0.03228',
        'THEO_XS_QCD': '0.01431'}}}}
    
    theory_systematics = \
        {'yield': {None: {'Resonant': {'THEO_XS_MHiggs': '0.0019',
        'THEO_Lumi_Run2': '0.017',
        'THEO_BR_Hyy': ('0.029', '-0.0284')}},
      'HH_ggF_non_resonant': {'Resonant': {'THEO_XS_alphas': '0.03',
        'THEO_XS_QCD': ('0.022', '-0.05'),
        'THEO_BR_Hbb': ('0.017', '-0.0173'),
        'THEO_XS_MTop': ('0.06', '-0.23'),
        'THEO_XS_Parton_Shower': '-0.0241'}},
      'HH_VBF_non_resonant': {'Resonant': {'THEO_XS_alphas': '0.021',
        'THEO_BR_Hbb': ('0.017', '-0.0173'),
        'THEO_XS_QCD': ('0.0003', '-0.0004'),
        'THEO_XS_Parton_Shower': '-0.0957'}},
      'HH_ggF_resonant': {'Resonant': {'THEO_XS_alphas': '0.00131',
        'THEO_XS_Parton_Shower': '0.1406'}},
      'ZH': {'Resonant': {'THEO_XS_alphas': '0.07523', 'THEO_XS_QCD': '0.1221'}},
      'ttH': {'Resonant': {'THEO_XS_alphas': '0.12541',
        'THEO_XS_QCD': '0.08776'}}},
     'spread_shape': {'HH_ggF_resonant': {'Resonant': {'THEO_XS_alphas': '0.00277',
        'THEO_XS_Parton_Shower': '0.0223'}},
      'ZH': {'Resonant': {'THEO_XS_alphas': '0.01355', 'THEO_XS_QCD': '0.02956'}},
      'ttH': {'Resonant': {'THEO_XS_alphas': '0.03228',
        'THEO_XS_QCD': '0.01431'}}}}

    return theory_systematics

def get_constraint_type(syst_name):
    if syst_name == 'THEO_XS_MHiggs':
        return 'gaus'
    else:
        return 'logn'

BKG_SPECS = {
   'XSection': '1',
   'SelectionEff': '1',
   'ImportSyst': ':self:',
   'MultiplyLumi': '0'
}

def write_spurious_signal(node, model_path, magnitude, category=None):
    sample = node.add_node('Sample',
                           Name='spurious',
                           InputFile=model_path,
                           **BKG_SPECS)
    syst_name = "SPURIOUS" if category is None else "SPURIOUS_{}".format(category)
    sample.add_node('Systematic',
                    Name=syst_name,
                    Constr="gaus",
                    CentralValue="0",
                    Mag=str(magnitude),
                    WhereTo="yield")
    
def write_background(node, model_path, magnitude, vmin="0", vmax="1000000", category=None):
    sample = node.add_node('Sample',
                           Name='background',
                           InputFile=model_path,
                           **BKG_SPECS)
    name = "nbkg" if category is None else "nbkg_{}".format(category)
    norm_factor = "{}[{},{},{}]".format(name, magnitude, vmin, vmax)
    sample.add_node("NormFactor",
                    Name=norm_factor)

def write_systematics(node, syst_type, systematics, process=None):
    if 'shape' in syst_type:
        if process is None:
            raise ValueError('process name must be provided for shape systematics')    
    if syst_type == 'yield':
        whereto = 'yield'
    else:
        whereto = 'shape'
    for syst_name in systematics:
        values = systematics[syst_name]
        if isinstance(values, list):
            values = tuple(values)
        if not isinstance(values, tuple):
            constraint_type = get_constraint_type(syst_name)
        else:
            constraint_type = 'asym'
            values = ','.join([v if isinstance(v, str) else "{:.4f}".format(v) for v in values])
        specs = {
            'Name': syst_name,
            'Constr': constraint_type,
            'CentralValue': "1",
            'Mag': values,
            'WhereTo': whereto,
        }
        if process is not None:
            if syst_type == 'spread_shape':
                specs['Process'] = '{}_res'.format(process)
            elif syst_type == 'position_shape':
                specs['Process'] = '{}_scale'.format(process)
            else:
                specs['Process'] = process
        node.add_node('Systematic', **specs)
    if 'shape' in syst_type:
        resp_func_prefix = '{}_{}'.format(res_func_prefix_map[syst_type], process)
        if syst_type == 'spread_shape':
            parameters = ['response::{}_{}_res'.format(syst_name, process) for syst_name in systematics]
        elif 'position_shape':
            parameters = ['response::{}_{}_scale'.format(syst_name, process) for syst_name in systematics]
        else:
            raise ValueError('unknown shape systematics type: {}'.format(syst_type))
        resp_func = '{}({})'.format(resp_func_prefix, ','.join(parameters))
        node.add_node('Item', Name=resp_func)
        
def create_signal_model_xml(process, mu_response=True, sigma_response=True, model_parameters=None):
    xml = TXMLTree(doctype='Model', system='AnaWSBuilder.dtd')
    xml.new_root('Model', Type='UserDef', CacheBinning='100000') 
    if model_parameters is not None:
        for param in model_parameters:
            xml.add_node('Item', Name="{}_{}[{}]".format(param, process, model_parameters[param]))
    if mu_response:
        name = "prod::muCB_{process}(muCBNom_{process}, resp_mu_{process})".format(process=process)
    else:
        name = "prod::muCB_{process}(muCBNom_{process}, prod::resp_mu_{process}())".format(process=process)
    xml.add_node('Item', Name=name)
    if sigma_response:
        name = "prod::sigmaCB_{process}(sigmaCBNom_{process}, resp_sigma_{process})".format(process=process)
    else:
        name = "prod::sigmaCB_{process}(sigmaCBNom_{process}, prod::resp_sigma_{process}())".format(process=process)        
    xml.add_node('Item', Name=name)        
    name = 'RooTwoSidedCBShape::signal(:observable:, ' +\
           'muCB_{process}, sigmaCB_{process}, alphaCBLo_{process},' +\
           'nCBLo_{process}, alphaCBHi_{process}, nCBHi_{process})'
    name = name.format(process=process)
    xml.add_node('ModelItem', Name=name)
    return xml

def create_background_model_xml(model_parameters=None):
    xml = TXMLTree(doctype='Model', system='AnaWSBuilder.dtd')
    xml.new_root('Model', Type='UserDef') 
    xml.add_node('Item', Name='xi[1,-10,100]')
    xml.add_node('ModelItem', Name="RooExponential::background_cont(:observable:,xi)")
    return xml
    
def create_model_xml(signal_modelling, systematics, processes):
    model_xmls = {}
    sigma_processes = list(systematics.get('spread_shape', {}))
    mu_processes = list(systematics.get('position_shape', {}))
    for process in processes:
        sigma_response = process in sigma_processes
        mu_response = process in mu_processes
        model_xmls[process] = create_signal_model_xml(process, mu_response=mu_response, 
                                                      sigma_response=sigma_response, model_parameters=signal_modelling)
    model_xmls['background'] = create_background_model_xml()
    return model_xmls

def create_category_xml(yields, efficiency, systematics, processes, mass,
                        spurious_signal, br, background):
    xml = TXMLTree(doctype='Channel', system='AnaWSBuilder.dtd')
    # create root node
    xml.new_root('Channel', 
                 Name='resonant',
                 Type='shape',
                 Lumi='139000')
    xml.add_node('Data', 
                 InputFile="config/data/mass_points_data_{}.txt".format(mass),
                 Observable="atlas_invMass_resonant[105,160]",
                 Binning="55",
                 InjectGhost="1",
                 BlindRange="120,130")
    # common yield systematics
    yield_systematics = systematics.get('yield', {})
    common_yield_syst = yield_systematics.get(None, {})
    common_yield_syst = common_yield_syst.get(_CATEGORY_, {})
    write_systematics(xml, syst_type='yield', systematics=common_yield_syst)
    # position-shape systematics
    position_shape_systematics = systematics.get('position_shape', {})
    for process in processes:
        if process in position_shape_systematics:
            write_systematics(xml, syst_type='position_shape', 
                              systematics=position_shape_systematics[process][_CATEGORY_], process=process)
    # spread-shape systematics
    spread_shape_systematics = systematics.get('spread_shape', {})
    for process in processes:
        if process in spread_shape_systematics:
            write_systematics(xml, syst_type='spread_shape', 
                              systematics=spread_shape_systematics[process][_CATEGORY_], process=process)
    # sample node for signal and (non-continuun) background processes
    for process in processes:
        multiply_lumi = "1" if process == "HH_ggF_resonant" else "0"
        model_path = os.path.join("config", "models", "{}_{}.xml".format(process, mass))
        sample_node = xml.add_node('Sample', 
                                   Name=process, 
                                   XSection="1", 
                                   SelectionEff="1", 
                                   InputFile=model_path,
                                   ImportSyst=":common:",
                                   MultiplyLumi=multiply_lumi)
        # write process-specific yield systematics
        if process in yield_systematics:
            write_systematics(sample_node, syst_type='yield', systematics=yield_systematics[process][_CATEGORY_])
        # write process yield
        process_yield = yields.get(process, "0")
        if process not in yields:
            print('WARNING: Yield information missing for the process "{}". Set to 0 by default.'.format(process))        
        if process != "HH_ggF_resonant":
            sample_node.add_node('NormFactor', Name="yield_{}[{}]".format(process, process_yield))
        # write normalization factors
        sample_node.add_node('NormFactor', Name="mu[1]")
        if "HH" in process:
            sample_node.add_node('NormFactor', Name="mu_XS_HH[1]")
            if "non_resonant" in process:
                sample_node.add_node('NormFactor', Name="mu_XS_HH_non_res[1]")
            else:
                sample_node.add_node('NormFactor', Name="mu_XS_HH_res[1]")
        else:
            sample_node.add_node('NormFactor', Name="mu_XS_H[1]")

        if process == "HH_ggF_resonant":
            signal_efficiency = efficiency['HH_ggF_resonant']
            sample_node.add_node('NormFactor', Name='BR_HHyybb[{}]'.format(br))
            sample_node.add_node('NormFactor', Name='eff_HH_res[{}]'.format(signal_efficiency))
        else:
            sample_node.add_node('NormFactor', Name="mu_{}[1]".format(process))
    spurious_signal_model_path = os.path.join("config", "models", "HH_ggF_resonant_{}.xml".format(mass))
    write_spurious_signal(xml, model_path=spurious_signal_model_path, magnitude=spurious_signal)
    background_model_path = os.path.join("config", "models", "background_{}.xml".format(mass))
    write_background(xml, model_path=background_model_path, magnitude=background)
    return xml

def create_input_xml(mass, processes):
    xml = TXMLTree(doctype='Combination', system='AnaWSBuilder.dtd')
    xml.new_root('Combination', 
                 WorkspaceName='combWS',
                 ModelConfigName='ModelConfig',
                 DataName='combData',
                 OutputFile="workspace/WS-yybb-resonant_{}.root".format(mass),
                 Blind="0")
    xml.add_node('Input', text="config/categories/category_{}.xml".format(mass))
    pois = ['mu_XS_HH_res', 'mu', 'mu_XS_HH_non_res', 'mu_XS_HH'] + ['mu_{}'.format(p) for p in processes if 'HH' not in p]
    xml.add_node('POI', text=','.join(pois))
    xml.add_node('Asimov',
                 Name="asimovData_0",
                 Setup="mu=0,mu_XS_HH=0",
                 Action="fixsyst:fit:genasimov:float:savesnapshot",
                 SnapshotNuis="nominalNuis_0",
                 SnapshotGlob="nominalGlob_0")
    xml.add_node('Asimov',
                 Name="asimovData_1",
                 Setup="mu=1,mu_XS_HH=1",
                 Action="genasimov")
    xml.add_node('Asimov',
                 Name="UCMLES0Snap",
                 Setup="",
                 Action="savesnapshot",
                 SnapshotAll="ucmles_0")
    return xml

def merge_systematics(systematics_list):
    merged = {}
    for systematics in systematics_list:
        for syst_type in systematics:
            if syst_type not in merged:
                merged[syst_type] = {}
            for process in systematics[syst_type]:
                if process not in merged[syst_type]:
                    merged[syst_type][process] = {}
                for category in systematics[syst_type][process]:
                    if category not in merged[syst_type][process]:
                        merged[syst_type][process][category] = {}
                    merged[syst_type][process][category].update(systematics[syst_type][process][category])
    return merged

def append_ATLAS_label(systematics):
    for syst_type in systematics:
        for process in systematics[syst_type]:
            for category in systematics[syst_type][process]:
                systematics[syst_type][process][category] = \
                {"ATLAS_{}".format(k):v for k,v in systematics[syst_type][process][category].items()}
                


def create_res_xmls(yields_path, efficiency_path, model_parameters_path, database_path, 
                    experimental_systematics_path=None, outdir='yybb_resonant'):
    resonant_path = os.path.join(database_path, "Resonant_IJCLab", "baseline")
    exp_syst_path = os.path.join(resonant_path, "Experimental_systematics")
    spurious_signal_path = os.path.join(resonant_path , "BkgModelling_SpuriousSignal")
    
    if experimental_systematics_path is not None:
        experimental_systematics = json.load(open(experimental_systematics_path))
        append_ATLAS_label(experimental_systematics)
    else:
        # extract systematics information from database
        from pdb import set_trace
        set_trace()
        experimental_systematics = extract_experimental_systematics(exp_syst_path)
    spurious_signal_systematics = extract_spurious_signal_systematics(spurious_signal_path)
    theory_systematics = extract_theory_systematics()
    systematics = merge_systematics([experimental_systematics, theory_systematics])
    
    # extract yield, efficiency and signal model_parameters from user input
    yields = yaml.safe_load(open(yields_path))
    efficiency = yaml.safe_load(open(efficiency_path))
    model_parameters = json.load(open(model_parameters_path))
    
    input_outdir = os.path.join(outdir, "config")
    category_outdir = os.path.join(outdir, "config", "categories")
    model_outdir = os.path.join(outdir, "config", "models")
    makedirs([outdir, input_outdir, category_outdir, model_outdir])
    
    processes = list(process_map['resonant'].keys())
    for mass in mass_points:
        mass_str = mass.replace('.', 'p')
        xml = create_category_xml(yields[mass_str], efficiency[mass_str], systematics, 
                                  processes, mass=mass_str,
                                  spurious_signal=spurious_signal_systematics[mass_str],
                                  br="0.002637286", background="10")
        xml.save(os.path.join(category_outdir, 'category_{}.xml'.format(mass_str)))
        model_xmls = create_model_xml(model_parameters[mass_str], systematics, processes)
        for key in model_xmls:
            model_xmls[key].save(os.path.join(model_outdir, "{}_{}.xml".format(key, mass_str)))
        input_xml = create_input_xml(mass_str, processes)
        input_xml.save(os.path.join(input_outdir, "input_{}.xml".format(mass_str)))