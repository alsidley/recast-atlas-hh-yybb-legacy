import os
import re
import glob
import yaml
import json
import shutil
from contextlib import contextmanager

import click
import numpy as np

import ROOT

POI = {'resonant': [('mu_XS_HH_res','')],
       'non-resonant': [('mu_XS_HH','')]}
WS_NAME_EXPRESSION = {'resonant': 'WS-yybb-resonant_(.+?).root',
                      'non-resonant': 'WS-yybb-non-resonant_kl(.+?).root'}
process_map = {
    "resonant":{
        "HH_ggF_resonant": ["HH_ggF_resonant"],
        "HH_ggF_non_resonant": ["HH_ggF_non_resonant"],
        "HH_VBF_non_resonant": ["HH_VBF_non_resonant"],
        "VBF": ["VBF"],
        "ZH": ["ZH", "ggZH"],
        "ttH": ["ttH"],
        "ggH": ["ggH"],
        "bbH": ["bbH"],
        "WH": ["WmH", "WpH"],
        "tHjb": ["tHjb"],
        "tWH": ["tWH"]
    },
    "non-resonant":{
        "HH_ggF_kl1p0": ["HH_ggF"],
        "HH_VBF_kl1p0cvv1": ["HH_VBF"],
        "VBF": ["VBF"],
        "ZH": ["ZH", "ggZH"],
        "ttH": ["ttH"],
        "ggH": ["ggH", "bbH"],
        "WH": ["WmH", "WpH"],
        "tHjb": ["tHjb"],
        "tWH": ["tWH"]
    }
}

mass_points = ['251', '260', '270', '280', '290', '300', '312.5', '325', '337.5', '350', '375', '400', '425',
               '450', '475', '500', '550', '600', '700', '800', '900', '1000']
mass_points_old = ['251', '260', '270', '280', '290', '300', '3125', '325', '3375', '350', '375', '400', '425',
                   '450', '475', '500', '550', '600', '700', '800', '900', '1000']
mass_points_str = ['251', '260', '270', '280', '290', '300', '312p5', '325', '337p5', '350', '375', '400', '425',
                   '450', '475', '500', '550', '600', '700', '800', '900', '1000']
category_map = {
    "resonant":{
        "251": "251",
        "260": "260",
        "270": "270",
        "280": "280",
        "290": "290",
        "300": "300",
        "312p5": "3125",
        "325": "325",
        "337p5": "3375",
        "350": "350",
        "375": "375",
        "400": "400",
        "425": "425",
        "450": "450",
        "475": "475",
        "500": "500",
        "550": "550",
        "600": "600",
        "700": "700",
        "800": "800",
        "900": "900",
        "1000": "1000",
    },
    "non-resonant":{
        "SM_1": "tightScore_HMass",
        "SM_2": "looseScore_HMass",
        "BSM_1": "tightScore_LMass",
        "BSM_2": "looseScore_LMass"
    }
}

def str_encode_kl(kl_value):
    kl_str = '%.1f' % kl_value
    kl_str = kl_str.replace('.', 'p').replace('-', 'n')
    return kl_str

def str_decode_kl(kl_str): 
    kl_value = float(kl_str.replace('p','.').replace('n','-'))
    return kl_value

def get_n_digit(str_val):
    if "." in str_val:
        return len(str_val.split(".")[1])
    else:
        return -len(str_val)
    
def to_pc_str(str_val):
    n_digit = get_n_digit(str_val)
    n_digit = max(n_digit+2, 0)
    return "{{:.{}f}}".format(n_digit).format(float(str_val)/100.)

def makedirs(paths):
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)


def get_limit(fname):
    f = ROOT.TFile(fname)
    if not f:
        raise FileNotFoundError('root file {} not found'.format(fname))
    h = f.Get('limit')
    if not h:
        raise ValueError('histogram "limit" not found')
    limits = {
        '-2sigma': h.GetBinContent(6),
        '-1sigma': h.GetBinContent(5),
        'observed': h.GetBinContent(1),
        'expected': h.GetBinContent(2),
        '+1sigma': h.GetBinContent(4),
        '+2sigma': h.GetBinContent(3),
    }
    return limits

@click.command(name='evaluate_limit')
@click.argument('ws_files', nargs=-1)
@click.option('-o', '--outdir', default="limits", help='Directory where outputs are saved')
@click.option('-p', '--poi', default=None, help='Specify POIs to be used in fit')
@click.option('-d', '--dataset', default='combData',  help='Name of the dataset')
@click.option('-w', '--workspace', default='combWS', help='Name of workspace')
@click.option('-m', '--model_config', default='ModelConfig', help='Name of model config')
@click.option('--rerun/--no-rerun', default=False, help='Whether to rerun existing limits')
@click.option('--resonant/--non-resonant', default=False, help='resonant or non-resonant analysis')
@click.option('-e', '--expr', default=None, help='workspace file name expression to search for')
@click.option('-n', '--name', default='limit_{expr}.root', help='Name of output. '
              'Keywords {expr} will be replaced by the {prefix}_{expr}.root for that workspace file')
def evaluate_limit(ws_files, outdir, poi, dataset, workspace, model_config, rerun, resonant, expr, name, **kwargs):
    """Tool for evaluating limit from workspaces """
    resonant_type = 'resonant' if resonant else 'non-resonant'
    expr_strings = []
    limits = {}
    if poi is None:
        poi_option = ','.join(['{}{}{}'.format(k,'' if not v else '=',v) for k,v in POI[resonant_type]])
    else:
        poi_option = poi
    limit_options = {
        'd': dataset,
        'w': workspace,
        'm': model_config,
        'p': poi_option
    }
    limit_options.update(kwargs)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    if expr is None:
        expr = WS_NAME_EXPRESSION[resonant_type]
    for ws_file in ws_files:
        if not expr:
            expr_str = os.path.basename(ws_file).replace('.root','')
        else:
            expr_match = re.search(expr, ws_file)
            if not expr_match:
                print('WARNING: Cannot extract expression from the workspace file name: {}'.format(ws_file))
                continue
            expr_str = expr_match.group(1)
        expr_strings.append(expr_str)
        outname = os.path.join(outdir, name.format(expr=expr_str))
        if os.path.exists(outname) and not rerun:
            continue
        limit_options['f'] = ws_file
        limit_options['o'] = outname
        command = 'quickLimit ' + ' '.join(['-{} {}'.format(k,v) for k,v in limit_options.items()])
        print(command)
        os.system(command)
    
    for expr_str in expr_strings:
        outname = os.path.join(outdir, name.format(expr=expr_str))
        if not os.path.exists(outname):
            try:
                FileNotFoundError
            except NameError:
                FileNotFoundError = IOError
            raise FileNotFoundError('Limit file {} does not exist'.format(outname))        
        limits[expr_str] = get_limit(outname)
    out_path = os.path.join(outdir, 'limits.json')
    if os.path.exists(out_path):
        with open(out_path, 'r') as f:
            old_limits = json.load(f)
        #limits = {**old_limits, **limits} # for python3
        old_limits.update(limits)
        limits = old_limits
    with open(out_path, 'w') as f:
        json.dump(limits, f, indent=2)

@click.command(name='limit_scan')
@click.option('-i', '--infile', required=True, help='Input workspace file')
@click.option('--min', 'vmin', type=float, default=-10, help='minimum value of kappa lambda')
@click.option('--max', 'vmax', type=float, default=10, help='maximum value of kappa lambda')
@click.option('--interval', 'interval', type=float, default=0.2, help='interval of kappa lambda scan')
@click.option('-o', '--outdir', default="limits", help='Directory where outputs are saved')
@click.option('-p', '--poi', default=None, help='Specify POIs to be used in fit')
@click.option('-s', '--scan_param', default='klambda', help='Parameter to scan')
@click.option('-d', '--dataset', default='combData',  help='Name of the dataset')
@click.option('-w', '--workspace', default='combWS', help='Name of workspace')
@click.option('-m', '--model_config', default='ModelConfig', help='Name of model config')
@click.option('--resonant/--non-resonant', default=False, help='resonant or non-resonant analysis')
@click.option('--rerun/--no-rerun', default=False, help='Whether to rerun existing limits')
@click.option('-n', '--name', default='limit_yybb_BDT_kl{value}.root', help='Name of output. '
              'Keywords {value} will be replaced by the scan parameter value for that workspace file')
def limit_scan(infile, vmin, vmax, interval, outdir, poi, scan_param, dataset, workspace, 
               model_config, resonant, rerun, name, **kwargs):
    """Tool for performing kappa lambda limit scan"""
    resonant_type = 'resonant' if resonant else 'non-resonant'
    limits = {}
    limit_options = {
        'f': infile,
        'd': dataset,
        'w': workspace,
        'm': model_config,
    }
    limit_options.update(kwargs)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    scan_values = np.linspace(vmin, vmax, int((vmax-vmin)/interval) +1)
    for scan_value in scan_values:
        value_str = str_encode_kl(scan_value)
        outname = os.path.join(outdir, name.format(value=value_str))
        if os.path.exists(outname) and not rerun:
            continue
        limit_options['o'] = outname 
        if poi is None:
            poi_option = ','.join(['{}{}{}'.format(k,'' if not v else '=',v) for k,v in POI[resonant_type]])
        else:
            poi_option = poi
        poi_option += (',' + '{}={}'.format(scan_param, scan_value))
        limit_options['p'] = poi_option
        command = 'quickLimit ' + ' '.join(['-{} {}'.format(k,v) for k,v in limit_options.items()])
        print(command)
        os.system(command)
    
    for scan_value in scan_values:
        value_str = str_encode_kl(scan_value)
        outname = os.path.join(outdir, name.format(value=value_str))
        if not os.path.exists(outname):
            try:
                FileNotFoundError
            except NameError:
                FileNotFoundError = IOError
            raise FileNotFoundError('Limit file {} does not exist'.format(outname))        
        limits[value_str] = get_limit(outname)
    out_path = os.path.join(outdir, 'limits.json')
    if os.path.exists(out_path):
        with open(out_path, 'r') as f:
            old_limits = json.load(f)
        #limits = {**old_limits, **limits} # for python3
        old_limits.update(limits)
        limits = old_limits
    with open(out_path, 'w') as f:
        json.dump(limits, f, indent=2)

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)
        
@click.command(name='create_workspace')
@click.option('-b', '--base_path', required=True, help='base path containing the "config" subdirectory where input xmls are stored') 
def create_workspace(base_path):
    """Tool for creating workspaces from input xmls"""
    with cd(base_path):
        input_xmls = glob.glob(os.path.join('config', 'input_*.xml'))
        for input_xml in input_xmls:
            print('INFO: Creating workspace for %s'%(input_xml))
            command = 'XMLReader -x %s' % input_xml
            os.system(command)        
# reference https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWGHH?redirectedfrom=LHCPhysics.LHCHXSWGHH
def reweight_yybb_ggF(kl):
    return (70.3874-50.4111*kl+11.0595*kl**2)/1000 #XS in pb

# reference https://indico.cern.ch/event/995807/contributions/4184798/attachments/2175756/3683303/VBFXSec.pdf
def reweight_yybb_VBF(kl):
    return (4.581-4.245*kl+1.359*kl**2)/1000 #XS in pb

def extract_95pc_CL(limit_band, kl_values):
    limit = limit_band - 1
    idx = np.argwhere(np.diff(np.sign(limit))).flatten()
    intersections = [kl_values[i] - (kl_values[i+1] - kl_values[i])/(limit[i+1] - limit[i]) * limit[i] for i in idx]
    return intersections

#Now using values from LHCWHGHHHXGGBGGGXXX
SCALE_GGF = 31.02/31.0358   #correct to xs at mH = 125.09 
SCALE_VBF = 1.723/(4.581-4.245+1.359)

def xs_ggF(kl):
    #https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWGHH?redirectedfrom=LHCPhysics.LHCHXSWGHH#Latest_recommendations_for_gluon
    return (70.3874-50.4111*kl+11.0595*kl**2) * SCALE_GGF #XS in fb

def xs_VBF(kl):
    #https://indico.cern.ch/event/995807/contributions/4184798/attachments/2175756/3683303/VBFXSec.pdf
    return (4.581-4.245*kl+1.359*kl**2) * SCALE_VBF

def xs_HH(kl):
    return xs_ggF(kl) + xs_VBF(kl)

    
@click.command(name='print_limit')    
@click.option('-i', '--infile', required=True, help='path to json file containing kappa lambda limits') 
@click.option('-o', '--outfile', default='limit_summary.json', help='name of outfile summary file') 
def print_limit(infile, outfile):
    """ Display limit summary """
    import pandas as pd
    with open(infile, 'r') as limit_file:
        data = json.load(limit_file)
    limits = {str_decode_kl(kl):data[kl] for kl in data}
    kl_values = sorted(list(limits.keys()))
    df = pd.DataFrame(limits).transpose().sort_index()
    limit_bands = {index: df[index].values for index in df}
    n = np.array([xs_HH(kl) for kl in kl_values]) 
    expected_95pc_CL = extract_95pc_CL(limit_bands['expected'], kl_values)
    observed_95pc_CL = extract_95pc_CL(limit_bands['observed'], kl_values)
    expected_limit = limits[1.0]['expected']
    observed_limit = limits[1.0]['observed']
    results = {
        'expected_limit_on_kl': expected_95pc_CL,
        'observed_limit_on_kl': observed_95pc_CL,
        'expected_limit_nominal_kl': expected_limit,
        'observed_limit_nominal_kl': observed_limit
    }
    with open(outfile, 'w') as output:
        json.dump(results, output, indent=2)
    print('-'*70)
    print(' '*25 + 'Summary of Results'+' '*25)
    print('-'*70)
    print('\nConstrain on kappa lambda:')
    print('  expected: {}'.format(expected_95pc_CL))
    print('  observed: {}'.format(observed_95pc_CL))
    print('\nSM Limit:')
    print('  expected: {}'.format(expected_limit))
    print('  observed: {}'.format(observed_limit))
'''  
@click.command(name='export_variable')    
@click.option('-i', '--infile', required=True, help='input root file for exporting variable')
@click.option('-t', '--tree', default='CollectionTree', help='tree name')
@click.option('-v', '--variable', default='m_yy', help='variable name')
@click.option('-o', '--outfile', default='output.txt', help='output file name')
@click.option('-f', '--fmt', default='%1.3f', help='output format')
'''
def export_variable(infile, tree, variable, outfile, fmt):
    """ Export variable data points as txt file """
    import uproot
    file = uproot.open(infile)
    x = file[tree][variable].array()
    # to account for python2 version of numpy requires byte formats
    binary_fmt = fmt.encode('ascii')
    np.savetxt(outfile, x, fmt=binary_fmt)
    print('INFO: Exported N-tuples for variable {} to {}'.format(variable, outfile))
    
@click.command(name='dump_mass_points')    
@click.option('-i', '--indir', required=True, help='input directory containing data minitrees containing the myy variable')
@click.option('-t', '--tree', default='CollectionTree', help='tree name')
@click.option('--resonant/--non-resonant', required=True, help='resonant or non-resonant analysis')
@click.option('-o', '--outname', default='mass_points_data_{category}.txt', help='name format of output file')
@click.option('-f', '--fmt', default='%1.3f', help='output format')
def dump_mass_points(indir, tree, resonant, outname, fmt):
    """ Export myy data points as txt file """
    resonant_type = "resonant" if resonant else "non-resonant"
    categories = category_map[resonant_type]
    for category in categories:
        file_name = os.path.join(indir, "data_{}_tree.root".format(categories[category]))
        if not os.path.exists(file_name):
            raise ValueError("Data minitree file {} does not exist".format(file_name))
        outfile = outname.format(category=category)
        export_variable(file_name, tree, "m_yy", outfile, fmt)  

@click.command(name='copy_dtd')    
@click.option('-d', '--destination', required=True, help='base path of workspaces and xml cards')
@click.option('-s', '--source', required=True, help='source ')
def copy_dtd(destination, source):
    """ Copy dtd file to destinations that require document type definition for xml cards"""
    input_xml_path    = os.path.join(destination, 'config')
    model_xml_path    = os.path.join(input_xml_path, 'models')
    category_xml_path = os.path.join(input_xml_path, 'categories')
    syst_xml_path     = os.path.join(input_xml_path, 'systs')
    dtd_name = os.path.basename(source)
    if not dtd_name.endswith('.dtd'):
        raise ValueError('{} is not a dtd file'.format(dtd_name))
    shutil.copy(source, input_xml_path)
    print('INFO: Copied {} to {}'.format(dtd_name, input_xml_path))
    shutil.copy(source, model_xml_path)
    print('INFO: Copied {} to {}'.format(dtd_name, model_xml_path))    
    shutil.copy(source, category_xml_path)
    print('INFO: Copied {} to {}'.format(dtd_name, category_xml_path))   
    if os.path.exists(syst_xml_path):
        shutil.copy(source, syst_xml_path)
        print('INFO: Copied {} to {}'.format(dtd_name, syst_xml_path))    

def combine_yield_inputs(inputs):
    yields = {}
    for fname in inputs:       
        with open(fname, 'r') as f:
            data = json.load(f)
        for process in data:
            if process not in yields:
                yields[process] = {}
            for category in data[process]:
                yields[process][category] = data[process][category]
    return yields
    
@click.command(name='transform_yield')    
@click.argument('inputs', nargs=-1)
@click.option('-d', '--data_dir', default=None, help='directory containing the data n-tuples')
@click.option('--resonant/--non-resonant', required=True, help='resonant or non-resonant analysis')
@click.option('-y', '--out_yield', default='yields.yaml', help='output file name for yields')
@click.option('-e', '--out_efficiency', default='efficiency.yaml', help='output file name for efficiency')
def transform_yield(inputs, data_dir, resonant, out_yield, out_efficiency):
    """ Transform the yield format from analysis framework and include data yields """
    import uproot
    resonant_type = "resonant" if resonant else "non-resonant"
    # load data mass points
    data = {}
    if data_dir is not None:
        for category in category_map[resonant_type]:
            data_path = os.path.join(data_dir, 'data_{}_tree.root'.format(category_map[resonant_type][category]))
            if not os.path.exists(data_path):
                raise ValueError("File {} does not exist".format(data_path))
            file = uproot.open(data_path)
            data[category] = float(len(file['CollectionTree']['m_yy'].array()))
        
    yields = combine_yield_inputs(list(inputs))
    new_yields = {}
    new_efficiency = {}
    for category in category_map[resonant_type]:
        new_yields[category] = {}
        new_efficiency[category] = {}
        cat = category_map[resonant_type][category]
        for process in process_map[resonant_type]:
            new_yields[category][process] = 0.
            new_efficiency[category][process] = 0.
            for sub_process in process_map[resonant_type][process]:
                new_yields[category][process] += yields[sub_process][cat][0]
                new_efficiency[category][process] += yields[sub_process][cat][2]
        if data:
            new_yields[category]['data'] = data[category]
    with open(out_yield, 'w') as output:
        yaml.dump(new_yields, output, indent=2)
    print('INFO: Process yield saved to {}'.format(out_yield))
    with open(out_efficiency, 'w') as output:
        yaml.dump(new_efficiency, output, indent=2)    
    print('INFO: Process efficiency saved to {}'.format(out_efficiency))
    
# helper function in order to resolve path starting with root://
def list_files(dir_name, ext=".root"):
    is_dir = ROOT.TSystemFile(dir_name, "").IsDirectory()
    if not is_dir:
        return None
    sys_dir = ROOT.TSystemDirectory(dir_name, dir_name)
    files = [f.GetName() for f in sys_dir.GetListOfFiles()]
    files = [f for f in files if f.endswith(ext)]
    return files

def _extract_efficiency(fname):
    files = list_files(fname)
    if files is None:
        f = ROOT.TFile(fname)
    else:
        f = ROOT.TFile(os.path.join(fname, files[0]))
    if "CollectionTree" not in f.GetListOfKeys():
        raise ValueError("The MxAOD `{}` does not contain the tree `CollectionTree`. Please double check.".format(fname))
    t = f.Get("CollectionTree")
    if "HGamEventInfoAuxDyn.crossSectionBRfilterEff" not in t.GetListOfBranches():
        raise ValueError("The MxAOD `{}` does not contain the branch `HGamEventInfoAuxDyn.crossSectionBRfilterEff`."
                         "Please double check.".format(fname))
    t.Draw("HGamEventInfoAuxDyn.crossSectionBRfilterEff")   
    efficiency = t.GetVal(0)[0]
    return efficiency
    
    
    
@click.command(name='extract_efficiency')     
@click.option('-i', '--input_config', required=True, help='configuration file containing path information of MxAODs')
@click.option('-o', '--outfile', default="efficiency.json", help='output file name containing the efficiency information')
def extract_efficiency(input_config, outfile):
    """ Tool for extracting efficiency information from yield file created by YieldIterator from analysis framework """
    config = json.load(open(input_config))
    luminosity = config['lumi']
    directories = config['directories']
    samples = config['samples']
    result = {}
    for process in samples:
        efficiency = 0.
        mxaod_path = os.path.join(directories['mc16a'], samples[process]["datafiles"]['mc16a'])
        eff = _extract_efficiency(mxaod_path)
        for mc_campaign in luminosity:
            efficiency += luminosity[mc_campaign]*eff
        result[process] = efficiency
    with open(outfile, 'w') as output:
        json.dump(result, output, indent=2)
        print('INFO: Efficiency information saved to "{}"'.format(outfile))

@click.command(name='hadd_minitrees')
@click.option('-i', '--indir', required=True, help='input directory containing minitrees to be hadded')
@click.option('-p', '--process', default="HH_ggF,HH_VBF", help='process minitrees to hadd, separated by commas')
@click.option('-n', '--name', default="HH", help='name prefix of hadded minitrees')
@click.option('--hist/--no-hist', default=True, help='also hadd histograms')
@click.option('-o', '--outdir', default=None, help='output directory for the hadded minitrees, defaults to the input directory')
@click.option('--resonant/--non-resonant', default=False, help='resonant or non-resonant analysis')
def hadd_minitrees(indir, process, name, hist, outdir, resonant):
    """ Tool for combining minitrees from multiple processes """
    resonant_type = "resonant" if resonant else "non-resonant"
    categories = category_map[resonant_type]
    processes = [p.strip() for p in process.split(',')]
    if outdir is None:
        outdir = indir
    for category in categories:
        old_cat = categories[category]
        outname = os.path.join(outdir, "{}_{}_tree.root".format(name, old_cat))
        minitrees_path = [os.path.join(indir, "{}_{}_tree.root".format(proc, old_cat)) for proc in processes]
        for path in minitrees_path:
            if not os.path.exists(path):
                raise ValueError("minitree file {} does not exist".format(path))
        minitrees_to_merge = ' '.join(minitrees_path)
        os.system('hadd -f {} {}'.format(outname, minitrees_to_merge))
        if hist:
            hist_path = [os.path.join(indir, "{}_{}.root".format(proc, old_cat)) for proc in processes]
            for path in hist_path:
                if not os.path.exists(path):
                    raise ValueError("hist file {} does not exist".format(path))
            hist_to_merge = ' '.join(hist_path)
            outname = os.path.join(outdir, "{}_{}.root".format(name, old_cat))
            os.system('hadd -f {} {}'.format(outname, hist_to_merge))
            
            
            
@click.command(name='create_xml')
@click.option('-y', '--yield_path', required=True, help='path to yaml file containing process yields')
@click.option('-d', '--database_path', required=True, help='path to database where systematics information are stored')
@click.option('-m', '--model_param_path', required=True, help='path to json file containing signal model parameters')
@click.option('-e', '--efficiency_path', default=None, help='path to yaml file containing process efficiency')
@click.option('--exp_syst_path', default=None, help='path to json file containing experimental systematics')
@click.option('--resonant/--non-resonant', required=True, help='resonant or non-resonant analysis')
@click.option('-o', '--outdir', default="yybb_BDT", help='output directory where xml files are saved')
def create_xml(yield_path, database_path, model_param_path, efficiency_path, resonant, 
               exp_syst_path, outdir):
    """ Tool for creating xml files for making workspaces """
    if resonant:
        from resonant_xml_maker import create_res_xmls
        create_res_xmls(yield_path, efficiency_path, model_param_path, database_path, 
                        experimental_systematics_path=exp_syst_path, outdir=outdir)      
    else:
        from non_resonant_xml_maker import create_non_res_xmls
        create_non_res_xmls(yield_path, model_param_path, database_path, outdir)  