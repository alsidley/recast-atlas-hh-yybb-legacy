import os
import re
import json
from functools import partial

import ROOT

from recast_systematics.components.shape_systematics import ShapeSystematics
from recast_systematics.components.yield_systematics import YieldSystematics
from recast_systematics.utils.common import execute_multi_tasks

class SystematicsAnalysis:
    
    SYST_TYPES   = ["yield", "shape"]
    
    FILE_NAMES = {
        "individual_detailed"  : "{syst_type}_systematics_detailed.json",
        "individual_processed" : "{syst_type}_systematics_processed.json",
        "individual_summary"   : "{syst_type}_systematics.json",
        "combined_summary"     : "systematics.json",
        "input_tree": "{campaign}_{process}_{syst_theme}.root"
    }
    
    SCHEMES = {
        'error_method': ['analytic', 'bootstrap'],
        'shape_syst_method': ["fit", "mean_IQR", "mean_interquartile"],
        "partition_method": ["individual", "bootstrap", "jacknife"]
    }
    
    _NON_RES_CATEGORIES_ = ["XGBoost_btag77_withTop_BCal_tightScore_HMass",
                            "XGBoost_btag77_withTop_BCal_looseScore_HMass",
                            "XGBoost_btag77_withTop_BCal_tightScore_LMass",
                            "XGBoost_btag77_withTop_BCal_looseScore_LMass"]
    _RES_MASS_ = [251, 260, 280, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
    _RES_CATEGORIES_ = ["Resonant_mX{}".format(m) for m in _RES_MASS_]
    
    _ALL_CATEGORIES_ = _NON_RES_CATEGORIES_ + _RES_CATEGORIES_    

    @property
    def syst_types(self):
        return self._syst_types
    
    @syst_types.setter
    def syst_types(self, vals):
        if vals is None:
            self._syst_types = self.SYST_TYPES
        else:
            for v in vals:
                if v not in self.SYST_TYPES:
                    raise ValueError("unknown systematics type: {}".format(v))
            self._syst_types = vals
    
    @property
    def processes(self):
        return self._processes
    
    @processes.setter
    def processes(self, vals):
        if vals is None:
            self._processes = self.all_processes
        elif isinstance(vals, (str, list)):
            if isinstance(vals, str):
                vals = vals.split(',')
            for proc in vals:
                if proc not in self.all_processes:
                    raise ValueError('process `{}` not found in systematics config'.format(proc))
            self._processes = vals
        else:
            raise ValuError("unknown input`{}`".format(vals))
    
    
    @property
    def syst_themes(self):
        return self._syst_themes
    
    @syst_themes.setter
    def syst_themes(self, vals):
        if vals is None:
            self._syst_themes = self.all_systematics_themes
        elif isinstance(vals, (str, list)):
            if isinstance(vals, str):
                vals = vals.split(',')
            for theme in vals:
                if theme not in self.all_systematics_themes:
                    raise ValueError('systematics theme `{}` not found in systematics config'.format(proc))
            self._syst_themes = vals
        else:
            raise ValuError("unknown input`{}`".format(vals)) 
            
    def __init__(self, base_path, syst_config_file="systematics_config.json",
                 syst_types=None, processes=None, syst_themes=None, categories=None,
                 campaign="h026_mc16a_h026_mc16d_h026_mc16e",
                 verbose=False, **kwargs):
        self.campaign = campaign
        self.syst_types = syst_types
        self.base_path = base_path
        self.verbose = verbose
        if not os.path.exists(syst_config_file):
            raise RuntimeError('systematics configuration file `{}` does not exist'.format(syst_config_file))
        self.syst_config = json.load(open(syst_config_file))
        self.all_processes = sorted(list(self.syst_config["processes"]))
        self.all_systematics_themes = sorted(list(self.syst_config["systematics"]))
        self.systematics_by_theme = self.syst_config["systematics"]
        self.process_map = self.syst_config["processes"]
        self.discard_data = self.syst_config.get("discard_data", None)
        self.merge_condition = self.syst_config["merge"]
        self.processes = processes
        self.syst_themes = syst_themes
        self.categories = categories
        self.setup_root_env()
        self.init_configs()
        self.configure(**kwargs)
        self.yield_syst_module = YieldSystematics(**self.config)
        self.shape_syst_module = ShapeSystematics(**self.config, bootstrap_outdir=base_path)

    def init_configs(self):
        self.config = {
            "observable" : "m_yy",
            "save_toys": True,
            "parallel": -1,
            "cache": True,
            "prune_significative": True,
            "prune_threshold": 0.1,
            "n_toys" : 100,
            "error_method": "analytic",
            "syst_method": "mean_IQR",
            "partition_method": "bootstrap"
        }
        
    def configure(self, **kwargs):
        self.config.update(kwargs)
        
    def setup_root_env(self):
        #ROOT.gStyle.SetOptStat(0)
        #ROOT.gStyle.SetOptTitle(0)
        ROOT.TH1.SetDefaultSumw2(1)
        if not self.verbose:
            ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.INFO)
            ROOT.RooMsgService.instance().setSilentMode(1)
            ROOT.RooMsgService.instance().setStreamStatus(1, False)
    
    @staticmethod
    def apply_filter(source, reference=None):
        if reference is None:
            return source
        return [s for s in source if s in reference]
        
    @classmethod
    def get_categories(cls, process):
        match = re.search('X(\d+)tohh_bbyy_AF2', process)
        if match:
            mass = match.group(1)
            categories = ["Resonant_mX{}".format(mass)]
        else:
            categories = cls._ALL_CATEGORIES_
        return categories

            
    def get_input_tree_paths(self):
        tree_paths = {}
        for key in self.processes:
            process_name = self.process_map[key]
            tree_paths[process_name] = {}
            for syst_theme in self.syst_themes:
                if syst_theme not in self.syst_config["inputs"][key]:
                    continue
                dirname = os.path.join(self.base_path, "trees", syst_theme)
                basename = self.FILE_NAMES["input_tree"].format(campaign=self.campaign,
                                                                process=process_name,
                                                                syst_theme=syst_theme)
                tree_path = os.path.join(dirname, basename)
                if not os.path.exists(tree_path):
                    print("WARNING: Input tree `{}` does not exist. The corresponding systematics for " 
                          "the process `{}` and theme `{}` will not be evaluated".format(tree_path, process_name, syst_theme))
                    continue
                tree_paths[process_name][syst_theme] = tree_path
        return tree_paths
    
    def evaluate_yield_systematics(self):
        tree_paths = self.get_input_tree_paths()
        executor = self.yield_syst_module.get_systematics_from_root_file
        for process in tree_paths:
            categories = self.get_categories(process)
            for syst_theme in tree_paths[process]:
                syst_list = self.systematics_by_theme[syst_theme]
                fname = tree_paths[process][syst_theme]
                executor(fname, categories, syst_list, process, append=True)
                
    def create_bootstrap_trees(self):
        tree_paths = self.get_input_tree_paths()
        cache = self.config['cache']
        parallel = self.config['parallel']
        executor = partial(self.shape_syst_module.create_bootstrap_trees_from_root_file, cache=cache)
        args_fnames = []
        args_categories = []
        args_syst_list = []
        args_processes = []
        for process in tree_paths:
            categories = self.get_categories(process)
            for syst_theme in tree_paths[process]:
                syst_list = self.systematics_by_theme[syst_theme]
                fname = tree_paths[process][syst_theme]
                args_fnames.append(fname)
                args_categories.append(categories)
                args_syst_list.append(syst_list)
                args_processes.append(process)
        task_args = (args_fnames, args_categories, args_syst_list, args_processes)
        execute_multi_tasks(executor, *task_args, parallel=parallel)
                
    def evaluate_shape_systematics(self):
        parallel = self.config['parallel']
        save_toys = self.config['save_toys']
        cache = self.config['cache']
        executor = partial(self.shape_syst_module.get_bootstrap_systematics,
                           parallel=parallel, cache=cache, save_toys=save_toys)
        for key in self.processes:
            process_name = self.process_map[key]
            categories = self.get_categories(process_name)
            systematics_list = []
            for syst_theme in self.syst_themes:
                if syst_theme not in self.syst_config["inputs"][key]:
                    continue
                systematics_list += self.systematics_by_theme[syst_theme]
            executor(process_name, categories, systematics_list)
            
    def post_process(self, syst_types=None):
        modules = {"yield": self.yield_syst_module, "shape": self.shape_syst_module}
        if syst_types is None:
            syst_types = self.syst_types
        for syst_type in syst_types:
            # discard certain unwanted systematics by pattern
            if self.discard_data is not None:
                for item in self.discard_data:
                    print("INFO: Discarding `{}` from {} systematics".format(item, syst_type))
                    modules[syst_type].discard(**self.discard_data[item])
            print("INFO: Post-processing {} systematics results".format(syst_type))
            modules[syst_type].post_process(merge_condition=self.merge_condition)
            
    def load_result(self, syst_types=None, mode="processed"):
        modules = {"yield": self.yield_syst_module, "shape": self.shape_syst_module}
        if syst_types is None:
            syst_types = self.syst_types
        for syst_type in syst_types:
            dirname = os.path.join(self.base_path, "results")
            if mode == "detailed":
                basename = self.FILE_NAMES['individual_detailed'].format(syst_type=syst_type)
            elif mode == "processed":
                basename = self.FILE_NAMES['individual_processed'].format(syst_type=syst_type)
            else:
                raise ValueError("unknown mode: {}".format(mode))
            fname = os.path.join(dirname, basename)
            if os.path.exists(fname):
                modules[syst_type].load(fname)
                print("INFO: Loaded results for {} systematics from `{}`".format(syst_type, fname))
                modules[syst_type].processed = (mode == "processed")
            else:
                print("ERROR: No savaed results found for {} systematics (in `{}`)".format(syst_type, fname))
                
    def save_result(self, syst_types=None):
        modules = {"yield": self.yield_syst_module, "shape": self.shape_syst_module}
        if syst_types is None:
            syst_types = self.syst_types
        for syst_type in syst_types:
            dirname = os.path.join(self.base_path, "results")
            if modules[syst_type].processed:
                basename = self.FILE_NAMES['individual_processed'].format(syst_type=syst_type)
            else:
                basename = self.FILE_NAMES['individual_detailed'].format(syst_type=syst_type)
            fname = os.path.join(dirname, basename)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            modules[syst_type].save(fname)
            print("INFO: Saved results for {} systematics in `{}`".format(syst_type, fname))
            
    def get_combined_summary(self):
        modules = {"yield": self.yield_syst_module, "shape": self.shape_syst_module}
        combined_result = {}
        for syst_type in self.syst_types:
            if modules[syst_type].dataframe is None:
                continue
            simplified_data = modules[syst_type].get_simplified_data()
            combined_result.update(simplified_data)
        return combined_result
    
    def save_combined_summary(self):
        combined_result = self.get_combined_summary()
        dirname = os.path.join(self.base_path, "results")
        basename = self.FILE_NAMES['combined_summary']
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        fname = os.path.join(dirname, basename)
        with open(fname, 'w') as f:
            json.dump(combined_result, f, indent=2)
        print("INFO: Saved combined summary in `{}`".format(fname))
            
    def get_dataframe(self, syst_type, simplified=True):
        if syst_type == "yield":
            df = self.yield_syst_module.get_valid_dataframe("yield")
        elif syst_type == "shape":
            df = self.shape_syst_module.dataframe
        elif syst_type == "position_shape":
            df = self.shape_syst_module.get_valid_dataframe("position_shape")
        elif syst_type == "spread_shape":
            df = self.shape_syst_module.get_valid_dataframe("spread_shape")
        else:
            raise ValueError("invalid systematics type: {}".format(syst_type))
        if simplified:
            columns = list(df.columns)
            to_drop = [i for i in columns if isinstance(i, tuple) and "rel_effect" not in i]
            if syst_type == "position_shape":
                to_drop += [i for i in columns if isinstance(i, tuple) and "position_shape" not in i]
            elif syst_type == "spread_shape":
                to_drop += [i for i in columns if isinstance(i, tuple) and "spread_shape" not in i]                
            df = df.drop(to_drop, axis=1)
        return df