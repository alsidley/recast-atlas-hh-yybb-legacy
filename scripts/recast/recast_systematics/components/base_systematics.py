import os
import json
from ast import literal_eval

import numpy as np
import pandas as pd

import ROOT

class BaseSystematics(object):
    
    OBJECT_PATTERNS = {
        "THEORY": ["muR", "renscfact", "facscfact", "lhapdf", "pdf_set", "QCD", "PDF_alpha_s"],
        "EVENT_BASED": ["PRW_DATASF", "PH_EFF_TRIGGER_Uncertainty"],
        "PHOTON": ["PH_EFF_", "EG_SCALE_", "EG_RESOLUTION_", "PH_SCALE_"],
        "JET": ["JET_"],
        "FLAVOR_TAGGING": ["FT_EFF"],
        "LEPTON_MET": ["EL_EFF", "MUON_SCALE", "MUON_EFF", "MUON_ID", "MUON_MS", "MUON_SAGITTA",
                       "TAUS_TRUE", "MET_Soft"]
    }
    
    THEME_PATTERNS = {
        "Theory": ["muR", "renscfact", "facscfact", "lhapdf", "pdf_set", "QCD", "PDF_alpha_s"],
        "PhotonSys": ["PRW_DATASF", "PH_EFF", "EG_SCALE_ALL", "EG_SCALE_AF2", "EG_RESOLUTION_ALL"],
        "JetSys3": ["MCsmear"],
        "JetSys4": ["PDsmear"],
        "JetSys1": ["JET_EffectiveNP"],
        "JetSys2": ["JET_EtaIntercalibration", "JET_Flavor", "JET_Pileup", "JET_PunchThrough", "JET_BJES",
                    "JET_RelativeNonClosure", "JET_SingleParticle", "JET_JvtEfficiency", "JET_fJvtEfficiency"],
        "FlavorSys": ["FT_EFF"],
        "LeptonMETSys": ["EL_EFF", "MUON_SCALE", "MUON_EFF", "MUON_ID", "MUON_MS", "MUON_SAGITTA",
                       "TAUS_TRUE", "MET_Soft"]
    }    
    
    INDICES = ['process', 'category', 'systematics']
    
    PRIMARY_KEYS = {'base': [('up', 'rel_effect'), ('down', 'rel_effect')]}
    AUXILIARY_KEYS = None
    
    def __init__(self, prune_threshold=0.1, prune_significative=True, *args, **kwargs):
        """
            prune_threshold: prune relative effect less than a certain percentage
        """
        self.prune_threshold = prune_threshold
        self.prune_significative = prune_significative
        self.reset()
        #df.reset_index(inplace=True)
        
    def reset(self):
        self.dataframe = None
        self.processed = False
        
    def append(self, data, columns=None):
        df = pd.DataFrame(data, columns=columns).set_index(self.INDICES)
        if self.dataframe is None:
            self.dataframe = df
        else:
            self.dataframe = self.dataframe.combine_first(df)
    
    @classmethod
    def _prune_systematics(cls, df, significative:bool=True, threshold:float=0):
        pruned_df = df.copy()
        for syst_type in cls.PRIMARY_KEYS:
            # initialize prune status if not exist
            if (syst_type, 'prune_status') not in df:
                pruned_df[(syst_type, 'prune_status')] = 0
            mask_significative = pd.Series(True, index=df.index)
            mask_threshold     = pd.Series(True, index=df.index)
            for syst_effect in cls.PRIMARY_KEYS[syst_type]:
                val_label = syst_effect + ('val',)
                err_label = syst_effect + ('err',)
                criteria = pd.Series(False, index=df.index)
                if significative:
                    criteria_significative = df[err_label] > np.abs(df[val_label])
                    mask_significative &= criteria_significative
                    criteria |= criteria_significative
                if threshold > 0:
                    criteria_threshold = threshold > np.abs(df[val_label])
                    mask_threshold &= criteria_threshold
                    criteria |= criteria_threshold
                pruned_df.loc[criteria, [val_label]] = 0
                pruned_df.loc[criteria, [err_label]] = 0
            # set prune status to non-zero value if both up and down variations got pruned
            if significative:
                pruned_df.loc[mask_significative, [(syst_type, 'prune_status')]] = pruned_df[(syst_type, 'prune_status')] + 1
            if threshold > 0:
                pruned_df.loc[mask_threshold, [(syst_type, 'prune_status')]] = pruned_df[(syst_type, 'prune_status')] + 2
        return pruned_df
            
    def prune_systematics(self, do_threshold=True):
        if do_threshold:
            self.dataframe = self._prune_systematics(self.dataframe, self.prune_significative, self.prune_threshold)
        else:
            self.dataframe = self._prune_systematics(self.dataframe, self.prune_significative, 0)
            
    def _merge_smear_systematics(self, df):
        systematics_values = df.index.get_level_values('systematics')
        systematics_str = systematics_values.str
        index_smear = systematics_str.endswith("PDsmear") | systematics_str.endswith("MCsmear")
        final_df = df[~index_smear]
        target_df = df[index_smear]
        target_df_by_systematics = {}
        for systematics, new_df in target_df.groupby("systematics"):
            target_df_by_systematics[systematics] = new_df
        all_systematics = list(systematics_values.unique())
        source_systematics = list(set([s.replace("_PDsmear","").replace("_MCsmear", "") for s in all_systematics]))
        for source in source_systematics:
            pd_smear_df = target_df_by_systematics.get("{}_PDsmear".format(source), None)
            mc_smear_df = target_df_by_systematics.get("{}_MCsmear".format(source), None)
            # make sure both PDsmear and MCsmear systematics exist
            if (pd_smear_df is None) or (mc_smear_df is None):
                continue
            # make sure the dataframes for PDsmear and MCsmear have correct pairing of indexes
            pd_smear_df = pd_smear_df.reset_index("systematics")
            mc_smear_df = mc_smear_df.reset_index("systematics")
            common_index = list(set(pd_smear_df.index).intersection(set(mc_smear_df.index)))
            pd_smear_df = pd_smear_df.loc[common_index]
            mc_smear_df = mc_smear_df.loc[common_index]
            # create dataframe for the combined systematics
            combined_df = pd_smear_df.copy()
            combined_df["systematics"] = combined_df["systematics"].str.replace("_PDsmear", "")
            combined_df["theme"] = combined_df["theme"].str.replace("JetSys4", "JetSys3+4")
            for syst_type in self.PRIMARY_KEYS:
                if (syst_type, "prune_status") in combined_df:
                    combined_df[(syst_type, "prune_status")] = 0
                for syst_effect in self.PRIMARY_KEYS[syst_type]:
                    syst_effect_val = syst_effect + ('val',)
                    syst_effect_err = syst_effect + ('err',)
                    combined_df[syst_effect_val] = mc_smear_df[syst_effect_val] - pd_smear_df[syst_effect_val]
                    combined_df[syst_effect_err] = np.sqrt(np.power(mc_smear_df[syst_effect_err],2) + \
                                                           np.power(pd_smear_df[syst_effect_err],2))
            combined_df.set_index("systematics", append=True, inplace=True)
            final_df = final_df.combine_first(combined_df)
        return final_df
    
    def merge_smear_systematics(self):
        self.dataframe = self._merge_smear_systematics(self.dataframe)
    
    @classmethod
    def merge_index_level_df_by_max_effect(cls, df, index_level_condition, target_index, ungrouped_index=None,
                                           ignore_pruned=True):
        if ungrouped_index is None:
            ungrouped_index = list(df.index.names)
            ungrouped_index.remove(target_index)
        merged_df = pd.DataFrame([], columns=list(df.index.names) + list(df.columns))
        merged_df.set_index(df.index.names, inplace=True)
        if len(ungrouped_index) > 0:
            current_index = ungrouped_index[-1]
            for index_value, new_df in df.groupby(current_index):
                intermediate_df = cls.merge_index_level_df_by_max_effect(new_df, 
                                  index_level_condition, target_index, ungrouped_index[:-1],
                                  ignore_pruned=ignore_pruned)
                merged_df = merged_df.combine_first(intermediate_df)
        else:
            label_loc = df.index.names.index(target_index)
            all_labels = df.index.get_level_values(target_index).unique()
            all_labels_to_merge = []
            for labels in index_level_condition.values():
                all_labels_to_merge += labels
            untouched_labels = list(set(all_labels) - set(all_labels_to_merge))
            untouched_df = df[df.index.get_level_values(target_index).isin(untouched_labels)]
            merged_df = merged_df.combine_first(untouched_df)
            all_series = []
            for merged_label, labels_to_merge in index_level_condition.items():
                # gather systematics that should be merged
                target_df = df[df.index.get_level_values(target_index).isin(labels_to_merge)]
                if len(target_df) == 0:
                    continue
                merged_series = target_df.iloc[0].copy()
                # set all systematics values to 0 by default
                for syst_type in cls.PRIMARY_KEYS:
                    for syst_effect in cls.PRIMARY_KEYS[syst_type]:
                        syst_effect_val = syst_effect + ('val',)
                        syst_effect_err = syst_effect + ('err',)                        
                        merged_series[syst_effect_val] = 0.
                        merged_series[syst_effect_err] = 0.
                # set all other auxiliary values to None by default
                if cls.AUXILIARY_KEYS is not None:
                    for syst_type in cls.AUXILIARY_KEYS:
                        for aux_keys in cls.AUXILIARY_KEYS[syst_type]:
                            for aux_key in aux_keys:
                                merged_series[aux_key] = None
                for syst_type in cls.PRIMARY_KEYS:
                    # filter out systematics that failed pruning
                    if ignore_pruned and (syst_type, "prune_status") in target_df:
                        good_df = target_df[target_df[(syst_type, "prune_status")] == 0]                     
                        if len(good_df) == 0:
                            continue
                        merged_series[(syst_type, "prune_status")] = 0
                    else:
                        good_df = target_df
                    for i, syst_effect in enumerate(cls.PRIMARY_KEYS[syst_type]):
                        syst_effect_val = syst_effect + ('val',)
                        syst_effect_err = syst_effect + ('err',)
                        idxmax = np.abs(good_df[syst_effect_val]).idxmax()
                        merged_series[syst_effect_val] = good_df.loc[idxmax, [syst_effect_val]][0]
                        merged_series[syst_effect_err] = good_df.loc[idxmax, [syst_effect_err]][0]
                        if cls.AUXILIARY_KEYS is not None:
                            aux_keys = cls.AUXILIARY_KEYS[syst_type][i]
                            merged_series[aux_keys] = good_df.loc[idxmax, aux_keys]
                merged_index = list(merged_series.name)
                merged_index[label_loc] = merged_label
                merged_series.name = tuple(merged_index)
                all_series.append(merged_series)
            merged_df = merged_df.append(all_series)
        return merged_df

    @classmethod
    def merge_df_by_max_effect(cls, df, condition, ungrouped_index=None, ignore_pruned=True):  
        if ungrouped_index is None:
            ungrouped_index = df.index.names
        if len(ungrouped_index) > 0:
            target_index = ungrouped_index[-1]
            if target_index in condition:
                index_level_condition = condition[target_index]
                merged_df = cls.merge_index_level_df_by_max_effect(df, index_level_condition, target_index,
                                                                   ignore_pruned=ignore_pruned)
            else:
                merged_df = df
            return cls.merge_df_by_max_effect(merged_df, condition, ungrouped_index[:-1],
                                              ignore_pruned=ignore_pruned)
        else:
            return df
        
    @classmethod
    def _fix_same_sign(cls, df):
        fixed_df = df.copy()
        for syst_type in cls.PRIMARY_KEYS:
            syst_effect_val_up   = cls.PRIMARY_KEYS[syst_type][0] + ('val',)
            syst_effect_val_down = cls.PRIMARY_KEYS[syst_type][1] + ('val',)
            
            criteria = fixed_df[syst_effect_val_up]*fixed_df[syst_effect_val_down] > 0
            criteria_case_up_greater = criteria & (np.abs(fixed_df[syst_effect_val_up]) > np.abs(fixed_df[syst_effect_val_down]))
            criteria_case_up_smaller = criteria & ~criteria_case_up_greater
            
            fixed_df.loc[criteria_case_up_greater, [syst_effect_val_down]] = \
            -fixed_df[criteria_case_up_greater][syst_effect_val_down]
            fixed_df.loc[criteria_case_up_smaller, [syst_effect_val_up]] = \
            -fixed_df[criteria_case_up_smaller][syst_effect_val_up]
        return fixed_df
    
    def fix_same_sign(self):
        self.dataframe = self._fix_same_sign(self.dataframe)
            
    def remove_failed_results(self):
        self.dataframe = self.dataframe[self.dataframe['status'] == 1]
        
    def post_process(self, merge_condition=None):
        if self.dataframe is None:
            return
        # remove systematics with failed status
        print("INFO: Filtering systematics with failed status")
        self.remove_failed_results()
        print("INFO: Merging smear systematics")
        # need to prune non-significative systematics first
        self.prune_systematics(do_threshold=False)
        # also fix up and down variations with same sign
        self.fix_same_sign()
        self.merge_smear_systematics()
        print("INFO: Applying systematics pruning")
        self.prune_systematics(do_threshold=True)
        if merge_condition is not None:
            print("INFO: Merging systematics by maximum effect")
            self.dataframe = self.merge_df_by_max_effect(self.dataframe, merge_condition)
        print("INFO: Fixing up and down systematics with same sign")
        self.fix_same_sign()
        self.processed = True

    def save(self, fname):
        if self.dataframe is None:
            return
        self.dataframe.reset_index().to_json(fname, indent=2)
        
    @classmethod
    def fill_hierarchical_data(cls, df, index_list, keys, scale=1./100):
        data = {}
        if len(index_list) == 0:
            values = tuple()
            for key in keys:
                key = key + ('val',)
                value = df[key].values[0]*scale
                values += (value,)
            if df['symmetric'][0]:
                return values[0]*scale
            return values
        for index, df_new in df.groupby(index_list[0]):
            data[index] = cls.fill_hierarchical_data(df_new, index_list[1:], keys)
        return data
    
    def get_simplified_data(self, scale=1./100):
        if self.dataframe is None:
            return
        data = {}
        for syst_type in self.PRIMARY_KEYS:
            df = self.get_valid_dataframe(syst_type)
            keys = self.PRIMARY_KEYS[syst_type]
            data[syst_type] = self.fill_hierarchical_data(df, self.INDICES, keys, scale=scale)
        return data
    
    def get_valid_dataframe(self, syst_type):
        if self.dataframe is None:
            return
        if (syst_type, "prune_status") in self.dataframe:
            return self.dataframe[self.dataframe[(syst_type, 'prune_status')] == 0]
        return self.dataframe
    
    def save_simplified_data(self, fname):
        data = self.get_simplified_data()
        with open(fname, 'w') as f:
            json.dump(data, f, indent=2)
        
    def load(self, fname):
        data = json.load(open(fname))
        data_fixed_keys = {}
        for key in data:
            if ("(" in key) and (")" in key):
                fixed_key = literal_eval(key)
                data_fixed_keys[fixed_key] = data[key]
            else:
                data_fixed_keys[key] = data[key]
            
        self.dataframe = pd.DataFrame(data_fixed_keys).set_index(self.INDICES)
    
    @staticmethod
    def _discard(df, **expressions):
        new_df = df.copy()
        mask = pd.Series(True, index=df.index)
        for index in expressions:
            expression = expressions[index]
            mask &= new_df.index.get_level_values(index).str.contains(expression)
        new_df = new_df[~mask]
        return new_df
    
    def discard(self, **expressions):
        self.dataframe = self._discard(self.dataframe, **expressions)
            
    @classmethod
    def get_variations(cls, symmetric_syst):
        if symmetric_syst:
            return [""]
        return ["__1up", "__1down"]
        
    @classmethod
    def is_symmetric(cls, syst_name):
        if (syst_name == "MET_SoftTrk_ResoPara") or (syst_name == "MET_SoftTrk_ResoPerp") or \
           (syst_name == "showering"):
            return True
        syst_object = cls.get_syst_object(syst_name)
        if syst_object == "THEORY":
            return True
        return False

    @classmethod
    def get_syst_object(cls, syst_name):
        for obj, patterns in cls.OBJECT_PATTERNS.items():
            if any(pattern in syst_name for pattern in patterns):
                return obj
        raise ValueError("unable to infer object from the systematics `{}`".format(syst_name))
            
    @classmethod
    def get_syst_theme(cls, syst_name):
        for theme, patterns in cls.THEME_PATTERNS.items():
            if any(pattern in syst_name for pattern in patterns):
                return theme
        raise ValueError("unable to infer theme from the systematics `{}`".format(syst_name))
    
    @staticmethod
    def get_required_hists(syst_object):
        raise NotImplementedError("inherits from SystematicsData")