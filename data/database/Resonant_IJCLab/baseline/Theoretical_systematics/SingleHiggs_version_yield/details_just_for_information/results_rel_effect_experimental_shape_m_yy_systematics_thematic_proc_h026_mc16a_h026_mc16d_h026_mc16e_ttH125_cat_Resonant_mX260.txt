\newcolumntype{C}{>{\centering\arraybackslash}p{2cm}}
\begin{table}[h!]
\begin{center}
\scriptsize
\begin{tabular}{|l|l|C|C|}
\hline
\multicolumn{2}{|l|}{Source of systematic uncertainty}    &\multicolumn{2}{c|}{\% effect wrt nominal}\\
\multicolumn{2}{|l|}{                                }   &\multicolumn{2}{c|}{ttH125}\\
\multicolumn{2}{|l|}{                                } &$\mu$   &$\sigma$\\
\hline
\hline
\multirow{2}{*}{Event-based}    \rule[-1.ex]{0pt}{3.5ex}&\verb|PRW|	&- &$\pm 0.44$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|Trigger|	&- &-	\\
\hline
\multirow{4}{*}{Photon}         \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_PES|	&$\pm 0.41$ &$\pm 1.96$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_PER|	&$<0.1$ &$\pm 6.75$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_EFF_ID|	&$<0.1$ &-	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_EFF_Isol|	&$<0.1$ &$\pm 0.11$	\\
\hline
\multirow{3}{*}{Jet}            \rule[-1.ex]{0pt}{3.5ex}&\verb|JET_JES|	&$<0.1$ &$\pm 1.46$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|JET_JER|	&$<0.1$ &$\pm 3.78$	\\
\hline
\multirow{3}{*}{Flavour tagging}\rule[-1.ex]{0pt}{3.5ex}&\verb|FT_EFF_B|	&$<0.1$ &-	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|FT_EFF_C|	&$<0.1$ &-	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|FT_EFF_Light|	&- &$\pm 0.10$	\\
\hline
\end{tabular}
\end{center}
\vspace{-0.5cm}
\caption{Summary of dominant experimental systematic uncertainties affecting expected shape after the selection of the category \textrm{Resonant\_mX260}. Sources marked ~-~ are not significant.}
\label{table_experimental_systematics_thematic_shape_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_Resonant_mX260}
\end{table}
