\newcolumntype{C}{>{\centering\arraybackslash}p{2cm}}
\begin{table}[h!]
\begin{center}
\scriptsize
\begin{tabular}{|l|l|c|}
\hline
\multicolumn{2}{|l|}{Source of systematic uncertainty}   &\multicolumn{1}{c|}{\% effect wrt nominal}\\
\multicolumn{2}{|l|}{                                }   &ttH125\\
\hline
\hline
\multirow{3}{*}{Theory}\rule[-1.ex]{0pt}{3.5ex}&\verb|QCD|	&$\pm 6.91$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PDF_alpha_s|	&$\pm 12.54$	\\
\hline
\end{tabular}
\end{center}
\vspace{-0.5cm}
\caption{Summary of dominant theoretical systematic uncertainties affecting expected yield after the selection of the category \textrm{Resonant\_mX1000}. Sources marked ~-~ are not significant.}
\label{table_theoretical_systematics_thematic_yield_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_Resonant_mX1000}
\end{table}
