\newcolumntype{C}{>{\centering\arraybackslash}p{2cm}}
\begin{table}[h!]
\begin{center}
\scriptsize
\begin{tabular}{|l|l|c|}
\hline
\multicolumn{2}{|l|}{Source of systematic uncertainty}   &\multicolumn{1}{c|}{\% effect wrt nominal}\\
\multicolumn{2}{|l|}{                                }   &ttH125\\
\hline
\hline
\multirow{2}{*}{Event-based}    \rule[-1.ex]{0pt}{3.5ex}&\verb|PRW|	&$\pm 0.91$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|Trigger|	&$\pm 1.00$	\\
\hline
\multirow{4}{*}{Photon}         \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_PES|	&$\pm 0.98$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_PER|	&$\pm 0.23$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_EFF_ID|	&$\pm 1.18$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PH_EFF_Isol|	&$\pm 1.77$	\\
\hline
\multirow{3}{*}{Jet}            \rule[-1.ex]{0pt}{3.5ex}&\verb|JET_JES|	&$\pm 3.19$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|JET_JER|	&$\pm 3.00$	\\
\hline
\multirow{3}{*}{Flavour tagging}\rule[-1.ex]{0pt}{3.5ex}&\verb|FT_EFF_B|	&$\pm 1.72$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|FT_EFF_C|	&$\pm 0.64$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|FT_EFF_Light|	&$\pm 1.16$	\\
\hline
\end{tabular}
\end{center}
\vspace{-0.5cm}
\caption{Summary of dominant experimental systematic uncertainties affecting expected yield after the selection of the category \textrm{Resonant\_mX1000}. Sources marked ~-~ are not significant.}
\label{table_experimental_systematics_thematic_yield_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_Resonant_mX1000}
\end{table}
