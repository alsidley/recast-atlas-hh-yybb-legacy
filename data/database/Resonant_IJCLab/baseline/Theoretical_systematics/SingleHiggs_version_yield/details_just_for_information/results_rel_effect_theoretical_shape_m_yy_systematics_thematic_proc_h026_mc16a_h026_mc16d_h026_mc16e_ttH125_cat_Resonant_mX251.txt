\newcolumntype{C}{>{\centering\arraybackslash}p{2cm}}
\begin{table}[h!]
\begin{center}
\scriptsize
\begin{tabular}{|l|l|C|C|}
\hline
\multicolumn{2}{|l|}{Source of systematic uncertainty}    &\multicolumn{2}{c|}{\% effect wrt nominal}\\
\multicolumn{2}{|l|}{                                }   &\multicolumn{2}{c|}{ttH125}\\
\multicolumn{2}{|l|}{                                } &$\mu$   &$\sigma$\\
\hline
\hline
\multirow{3}{*}{Theory}\rule[-1.ex]{0pt}{3.5ex}&\verb|QCD|	&- &-	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PDF_alpha_s|	&- &-	\\
\hline
\end{tabular}
\end{center}
\vspace{-0.5cm}
\caption{Summary of dominant theoretical systematic uncertainties affecting expected shape after the selection of the category \textrm{Resonant\_mX251}. Sources marked ~-~ are not significant.}
\label{table_theoretical_systematics_thematic_shape_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_Resonant_mX251}
\end{table}
