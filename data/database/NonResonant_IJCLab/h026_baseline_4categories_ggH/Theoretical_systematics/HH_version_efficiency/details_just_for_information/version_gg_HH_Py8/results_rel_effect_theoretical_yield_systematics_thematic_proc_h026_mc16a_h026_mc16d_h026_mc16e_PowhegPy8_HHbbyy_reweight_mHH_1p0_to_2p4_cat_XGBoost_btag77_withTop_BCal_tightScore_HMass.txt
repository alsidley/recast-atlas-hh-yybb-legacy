\newcolumntype{C}{>{\centering\arraybackslash}p{2cm}}
\begin{table}[h!]
\begin{center}
\scriptsize
\begin{tabular}{|l|l|c|}
\hline
\multicolumn{2}{|l|}{Source of systematic uncertainty}   &\multicolumn{1}{c|}{\% effect wrt nominal}\\
\multicolumn{2}{|l|}{                                }   &h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_HHbbyy_reweight_mHH_1p0_to_2p4\\
\hline
\hline
\multirow{3}{*}{Theory}\rule[-1.ex]{0pt}{3.5ex}&\verb|QCD|	&$\pm 2.90$	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PDF_alpha_s|	&$\pm 0.75$	\\
\hline
\end{tabular}
\end{center}
\vspace{-0.5cm}
\caption{Summary of dominant theoretical systematic uncertainties affecting expected yield after the selection of the category \textrm{XGBoost\_btag77\_withTop\_BCal\_tightScore\_HMass}. Sources marked ~-~ are not significant.}
\label{table_theoretical_systematics_thematic_yield_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_HHbbyy_reweight_mHH_1p0_to_2p4_XGBoost_btag77_withTop_BCal_tightScore_HMass}
\end{table}
