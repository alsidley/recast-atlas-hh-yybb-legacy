[XGBoost_btag77_withTop_BCal_looseScore_LMass]

muCB_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_LMass =  125.08 +/- 0.025324 L(123.5 - 126.5) // [GeV]
sigmaCB_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_LMass =  1.6436 +/- 0.031371 L(0 - 6.36943) // [GeV]
alphaCB_Low_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_LMass =  1.5120 +/- 0.17549 L(0 - 5) 
nCB_Low_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_LMass =  16.392 +/- 22.139 L(0 - 200) 
alphaCB_High_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_LMass =  1.3689 +/- 0.071154 L(0 - 5) 
nCB_High_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_LMass =  118.83 +/- 178.27 L(0 - 200) 
