{
  "sequencer":["YieldCalculator"],

    "name":{
        "yields" : "yields"
    },

  "directories": {
    "mc16a": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16a/Nominal/",
    "mc16d": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16d/Nominal/",
    "mc16e": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16e/Nominal/"
  },

  "dumper": {
    "mc16a": false,
    "mc16d": false,
    "mc16e": false
  },

  "variables": {
    "m_yy": {
      "var": "HGamEventInfoAuxDyn.m_yy*0.001",
      "bins": {
        "nbins": 55,
        "lbins": 105,
        "ubins": 160
      }
    }
  },
  "selections": {

    "weight": "HGamEventInfoAuxDyn.crossSectionBRfilterEff*HGamEventInfoAuxDyn.weight*HGamEventInfoAuxDyn.yybb_weight*HGamEventInfoAuxDyn.weightFJvt*4",

    "tightScore_HMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 1) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3) ",

    "looseScore_HMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 2) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3)   ",

    "tightScore_LMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2  && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 3) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3)   ",

    "looseScore_LMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2  && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 4) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3)   "
  },

  "lumi": {
    "mc16a": 36207.66,
    "mc16d": 44307.4,
    "mc16e": 58450.1
  },

  "samples": {
    "HH_ggF": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim.e8222_s3126_r9364_p4239_h026.root",
        "mc16d": "mc16d.PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim.e8222_s3126_r10201_p4239_h026.root",
        "mc16e": "mc16e.PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim.e8222_s3126_r10724_p4239_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_HHbbyy_cHHH01d0_noDalitz_weighted"
    },
    "HH_VBF": {
      "datafiles": {
        "mc16a": "mc16a.MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126_r9364_p4239_h026.root",
        "mc16d": "mc16d.MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126_r10201_p4239_h026.root",
        "mc16e": "mc16e.MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126_r10724_p4239_h026.root"
      },
      "histoName": "CutFlow_MGPy8_hh_bbyy_vbf_l1cvv1cv1_noDalitz_weighted"
    },
    "ttH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailed.e7488_s3126_r9364_p4180_h026.root",
        "mc16d": "mc16d.PowhegPy8_ttH125_fixweight.MxAODDetailed.e7488_s3126_r10201_p4180_h026.root",
        "mc16e": "mc16e.PowhegPy8_ttH125_fixweight.MxAODDetailed.e7488_s3126_r10724_p4180_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_ttH125_fixweight_noDalitz_weighted"
    },
    "VBF": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODDetailed.e6970_s3126_r9364_p4180_h026.root",
        "mc16d": "mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODDetailed.e6970_s3126_r10201_p4180_h026.root",
        "mc16e": "mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODDetailed.e6970_s3126_r10724_p4180_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8EG_NNPDF30_VBFH125_noDalitz_weighted"
    },
    "ZH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_ZH125J.MxAODDetailed.e5743_s3126_r9364_p4207_h026.root",
        "mc16d": "mc16d.PowhegPy8_ZH125J.MxAODDetailed.e5743_s3126_r10201_p4207_h026.root",
        "mc16e": "mc16e.PowhegPy8_ZH125J.MxAODDetailed.e5743_s3126_r10724_p4207_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_ZH125J_noDalitz_weighted"
    },
    "ggZH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_ggZH125.MxAODDetailed.e5762_s3126_r9364_p4207_h026.root",
        "mc16d": "mc16d.PowhegPy8_ggZH125.MxAODDetailed.e5762_s3126_r10201_p4207_h026.root",
        "mc16e": "mc16e.PowhegPy8_ggZH125.MxAODDetailed.e5762_s3126_r10724_p4207_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_ggZH125_noDalitz_weighted"
    },
    "ggH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.e5607_s3126_r9364_p4180_h026.root",
        "mc16d": "mc16d.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.e5607_s3126_r10201_p4180_h026.root",
        "mc16e": "mc16e.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.e5607_s3126_r10724_p4180_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_NNLOPS_ggH125_noDalitz_weighted"
    },
    "WmH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_WmH125J.MxAODDetailed.e5734_s3126_r9364_p4207_h026.root",
        "mc16d": "mc16d.PowhegPy8_WmH125J.MxAODDetailed.e5734_s3126_r10201_p4207_h026.root",
        "mc16e": "mc16e.PowhegPy8_WmH125J.MxAODDetailed.e5734_s3126_r10724_p4207_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_WmH125J_noDalitz_weighted"
    },
    "WpH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_WpH125J.MxAODDetailed.e5734_s3126_r9364_p4207_h026.root",
        "mc16d": "mc16d.PowhegPy8_WpH125J.MxAODDetailed.e5734_s3126_r10201_p4207_h026.root",
        "mc16e": "mc16e.PowhegPy8_WpH125J.MxAODDetailed.e5734_s3126_r10724_p4207_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_WpH125J_noDalitz_weighted"
    },
    "bbH": {
      "datafiles": {
        "mc16a": "mc16a.PowhegPy8_bbH125.MxAODDetailed.e6050_s3126_r9364_p4180_h026.root",
        "mc16d": "mc16d.PowhegPy8_bbH125.MxAODDetailed.e6050_s3126_r10201_p4180_h026.root",
        "mc16e": "mc16e.PowhegPy8_bbH125.MxAODDetailed.e6050_s3126_r10724_p4180_h026.root"
      },
      "histoName": "CutFlow_PowhegPy8_bbH125_noDalitz_weighted"
    },
    "tHjb": {
      "datafiles": {
        "mc16a": "mc16a.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODDetailed.e7305_s3126_r9364_p4180_h026.root",
        "mc16d": "mc16d.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODDetailed.e7305_s3126_r10201_p4180_h026.root",
        "mc16e": "mc16e.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODDetailed.e7305_s3126_r10724_p4180_h026.root"
      },
      "histoName": "CutFlow_aMCnloPy8_tHjb125_4fl_noDalitz_weighted"
    },
    "tWH": {
      "datafiles": {
        "mc16a": "mc16a.aMCnloPy8_tWH125.MxAODDetailed.e7425_s3126_r9364_p4180_h026.root",
        "mc16d": "mc16d.aMCnloPy8_tWH125.MxAODDetailed.e7425_s3126_r10201_p4180_h026.root",
        "mc16e": "mc16e.aMCnloPy8_tWH125.MxAODDetailed.e7425_s3126_r10724_p4180_h026.root"
      },
      "histoName": "CutFlow_aMCnloPy8_tWH125_noDalitz_weighted"
    }
  }
}
